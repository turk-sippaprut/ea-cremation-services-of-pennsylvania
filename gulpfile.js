let gulp = require('gulp');
let cleanCss = require('gulp-clean-css');
let concatCss = require('gulp-concat-css');
let fs = require('fs');
let notify = require("gulp-notify");

let gutil = require('gulp-util');
let ftp = require( 'vinyl-ftp' );


let uglify = require('gulp-uglify');
let pump = require('pump');
let concat = require('gulp-concat');
let rename = require("gulp-rename");
let babel = require('gulp-babel');

let cleanJSON = require("strip-json-comments");



gulp.task('css', () => {
	let cssSrc = [
		'assets/css/font-awesome.css',
		'assets/css/validationEngine.jquery.css',
		'assets/css/reset.css' ,
		'assets/css/ea.css',
		'assets/css/pnotify.custom.css',
		'assets/js/magnific/magnific-popup.css' ,
		'assets/js/bxslider/jquery.bxslider.css'
	];

	return gulp.src(cssSrc)
			   .pipe(concatCss("build.min.css"))
			   .pipe(cleanCss( {compatibility: 'ie8'} , (details) => {} ))
			   .pipe(gulp.dest('assets/css/'));
});

gulp.task('js' , (cb) => {
	let jsSrc = [
		'assets/js/moblie.js' ,
		'assets/js/jquery.validationEngine-en.js' ,
		'assets/js/jquery.creditCardValidator.js' ,
		'assets/js/jquery.validationEngine.js' ,
		'assets/js/formatter/formatter.js' ,
		'assets/js/jstz.min.js' ,
		'assets/js/moment.min.js' ,
		'assets/js/moment-timezone.js' ,
		'assets/js/lodash.min.js' ,
		'assets/js/pnotify.custom.min.js' ,
		'assets/js/earrangement.js' ,
		'assets/js/magnific/jquery.magnific-popup.min.js' ,
		'assets/js/jQuery.print.js' ,
		'assets/js/script.js' ,
		'assets/js/util.js' ,
		'assets/js/validate.js' ,
		'assets/js/fnetea.js' ,
		'assets/js/caculator.js' ,
		'assets/js/PrettyOption.js' ,
		'assets/js/plugin-ea.js' ,
		'assets/js/bxslider/jquery.bxslider.min.js'
	];

	pump(
		[
	      gulp.src(jsSrc),
	      concat('build.min.js'),
	      uglify(),
	      gulp.dest('assets/js/')
	    ],
	    cb
	);
});

gulp.task('js-minify-es6' , (cb) => {
	let jsTemplate = [
		'assets/js/template/**/*.js' ,
		'!assets/js/template/**/*.min.js' 
	]
	gulp.watch(jsTemplate).on('change', (event) => {
		let basefile = event.path.split('\\').pop();
		let pathFile = event.path.replace(event.path.split('\\').pop() , '');
		
		if ( basefile.split(".").pop() !== "js" ) {
			return;
		}
		
		pump(
			[
				gulp.src(event.path) ,
				babel({
		            presets: ['env']
		        }) ,
				rename({
					suffix: ".min"
				}),
				uglify() ,
				gulp.dest(pathFile),
				notify( event.path + ' is min file')
			] 
		);
	});
});

gulp.task('ftp' , (cb) => { 

	//Watch Ftp
	let globs = [
        'assets/js/build.min.js',
        'assets/css/build.min.css',
        'assets/js/template/**/*.min.js' 
    ];

    let config = fs.readFileSync("./sftp-config.json", "utf8");
    config = JSON.parse(cleanJSON(config));

    let conn = ftp.create( {
	        host:     config.host ,
	        user:     config.user,
	        password: config.password,
	        port:     21,
	        parallel: 3 ,
	     //   debug:    function(d){console.log(d);},
	        //log:      gutil.log
	    }
    );

    gulp.watch(globs).on('change', (event) => {
		console.log('Start detected! Uploading file "' + event.path + '", ' + event.type);
		pump(
	    	[
	    		gulp.src( event.path , { base: '.' , buffer: false } ) ,
	    		conn.newer( config.remote_path ) ,
	    		conn.dest( config.remote_path ) ,
	    		notify( event.path + ' is updated ! ' + `${config.remote_path}` )
	    	]
	    );
	});

});



gulp.task('default' , ['css' , 'js' , 'js-minify-es6' , 'ftp'  ] , () => {
	//Watch Css
	let watchCss = [
		'assets/css/*.css' ,
		'!assets/css/build.min.css'
	];
	gulp.watch(watchCss , ['css']);

	//Watch JS
	let watchJs = [
		'assets/js/*.js' ,
		'!assets/js/build.min.js' 
	];
	gulp.watch(watchJs , ['js']);

	
	

});