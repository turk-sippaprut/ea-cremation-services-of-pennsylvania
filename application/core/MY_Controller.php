<?php

if((ENVIRONMENT !== 'production')){
    error_reporting(E_ALL);
}



//For Encryption Session
include('/home/funeralnet/public_html/mfscommon/php/encryption.php');
require_once(APPPATH."libraries/kint/Kint.class.php");
require_once("/home/funeralnet/public_html/mfscommon/php/dev_check.php");

//For upload to s3
require_once("/home/funeralnet/public_html/mfscommon/php/s3lib/s3lib.php");


/**
 * Main Controller for manage template
 */
class MY_Controller extends CI_Controller{

    const paymentprefix = 'payment';
    const vitalprefix = 'formvital';
    const authprefix = 'formauth';

    //const tmpUploadfile =  '/earrangement/assets/uploads/';
    protected $_targetUrl;
    protected $cfg;
    protected $settings;
    protected $summary;
    protected $_s3;
    //protected $session;
    protected $archive_id = NULL;

    //Data for send to view
    protected $data;


    public function __construct()
    {

        parent::__construct();
        if ( is_dev() )
        {
            opcache_reset();
        }

        //Load Config of earrangement
        $this->cfg = $this->config->config;

        date_default_timezone_set('America/New_York');

        $sessionPath = $this->cfg['sess_save_path'];
        session_name($this->cfg['sess_cookie_name']);
        session_save_path($sessionPath);
        session_start();

        $this->_s3 =  new s3lib;

        // //Get step
        $this->data['steps'] = $this->config->item('steps');
        $this->data['states'] = $this->config->item('states');
        $this->data['county'] = $this->config->item('county');

        $this->data['isAdmin'] = $this->input->get('admin');
        $this->data['current_segment'] = $this->uri->segment(1);
        $this->settings = $this->msetting->get_settings();
        $this->data['baseUrl'] = "http://" . $_SERVER['SERVER_NAME'];
        //$this->data['assetsPath'] = $this->cfg['root']."assets/";

        $user_session = $this->session->get_user();
        $this->archive_id = isset($user_session['order_archive_id']) ? $user_session['order_archive_id'] : NULL;

        $this->data['pathPictures'] = "https://fnetclients.s3.amazonaws.com/mastercart/clients/".$this->cfg['client_id']."/pictures/";

        // //Set phone local
        $this->data['phoneLocal'] = (isset( $this->settings['client_phone_local']) ) ? $this->settings['client_phone_local'] : NULL;
        $this->data['phoneLocal'] = str_replace("(" , "", $this->data['phoneLocal']);
        $this->data['phoneLocal'] = str_replace(") " , "-", $this->data['phoneLocal']);

        // //Set Address
        $cityState  = NULL;
        $cityState .= (isset($this->settings['client_address_city'])) ? $this->settings['client_address_city'].", " : NULL;
        $cityState .= (isset($this->settings['client_address_state'])) ? $this->settings['client_address_state'] : NULL;
        $cityState .= (isset($this->settings['client_address_zip'])) ?  " ".$this->settings['client_address_zip'] : NULL;
        $this->data['cityState'] = $cityState;

        $page =  $this->uri->segment_array();
        if(empty($page[1]) or $page[1] != "debuger" ){
            if((ENVIRONMENT === 'production') & !is_dev()){
                $this->mylogs
                    ->setData('session' , $this->session->get_user())
                    ->setData('fileName' ,  session_id().'.log' )
                    ->setData('path' , SEVERPATH.$this->cfg['root'].'assets/logs/session/'.date("Y/m/d/") )
                    ->saveStep();
            }
        }

        if(!isset($user_session['session_id'])){
            $this->session->set_user('session_id' , session_id());
        }
    }

    protected function uriDirection(){
        $target =  $this->uri->segment_array();
        $size = $this->uri->total_segments();
        return isset($target[$size]) ? $target[$size] : null;
    }


    protected function get_wrapper($params = array()){
        $template = $this->settings['template'];
        if(isset($params['printview']) and $params['printview'] == true){
            $template = $this->settings['template-print'];
        }
        $wrapper = $this->get_wrapper_cache($template);

        # Modify page title
        $page_title = $this->settings['page_title'];
        $page_title = str_replace('[page_title]', $params['page_title'], $page_title);

        # Modify template
        $wrapper = str_replace('TITLE_PLACEHOLDER', $page_title, $wrapper);
        if(!isset($params['printview']) or $params['printview'] == false){
            $wrapper = str_replace('</head>', '<script src="https://secure.funeralnet.com/js/reconfigformat.js"></script></head>', $wrapper);
        }
        $domain = array_reverse(explode('.', $_SERVER['SERVER_NAME']));
        $domain = implode('.', array($domain[1],$domain[0]));
        $wrapper = str_replace('document.domain="'.$_SERVER['SERVER_NAME'].'";', 'document.domain="'.$domain.'";', $wrapper);

        $wrapper = explode('<!--EA-CONTENT-->', $wrapper);

        if((is_dev() or is_staff()) and (!isset($params['printview']) or $params['printview'] == false) ){
            $test_badge = $this->load->view('test_badge', $this->data, true);
            if(isset($wrapper[1])){
                $wrapper[1] = $test_badge. $wrapper[1];
            }
        }

        return array(
            'header' => $wrapper[0],
            'footer' => $wrapper[1]
        );
    }

    protected function get_wrapper_cache($wrapper_link){
        $wrapper = '';
        // Get wrapper
        $cache_filename = 'application/cache/'.basename($wrapper_link).'.html';
        $cache_time     = 3*3600; // Time in seconds to keep a page cached

        // Check Cache
        $cache_created = (file_exists($cache_filename)) ? filemtime($cache_filename) : 0;
        // If cache valid, get it
        if ((time() - $cache_created) < $cache_time) {
            $wrapper = file_get_contents($cache_filename);
        }
        // If no cache, get wrapper and cache it
        if( empty($wrapper) ){
            $wrapper = file_get_contents($wrapper_link);
            $wrapper = str_replace('<link rel="stylesheet" href="/themes/'.$this->cfg['client_id'].'/elements/ea/ea.css">', '', $wrapper);
            $wrapper = str_replace('<link rel="stylesheet" href="/themes/'.$this->cfg['client_id'].'/css/jquery.bxslider.css">', '', $wrapper);
            $wrapper = str_replace('<script src="/js/plugins.js"></script>', '', $wrapper);
            $wrapper = str_replace('<script src="/themes/'.$this->cfg['client_id'].'/elements/ea/PrettyOption.js"></script>', '', $wrapper);
            $wrapper = str_replace('<script src="/themes/'.$this->cfg['client_id'].'/elements/ea/plugin-ea.js"></script>', '', $wrapper);
            $wrapper = str_replace('<script src="/themes/'.$this->cfg['client_id'].'/elements/ea/jquery.bxslider.min.js"></script>', '', $wrapper);
            // $wrapper = str_replace('/concrete/js/jquery.js', $this->cfg['root'] . 'assets/js/jquery.min.js', $wrapper);
            $wrapper = str_replace('<script src="/themes/'.$this->cfg['client_id'].'/js/jquery.bxslider.min.js"></script>', '', $wrapper);
            $wrapper = str_replace('<script src="/themes/'.$this->cfg['client_id'].'/js/main.js"></script>', '', $wrapper);
            $wrapper = str_replace('</head>', '<script>var root = "'.$this->cfg['root'].'"</script></head>', $wrapper);

            $font = '<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet" type="text/css">';
            $wrapper = str_replace('</head>', $font . '</head>', $wrapper);

            # Add CSS files
            foreach($this->cfg['assets']['css'] as $key => $file){
                $wrapper = str_replace('</head>', '<link rel="stylesheet" type="text/css" href="'.$this->cfg['root'].$file.'" /></head>', $wrapper);
            }

            # Add JS files
            foreach($this->cfg['assets']['js'] as $key => $file){
                 if(basename($wrapper_link) == 'ea-template-print' and basename($file) == "sidebar.js"){
                    continue;
                 }
                $wrapper = str_replace('</head>', '<script src="'.$this->cfg['root'].$file.'"></script></head>', $wrapper);
            }
            file_put_contents($cache_filename, $wrapper);


            @chmod($cache_filename, 0775);
            @chown($cache_filename, $this->cfg['client_id']);
        }
        return $wrapper;
    }

    protected function go_to($next_form){
        # Build an absolute URL
        $url = $this->proper_url(array('page' => $next_form, 'secure' => $this->config->item('secure')));

        # Load the next page.
        header('Location: ' . $url);
        exit();
    }


    protected function proper_url($params){
        $page = $params['page'];
        # Determine if the page should be secure or not.
        $protocol = 'http';
        if ( isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == "on"){
            $protocol = 'https';
        }
        # Build an absolute URL
        $url = $protocol . '://' . $_SERVER['HTTP_HOST'] . $this->cfg['root']. $page;
        # Return the absolute URL.
        return $url;
    }


    protected function load_selected($vars){
        extract($vars);

        $selected = $this->session->get_user($session_index);
        $json = array();
        if($selected){
            foreach($selected as $group_key=>$group){
                if(isset($option_groups[$group_key]) and isset($option_groups[$group_key]['options']) and is_array($option_groups[$group_key]['options'])){
                    foreach($group as $id=>$option){
                        foreach($option_groups[$group_key]['js_structure'] as $field){
                            $json[$group_key][$id][$field] = $option[$field];
                        }
                    }
                }
            }
        }
        if(count($json)){
            $json = json_encode($json);
        }else{
            $json = '{}';
        }
        $this->load->vars(array($session_index=>$json));
    }


    protected function save_selected($vars){
        extract($vars);
        $selected = json_decode($this->input->post('selected'), true);
        foreach($selected as $group_key=>$group){
            if(isset($option_groups[$group_key]) and is_array(@$option_groups[$group_key]['options'])){
                foreach($group as $id=>$option){
                    if(isset($option_groups[$group_key]['options'][$id])){
                        $selected[$group_key][$id] = array_merge($option_groups[$group_key]['options'][$id] , $option);
                    }
                }
            }
        }
        $this->session->set_user($session_index, $selected);
    }

    /**
     * [checkDirectForm block direct form if not select package and cart but if become a member is show form]
     */
    protected function checkDirectForm(){
       if($this->session->checking() == false){
            show_404();
       }
       $user_session = $this->session->get_user();
       if(isset($user_session['order_archive_id'])){
           if($this->morder->checkCompleted($user_session['order_archive_id']) and $this->morder->checkCompletedPaper($user_session['order_archive_id']) ){
               $this->go_to("thankyou");
           }
       }
    }

    protected function checkPaymentAccept(){
        //if pay online done will can not change options
        $user_session = $this->session->get_user();
        if(isset($user_session['ccresult']['result']) and strtolower($user_session['ccresult']['result']) == "accepted"){
           return true;
        }
        return false;
    }


    protected function check_paperwork(){
        if($this->morder->checkCompletedPaper($this->archive_id) and $this->isAdmin == false){
            $this->go_to('thankyou');
        }
    }

    protected function getDataByHash($hash = NULL){

        if(!empty($hash)){
            $hash  = urldecode($hash);
            $order = $this->morder->getByHash($hash);
            $user_session = $this->session->get_user();
            $decodeSession = $this->session->decode_user($order['session'], $this->settings['encryption_key']);

            if(isset($user_session['hash_url_encoded']) and $user_session['hash_url_encoded'] == $hash){
                $sessionRef = array_merge(
                    $user_session ,
                    $decodeSession
                );
            }
            else{
                $sessionRef = $decodeSession;
            }
            $user_session = $this->session->set(
                'user' ,
                $sessionRef
            );
        }
    }

    protected function createNewArchive(){
        $user_session = $this->session->get_user();
         //Save new order for get id data
        if(!isset($user_session['order_archive_id'])){
             //Set uniq
            if(!isset($user_session["uniqid"]) ){
                $uniq = $this->morder->getNewUniq();
                $user_session = $this->session->set_user('uniqid', $uniq);
            }
            //Set Hash
            if(!isset($user_session["hash"]) ){
                $user_session = $this->session->set_user('hash', get_hash_code());
            }
            if(isset($user_session['hash'])){
                $user_session = $this->session->set_user('hash_url_encoded', urlencode($user_session['hash']));
            }
            self::order_archive();
        }
    }

    protected function updateSessionOrder(){
        $user_session = $this->session->get_user();
        $session_save =  $this->session->encode_user($this->settings['encryption_key']);
        $newData = array(
            'session' => $session_save
        );
        $this->morder->update($newData , array("id" => $user_session['order_archive_id'] )  );
    }

    /**
     * Save Order when this order not complete
     * @param  string $status    status of order  [Abandon / complete]
     * @param  string $paperwork status of paper  [Waiting / complete]
     * @return [type]            [description]
     */
    protected function order_archive($status="Abandon", $paperwork='Waiting'){

        # If the order was completed the record shouldn't be updating anymore.
        if($this->morder->checkCompleted($this->archive_id) and $paperwork == 'Waiting') return;

        # Purchase date
        if(!$this->session->get_user('purchaseTimeStamp')){
            $this->session->set_user('purchaseTimeStamp', time());
        }
        $times  = $this->session->get_user('purchaseTimeStamp');

        # In case of admin everything do not need to be updating.
        if($this->data['isAdmin']){
            return;
        }

        # This session will be saved in archive record
        $session_save =  $this->session->encode_user($this->settings['encryption_key']);

        # Preparing data to save to archive record
        $user_session = $this->session->get_user();


        # Legal name
        $legal_name = "";
        if(isset($user_session[self::paymentprefix]['informant_name'])){
            $legal_name = $user_session[self::paymentprefix]['informant_name'];
        }

        # Package type
        $pkgtype = $user_session['pkgtype'];
        $package_name = "";
        if(isset($user_session['packageSelected']['package'])){
            $package_id = getFirstKey($user_session['packageSelected']['package']);
            $package_name     = $user_session['packageSelected']['package'][$package_id]['name'];
        }

        $paymentMethod = Vars::get($user_session[self::paymentprefix]['paybycash']);
        $data = array(
            "uniq"          => trim($this->session->get_user('uniqid')),
            "firstname"     => trim($legal_name),
            "pkgtype"       => $pkgtype,
            "package"       => trim($package_name),
            "price"         => $user_session['summary']['total'],
            "paymentmethod" => $paymentMethod ,
            "purchasedate"  => date("Y-m-d H:i:s", $times),
            "session"       => $session_save,
            "hash"          => isset($user_session['hash']) ? $user_session['hash'] : '',
            "status"        => strtolower($status),
            "paperwork"     => strtolower($paperwork)
        );
        if($this->session->get_user('order_archive_id')){
            $this->morder->update($data , array("id" => $this->session->get_user('order_archive_id') )  );
        }else{
            $this->archive_id = $this->morder->insert($data);
            $this->session->set_user('order_archive_id', $this->archive_id );
        }
    }




    protected function savePaymentLog($arr=array(),$path="assets/logs/payment.log"){
        if(!count($arr)) return false;

        if($logf = fopen($path, 'a')){ # a/w
            $text  = "=== ";
            $text .= date('Y-m-d H:i:s');
            $text .= " =====X=X=X=X=X=X=X=X=============================\n";
            $text .= print_r($arr,true);
            $text .= "\n\n\n";

            flock($logf, LOCK_EX);
            fwrite($logf, $text);
            flock($logf, LOCK_UN);  # Release the write lock.
            fclose($logf);
        }else{
            die("Cannot open log file.");
        }
    }


    # Summary calculation function
    protected function _calculate_amount_due(){

        if($this->morder->checkCompleted(  $this->session->get("order_archive_id")  )){
            return true;
        }

        $this->summary = array(
            'each'            => array() ,
            'ins_policy_info' => array() ,
            'payment_policy'  => "" ,
            'has_ins_policy'  => 0
        );
        /*
         * Drump session
         */
        $user_session = $this->session->get_user();

        /*
         * Package price
         */
        if(isset($user_session['packageSelected']['package'])){

            $packageTotal = 0;
            $packages = $this->mpackage->getData();
            foreach($user_session['packageSelected']['package'] as $id => $row){
                $price = (float)$packages[ $row['id'] ]['price'];
                $user_session['packageSelected']['package'][$id]['price'] = $price;
                $user_session['packageSelected']['package'][$id]['summary'] = $price;
                $packageTotal = $price;
            }
            $this->summary['each']['packages'] = $packageTotal;
        }

        $serviceIndex = 'transport';
        $this->summary['each'][ $serviceIndex ] = 0;
        if(isset($user_session['packageSelected'][ $serviceIndex ])  and is_array($user_session['packageSelected'][ $serviceIndex ])){
            $this->data[ $serviceIndex ] = $this->mserives->getByType( $serviceIndex );
            extract($user_session['packageSelected']);
            $serviceSummary = 0;
            foreach($$serviceIndex as $id => $row){
                $quan = 0;
                if ( !empty($this->data[ $serviceIndex ][$id]['name_short']) ){
                  $user_session['packageSelected'][ $serviceIndex ][$id]['name'] = $this->data[ $serviceIndex ][$id]['name_short'] ;
                }

                if($id == 1){
                    //Transport
                    $price = 0;
                    if(isset($counties)){
                        $county = getFirstKey($counties);
                        $price = $this->cfg['counties'][ $county ]['rate'];
                    }
                    $totalPrice = $price;
                } else if($id == 2){
                    //For Death certificate
                    $quan  = $row['quan'];
                    $price = isset($this->data[ $serviceIndex ][ $id ]) ? (float)$this->data[ $serviceIndex ][ $id ]['price'] : 0;
                    $totalPrice = $price * $row['quan'];

                    $handling_id = Arr::firstKey(Vars::get($handling, []) );
                    if ( $handling_id != 3 ) {
                        $totalPrice += $this->cfg['dcc_shipping'];
                        $user_session['packageSelected'][ $serviceIndex ][$id]['shipping'] = $this->cfg['dcc_shipping'];
                    }
                } else if ($id == 14){
                    $price = 0;
                    if(isset($this->cfg['deathOccurOption'][$row['choice']]['price'])){
                        $price = $this->cfg['deathOccurOption'][$row['choice']]['price'];
                    }
                    $totalPrice = $price;
                }
                else{
                    $price = isset($this->data[ $serviceIndex ][ $id ]) ? (float)$this->data[ $serviceIndex ][ $id ]['price'] : 0;
                    $totalPrice = $price;
                }
                $serviceSummary += $totalPrice;
                $user_session['packageSelected'][ $serviceIndex ][$id]['quan'] = $quan;
                $user_session['packageSelected'][ $serviceIndex ][$id]['price'] = $price;
                $user_session['packageSelected'][ $serviceIndex ][$id]['summary'] = $totalPrice;
            }
            $this->summary['each'][ $serviceIndex ] += $serviceSummary;
        }


        /*
         * Handling
         */
        $serviceIndex = 'handling';
        $this->summary['each'][ $serviceIndex ] = 0;
        if(isset($user_session['packageSelected'][ $serviceIndex ])  and is_array($user_session['packageSelected'][ $serviceIndex ]))
        {
            $this->data[ $serviceIndex ] = $this->mserives->getByType( $serviceIndex );
            extract($user_session['packageSelected']);
            $handlingSummary = 0;
            foreach($handling as $id => $row){
                $price = isset($this->data[ $serviceIndex ][ $id ]) ? (float)$this->data[ $serviceIndex ][ $id ]['price'] : 0;
                if ($id === 4 ) {
                    $country = getFirstKey(Vars::get($user_session['packageSelected']['counties'] , []));
                    $price = isset($this->cfg['counties'][ $county ]['delivery_price']) ? $this->cfg['counties'][ $county ]['delivery_price'] : $price;
                }
                $handlingSummary += $price;
                if ( !empty($this->data[ $serviceIndex ][$id]['name_short']) ){
                  $user_session['packageSelected'][ $serviceIndex ][$id]['name'] = $this->data[ $serviceIndex ][$id]['name_short'] ;
                }
                $user_session['packageSelected'][ $serviceIndex ][$id]['price'] = $price;
                $user_session['packageSelected'][ $serviceIndex ][$id]['summary'] = $price;
            }
            $this->summary['each'][ $serviceIndex ] += $handlingSummary;
        }

        /*
         * Additional
         */
        $serviceIndex = 'addition';
        $this->summary['each'][ $serviceIndex ] = 0;
        if(isset($user_session['packageSelected'][ $serviceIndex ])  and is_array($user_session['packageSelected'][ $serviceIndex ]))
        {
            $this->data[ $serviceIndex ] = $this->mserives->getByType( $serviceIndex );
            extract($user_session['packageSelected']);
            $serviceSummary = 0;
            foreach($addition as $id => $row){
                $price = isset($this->data[ $serviceIndex ][ $id ]) ? (float)$this->data[ $serviceIndex ][ $id ]['price'] : 0;
                $serviceSummary += $price;
                $user_session['packageSelected'][ $serviceIndex ][$id]['price'] = $price;
                $user_session['packageSelected'][ $serviceIndex ][$id]['summary'] = ($id == 9) ? "TBD" :  $price;
            }
            $this->summary['each'][ $serviceIndex ] += $serviceSummary;
        }


        /*
         * Merchandise
         */
        if(isset($user_session['cart']))
        {
            $cart = $user_session['cart'];
            if(is_array($cart) and count($cart)){
                $totalProduct = 0;

                $products = $this->mproduct->getAll();
                $additionMerch = $this->mserives->getByType('merchandise');
                foreach($cart as $key => $items){
                    foreach($items as $pid => $item){
                        $summaryProduct = 0;
                        if($key != "merchandise"){
                            $price =  isset($products[$pid]['price']) ? (float)$products[$pid]['price'] : 0;

                            //is included
                            if ( in_array($pid , $this->cfg['includedProduct'] ) )
                            {
                                $price = 0;
                            }

                        } else{
                            $price =  isset($additionMerch[$pid]['price']) ? (float)$additionMerch[$pid]['price'] : 0;
                            //(id of merchandise server)
                            if($pid == 13){
                                if(isset($this->cfg['memorialPortrait'][$item['keywidth']])){
                                    $price = $this->cfg['memorialPortrait'][$item['keywidth']]['price'];
                                } else{
                                    $price = $this->cfg['memorialPortrait'][1]['price'];
                                }
                            }
                        }
                        $quan  = isset($item['quan']) ? $item['quan'] : 1;
                        $summaryProduct = $price * $quan;

                        //full price for calculator sales tax
                        $totalProduct += $summaryProduct;
                        $user_session['cart'][$item['group']][$pid]['price'] = $price;
                        $user_session['cart'][$item['group']][$pid]['summary'] = $summaryProduct;
                    }
                }
                $this->summary['each']['merchandise'] = $totalProduct;
            }
        }

        # Calculate subtotal
        $this->summary['subtotal'] = 0;
        $this->summary['optionTotal'] = 0;
        if(count($this->summary['each'])){
            foreach($this->summary['each'] as $key => $price){
                $this->summary['subtotal'] += $price;
                if($key != 'packages' and $key != "merchandise"){
                    $this->summary['optionTotal'] += $price;
                }
            }
        }


        $this->summary['taxrate'] = 0;


         //Set new packageSelected
        if(isset($user_session['packageSelected'])){
            $this->session->set_user('packageSelected', $user_session['packageSelected']);
        }
        if(isset($user_session['cart'])){
            $this->session->set_user('cart', $user_session['cart']);
        }
        # Calculate summary

        $this->summary['total']  = $this->summary['subtotal'] + $this->summary['taxrate'];
        $user_session = $this->session->set_user('summary', $this->summary);
    }

     # Message creating function
    protected function _notify_message_preparation(){
        if($this->morder->checkCompleted(  $this->session->get("order_archive_id")  )){
            return true;
        }

        $user_session = $this->session->get_user();
        $this->report->clearAllMessage();
        $this->session->set_user('admintable', array());

        /*=====================================
        =            Set Documents            =
        =====================================*/
        if(isset($user_session['documents'])){
            $this->report->setAdminMessage()->setAdminMessage('Documents');
            $this->report->setMessage()->setMessage('Documents');
            foreach($user_session['documents'] as $key => $file){
                $hrefFile = '<a target="_blank" href="'.$file['url'].'">'.$file['name'].'</a>';
                if($key != 'vital'){
                    $this->report->setMessage('[indent]', $hrefFile);
                }
                $this->report->setAdminMessage($hrefFile , '');
            }
            $this->session->set_user('documentsTable', $this->report->getMessage());
            $this->report->clearEmailMessage();
        }


        /*=====  End of Mail Form in the Admin  ======*/

        /* ***********************************************************************************
         * ARRANGEMENT TABLE
         * ***********************************************************************************
         */
        //Set state and county


        # Set package
        if(isset($user_session['packageSelected']['package']) and is_array($user_session['packageSelected']['package']) ){
            # Package price
            $this->report->setMessage()->setMessage(' Selected Package');
            #Set package for admin
            $this->report->setAdminMessage()->setAdminMessage('Selected Package ');
            $packages = $this->mpackage->getData();
            foreach($user_session['packageSelected']['package'] as $id => $package){
                //message
                $this->report->setMessage($package['name'] ,'$'.number_format($package['price'],2));
                //for admin
                $this->report->setAdminMessage($package['name'] ,'$'.number_format($package['price'] ,2), array(
                    'action' => 'showdetail',
                    'detail' =>  $packages[$id]['full_detail'].'<br><br>'
                ));
            }
        }

         # Service
        $headline = 'State and Transportation fees ';
        $this->report->setMessage()->setMessage( $headline );
        $this->report->setAdminMessage()->setAdminMessage( $headline );

        if(isset($user_session['packageSelected']['counties']) and is_array($user_session['packageSelected']['counties']) ){
            $countyTitle = 'County';
            $countyValue = getFirstKey($user_session['packageSelected']['counties']);
            //County
            $this->report->setMessage($countyTitle , $countyValue);
        }

        if(isset($user_session['packageSelected']['transport']) and is_array($user_session['packageSelected']['transport']) and
            sizeof($user_session['packageSelected']['transport']) > 0){
            $services = $this->mserives->getByType('transport');

            foreach($user_session['packageSelected']['transport'] as $id => $service ){
                $title = "";
                $subTitle = "";
                $summary = '$'.number_format($service['summary'] ,2);
                $title = $service['name'];
                if($id == 2){
                    $title = $service['name'] .": ".$service['quan'] . " at $".number_format( $service['price'] ,2);
                    $title .= " each";
                    if(@$service['shipping']) {
                        $title .= ", Shipping $".number_format( $service['shipping'] ,2);
                    }
                } else if ($id == 14){
                    $title = $service['name'] . "(" . $this->cfg['deathOccurOption'][ $service['choice'] ]['name'] .")";
                }
                $this->report->setMessage($title, $summary);
                if($services[$service['id']]['show_detail'] == 0){
                    $this->report->setAdminMessage($title , $summary);
                } else{
                    $this->report->setAdminMessage($title , $summary , array(
                            'action' => 'showdetail',
                            'detail' =>  $services[$service['id']]['description']
                    ));
                }
            }
        }

        if(isset($user_session['packageSelected']['handling']) and is_array($user_session['packageSelected']['handling']) and
            sizeof($user_session['packageSelected']['handling']) > 0){
            $services = $this->mserives->getByType('handling');
            # Service
            $headline = "Handling of cremated remains";
            $this->report->setMessage()->setMessage($headline);
            $this->report->setAdminMessage()->setAdminMessage($headline);

            foreach($user_session['packageSelected']['handling'] as $id => $service ){
                $title = "";
                $subTitle = "";
                $summary = '$'.number_format($service['summary'] ,2);

                $title = $service['name'];
                $this->report->setMessage($title, $summary);
                if($services[$service['id']]['show_detail'] == 0){
                    $this->report->setAdminMessage($title , $summary);
                } else{
                    $this->report->setAdminMessage($title , $summary , array(
                            'action' => 'showdetail',
                            'detail' =>  $services[$service['id']]['description']
                    ));
                }
            }
        }

        if(isset($user_session['packageSelected']['addition']) and is_array($user_session['packageSelected']['addition']) and
            sizeof($user_session['packageSelected']['addition']) > 0){
            $services = $this->mserives->getByType('addition');

            $headline = "Customize your plan";
            $this->report->setMessage()->setMessage($headline);
            $this->report->setAdminMessage()->setAdminMessage($headline);

            foreach($user_session['packageSelected']['addition'] as $id => $service ){
                $title = "";
                $subTitle = "";
                $summary = '$'.number_format((float)$service['summary'] ,2);

                $title = $service['name'];
                $this->report->setMessage($title, $summary);
                if($services[$service['id']]['show_detail'] == 0){
                    $this->report->setAdminMessage($title , $summary);
                } else{
                    $this->report->setAdminMessage($title , $summary , array(
                            'action' => 'showdetail',
                            'detail' =>  $services[$service['id']]['description']
                    ));
                }
            }
        }

        if(isset($user_session['cart']) and is_array($user_session['cart']) and count($user_session['cart']) > 0){
            # Merchandise
            $this->report->setMessage()->setMessage('Merchandise Selected');

            #//for admin
            $products = $this->mproduct->getAll();
            $this->report->setAdminMessage()->setAdminMessage('Merchandise Selected');
            foreach($user_session['cart'] as $key => $items){
                foreach($items as $pid => $row){
                    $type = $products[$pid]['type'];
                    $summary         = ($row['summary'] < 1) ? "Included" : "$".number_format($row['summary'] , 2);
                    $large_image_url = pathImage( $type."/".$pid."_image.jpg" );
                    $thumb_image_url = pathImage( $type."/".$pid."_thumbnail.jpg" );
                    $title = $row['name'];
                    if($row['quan'] > 1){
                        $title  = $row['quan'].'x '.$title ;
                    }
                    $this->report->setMessage($title , $summary);
                    $showImage = ($key != "merchandise") ? array(
                            'action' => 'showlargeimage',
                            'url'    => $large_image_url,
                            'thumb'  => $thumb_image_url,
                    ) : null ;
                    $this->report->setAdminMessage( $title, $summary , $showImage);
                }
            }
        }

        # Calculate summary
        $this->report->setMessage()->setMessage('Summary');
        $this->report->setAdminMessage()->setAdminMessage('Summary');


        $total  = "$";
        $total .= number_format(Vars::get($user_session['summary']['total'] , 0), 2);
        $this->report->setMessage('Total' , $total);
        $this->report->setAdminMessage('Total',$total);

        $this->session->set_user('admintable', $this->report->getAdminMessage() );
        $this->session->set_user('arrangementtable', $this->report->getMessage());


        /* ***********************************************************************************
         * DECEDENT TABLE
         * ***********************************************************************************
         */
        $this->report->clearAllMessage();
        # Decedent's Name
        $label      = ($user_session['pkgtype'] == "atneed") ? "Decedent's Name" : "Member's Name";

        $decedent_name = Vars::get($user_session[self::paymentprefix]['person'])." ";
        $this->session->set_user('decedent_name', $decedent_name );
        $this->report->setMessage($label , $decedent_name);

        if(isset($user_session[self::vitalprefix]['sex'])){
            $label       = "Gender";
            $this->report->setMessage($label , $user_session[self::vitalprefix]['sex'] );
        }

        if($user_session['pkgtype'] == "atneed"){
            $label           = "Date of Death";
            $death_of_months = Vars::get($user_session[self::paymentprefix]['mod']);
            $death_of_date   = Vars::get($user_session[self::paymentprefix]['dod']);
            $death_of_year   = Vars::get($user_session[self::paymentprefix]['yod']);
            $this->report->setMessage($label , $death_of_months." ".$death_of_date." , ".$death_of_year);
        }

        if(isset($user_session[self::vitalprefix])){
            $label                   = "Date of Birth";
            $birth_of_months         = Vars::get($user_session[self::vitalprefix]['mob']);
            $birth_of_date           = Vars::get($user_session[self::vitalprefix]['dob']);
            $birth_of_years          = Vars::get($user_session[self::vitalprefix]['yob']);
            $this->report->setMessage($label , $birth_of_months." ".$birth_of_date." , ".$birth_of_years);
        }

        $this->session->set_user('decedenttable', $this->report->getMessage());

        /* ***********************************************************************************
         * CONTACT TABLE
         * ***********************************************************************************
         */
        $this->report->clearAllMessage();
        # Name
        $name = Vars::get($user_session[self::paymentprefix]['informant_name']);
        $this->report->setMessage('Name', $name);

        # Email
        $email = Vars::get($user_session[self::paymentprefix]['informant_email']);
        $this->report->setMessage('Email', $email);

        # Phone
        $phone = Vars::get($user_session[self::paymentprefix]['informant_phone']);
        $this->report->setMessage('Phone', $phone);
        $this->session->set_user('contacttable', $this->report->getMessage());


        /* ***********************************************************************************
         * Set before send email
         * ***********************************************************************************
         */
        $uniqid = Vars::get($user_session['uniqid']);
        $replaceText = "Your Payment";
        $subjectText = "Thank You for #action | Arrangement ID ".$uniqid;
        if(Vars::get($user_session[self::paymentprefix]['paybycash']) != "credit card"){
            $replaceText = "Your Arrangement";
            $subjectText = "Thank You for #action | ID ".$uniqid;
        }

        if($user_session['pkgtype'] == "atneed"){
            $typeText = 'Immediate Need';
        }
        else if($user_session['pkgtype'] == "preneed"){
            $typeText       = 'Advanced Planning';
        }
        $subjectText = str_replace("#action" , $replaceText , $subjectText );
        $user_session = $this->session->set_user('typetext', $typeText );
        $user_session = $this->session->set_user('subjectText', $subjectText );
        $user_session = $this->session->set_user('funeralhome', $this->settings['client_company_name']);
        $user_session = $this->session->set_user('funeralhomefull', $this->settings['client_company_name_full']);
        $user_session = $this->session->set_user('email_form', $this->settings['email_'.$user_session['pkgtype'].'_from']  );

        $user_session = $this->session->set_user('go_back', base_url()."vital/?hash=");
        if(isset($user_session['hash_url_encoded'])){
            $user_session = $this->session->set_user('go_back', $user_session['go_back'].$user_session['hash_url_encoded'] );
        }

        $email_to     = array();//$this->settings['email_'.$user_session['pkgtype'].'_to'];
        $user_email   = NULL;
        if(isset($user_session[self::paymentprefix]['informant_email']) ){
            $user_email = $user_session[self::paymentprefix]['informant_email'];
            $email_to  = array_merge($email_to , array($user_email));
        }

        if(!is_debugger($user_email)){
            $email_to  = array_merge($email_to , $this->settings['email_'.$user_session['pkgtype'].'_to'] );
        }

        $this->session->set_user('email_to', $email_to);
        $this->session->set_user('phonenumber', $this->settings['client_phone_local'] );
        if(isset($user_session[self::paymentprefix]['person'])){
            $this->session->set_user('decedent_name' , $user_session[self::paymentprefix]['person'] );
        }
    }

    protected function sendCompleteEmail(){
        $user_session = $this->session->get_user();
        //Send Email
        $this->email_lib
                ->setemailTo($user_session['email_to'])
                ->setemailFrom( $user_session['email_form'] )
                ->setSubject("Thank You For ".$user_session['subjectText'])
                ->setTemplate('assets/templates/'.$user_session['pkgtype'].'/complete.tmpl.html' , $user_session)
                ->send();
    }

    protected function load_step($target, $page_title ='', $params = array()){

        # Calculate summary before load the view.
        $this->_calculate_amount_due();
        $this->_notify_message_preparation();

        # Generate variable from user session.
        $this->data['vitalprefix'] = self::vitalprefix;
        $this->data['authprefix'] = self::authprefix;
        $this->data['paymentprefix'] = self::paymentprefix;
        $this->data['user_session'] = $this->session->get_user();
        $this->data['cfg'] = $this->cfg;
        $this->data['settings'] = $this->settings;

        # Generate wrapper.
        $wrapper = $this->get_wrapper(array(
            'page_title' => $page_title,
            'printview' => (isset($params['printview'])) ? true : false
        ));

        $this->data['header'] = $wrapper['header'];
        $this->data['footer'] = $wrapper['footer'];
        $this->data['sidebar'] = $this->load->view('sidebar', $this->data , true);
        $this->data['params'] = $params;
        $this->data['target_view'] = $this->load->view($target , $this->data , true);

        # Display page step
        $this->load->view('wrapper', $this->data );
    }

}


?>

