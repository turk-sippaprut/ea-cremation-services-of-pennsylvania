<?php 
use JBZoo\Utils\Vars;
use JBZoo\Utils\Arr;

class PdfCore {

    protected $_ci;

    protected $client_id;

     /**
     * [$data information for fillable]
     * @var array
     */
    protected $data = [];

    /**
     * Resource for PDF file need to fillable
     * @var string 
     */
    protected $resource = "";

    /**
     * File name for upload 
     * @var string
     */
    protected $file_name = "";

    /**
     * Place for upload file
     * @var string
     */
    protected $destination = "";

    /**
     * Type of the PDF file
     * @var String
     */
    protected $pdfType = '';


    public function __construct()
    {
        $this->_ci =& get_instance();
        $this->_ci->config->load('earrangement');
        $config = $this->_ci->config->config;
        $this->client_id = $config['client_id'];
    }

    public function process()
    {
        $user_session = $this->_ci->session->get_user();

        $config = [
            'file' => $this->resource ,
            'file_name' => $this->file_name , 
            'destination' => $this->destination ,
            'client_id' => $this->client_id
        ];

        $this->_ci->pdftk->initialize($config);
        $reponse = $this->_ci->pdftk->setData( $this->data )->fill();

        if ( $reponse['status'] == 1 )
        {
            $user_session['documents'][$this->pdfType] = array(
                'name' => $this->file_name ,
                'url' => $reponse['file_url'] ,
                'time' => date('Y-m-d H:i:s')
            );
            $this->_ci->session->set_user('documents' , $user_session['documents'] );
            $this->_ci->session->set_user( $this->pdfType . '_document' , $reponse['file_url']);

            return true;
        }
        else 
        {
            $user_session['document_error'][$this->pdfType] = $reponse['message'];
            $this->_ci->session->set_user('document_error' , $user_session['document_error'] );
            return false;
        }

    }
}