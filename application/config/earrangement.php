<?php
$config = array(
	'client_id' => 'cremapa',
	'secure' => 1,
	'root' =>  str_replace(SEVERPATH , '' , FCPATH ),
	'imagesPath' => "/assets/images/",
	'saletax' => 0 ,
	// 'death_certificate' => 9 ,
	'timezone' => 'America/New_York' ,

	'dcc_shipping' => 10,

	// 'authorize' => array(
	// 	'account_id' => '7x7TRNyZX9U7',
	// 	'access_key' => '3f7jQ4fGyvn2E256',
	// ),

	'assets' => array(
		'css' => array(
			'assets/css/build.min.css'
			// 'assets/css/font-awesome.css',
			// 'assets/css/validationEngine.jquery.css',
			// 'assets/css/reset.css' ,
			// 'assets/css/ea.css?_='.strtotime('now') ,
			// 'assets/css/pnotify.custom.css',
			// 'assets/js/magnific/magnific-popup.css' ,
			// 'assets/js/bxslider/jquery.bxslider.css' ,
		),
		'js'  => array(
			'assets/js/build.min.js'
			// 'assets/js/angular.min.js' ,
			// 'assets/js/moblie.js' ,
			// 'assets/js/jquery.validationEngine-en.js' ,
			// 'assets/js/jquery.creditCardValidator.js' ,
			// 'assets/js/jquery.validationEngine.js' ,
			// 'assets/js/formatter/formatter.js' ,
			// 'assets/js/jstz.min.js' ,

			// //'assets/js/moment-with-locales.min.js' ,
			// 'assets/js/moment.min.js' ,
			// 'assets/js/moment-timezone.js' ,

			// 'assets/js/lodash.min.js' ,
			// 'assets/js/pnotify.custom.min.js' ,
			// 'assets/js/earrangement.js' ,
			// 'assets/js/magnific/jquery.magnific-popup.min.js' ,
			// 'assets/js/jQuery.print.js' ,
			// 'assets/js/script.js' ,
			// 'assets/js/util.js' ,
			// 'assets/js/validate.js' ,
			// 'assets/js/fnetea.js' ,
			// 'assets/js/caculator.js' ,
			// 'assets/js/PrettyOption.js' ,
			// 'assets/js/plugin-ea.js' ,
			// 'assets/js/bxslider/jquery.bxslider.min.js'
		) ,
	),


	'breakpoint' => array(
		'xs' => '480',
		'xs-max' => '767',
		'sm' => '768',
		'sm-max' => '991',
		'md' => '992',
		'md-max' => '1199',
		'lg' => '1200',
	),
	'steps' => array(
		'pkg' => array(
			'name' => 'SELECT A PLAN',
			'url' => '',
		),
		'merchandise' => array(
			'name' => 'MERCHANDISE',
			'url'  => 'merchandise',
		),
		'paperwork' => array(
			'name' => 'Paperwork',
			'url' => 'form_vital',
		),
		'overview' => array(
			'name' => 'SUMMARY',
			'url' => 'overview',
		),
		'payment' => array(
			'name' => 'Payment Options',
			'url' => 'payment',
		),
	),

	'memorialPortrait' => array(
		'1' => array('name' => '11"x14" - $140' , "title" => '11x14' , 'price' => 140) ,
		'2' => array('name' => '16"x20" - $170' , "title" => '16x20' , 'price' => 170)
	) ,

	// 'payment_option' => array(
	// 	'credit' => "Pay Online with Credit Card" ,
	// 	'check'  => "Pay Online with Check"
	// ),

	'states' => array(
		'AL' => array("code"=>"AL","name"=>"Alabama","timezone"=>"America/Chicago"),
		'AK' => array("code"=>"AK","name"=>"Alaska","timezone"=>"America/Anchorage"),
		'AZ' => array("code"=>"AZ","name"=>"Arizona","timezone"=>"America/Phoenix"),
		'AR' => array("code"=>"AR","name"=>"Arkansas","timezone"=>"America/Chicago"),
		'AS' => array("code"=>"AS","name"=>"American Samoa","timezone"=>"America/Chicago"),
		'CA' => array("code"=>"CA","name"=>"California","timezone"=>"America/Los_Angeles"),
		'CO' => array("code"=>"CO","name"=>"Colorado","timezone"=>"America/Denver"),
		'CT' => array("code"=>"CT","name"=>"Connecticut","timezone"=>"America/New_York"),
		'DC' => array("code"=>"DC","name"=>"Washington","timezone"=>"America/New_York"),
		'DE' => array("code"=>"DE","name"=>"Delaware","timezone"=>"America/New_York"),
		'FL' => array("code"=>"FL","name"=>"Florida","timezone"=>"America/New_York"),
		'GA' => array("code"=>"GA","name"=>"Georgia","timezone"=>"America/New_York"),
		'GU' => array("code"=>"GU","name"=>"Guam","timezone"=>"America/New_York"),
		'HI' => array("code"=>"HI","name"=>"Hawaii","timezone"=>"Pacific/Honolulu"),
		'ID' => array("code"=>"ID","name"=>"Idaho","timezone"=>"America/Denver"),
		'IL' => array("code"=>"IL","name"=>"Illinois","timezone"=>"America/Chicago"),
		'IN' => array("code"=>"IN","name"=>"Indiana","timezone"=>"America/Indianapolis"),
		'IA' => array("code"=>"IA","name"=>"Iowa","timezone"=>"America/Chicago"),
		'KS' => array("code"=>"KS","name"=>"Kansas","timezone"=>"America/Chicago"),
		'KY' => array("code"=>"KY","name"=>"Kentucky","timezone"=>"America/New_York"),
		'LA' => array("code"=>"LA","name"=>"Louisiana","timezone"=>"America/Chicago"),
		'ME' => array("code"=>"ME","name"=>"Maine","timezone"=>"America/New_York"),
		'MD' => array("code"=>"MD","name"=>"Maryland","timezone"=>"America/New_York"),
		'MA' => array("code"=>"MA","name"=>"Massachusetts","timezone"=>"America/New_York"),
		'MI' => array("code"=>"MI","name"=>"Michigan","timezone"=>"America/New_York"),
		'MN' => array("code"=>"MN","name"=>"Minnesota","timezone"=>"America/Chicago"),
		'MO' => array("code"=>"MO","name"=>"Missouri","timezone"=>"America/Chicago"),
		'MP' => array("code"=>"MP","name"=>"Northern Mariana Islands","timezone"=>"America/Chicago"),
		'MS' => array("code"=>"MS","name"=>"Mississippi","timezone"=>"America/Chicago"),
		'MT' => array("code"=>"MT","name"=>"Montana","timezone"=>"America/Denver"),
		'NE' => array("code"=>"NE","name"=>"Nebraska","timezone"=>"America/Chicago"),
		'NV' => array("code"=>"NV","name"=>"Nevada","timezone"=>"America/Los_Angeles"),
		'NH' => array("code"=>"NH","name"=>"New Hampshire","timezone"=>"America/New_York"),
		'NJ' => array("code"=>"NJ","name"=>"New Jersey","timezone"=>"America/New_York"),
		'NM' => array("code"=>"NM","name"=>"New Mexico","timezone"=>"America/Denver"),
		'NY' => array("code"=>"NY","name"=>"New York","timezone"=>"America/New_York"),
		'NC' => array("code"=>"NC","name"=>"North Carolina","timezone"=>"America/New_York"),
		'ND' => array("code"=>"ND","name"=>"North Dakota","timezone"=>"America/Chicago"),
		'OH' => array("code"=>"OH","name"=>"Ohio","timezone"=>"America/New_York"),
		'OK' => array("code"=>"OK","name"=>"Oklahoma","timezone"=>"America/Chicago"),
		'OR' => array("code"=>"OR","name"=>"Oregon","timezone"=>"America/Los_Angeles"),
		'PA' => array("code"=>"PA","name"=>"Pennsylvania","timezone"=>"America/New_York"),
		'PR' => array("code"=>"PR","name"=>"Puerto Rico","timezone"=>"America/New_York"),
		'PW' => array("code"=>"PW","name"=>"Palau","timezone"=>"America/New_York"),
		'RI' => array("code"=>"RI","name"=>"Rhode Island","timezone"=>"America/New_York"),
		'SC' => array("code"=>"SC","name"=>"South Carolina","timezone"=>"America/New_York"),
		'SD' => array("code"=>"SD","name"=>"South Dakota","timezone"=>"America/Chicago"),
		'TN' => array("code"=>"TN","name"=>"Tennessee","timezone"=>"America/Chicago"),
		'TX' => array("code"=>"TX","name"=>"Texas","timezone"=>"America/Chicago"),
		'UT' => array("code"=>"UT","name"=>"Utah","timezone"=>"America/Denver"),
		'VA' => array("code"=>"VA","name"=>"Virginia","timezone"=>"America/New_York"),
		'VI' => array("code"=>"VI","name"=>"Virgin Islands","timezone"=>"America/New_York"),
		'VT' => array("code"=>"VT","name"=>"Vermont","timezone"=>"America/New_York"),
		'WA' => array("code"=>"WA","name"=>"Washington","timezone"=>"America/Los_Angeles"),
		'WV' => array("code"=>"WV","name"=>"West Virginia","timezone"=>"America/New_York"),
		'WI' => array("code"=>"WI","name"=>"Wisconsin","timezone"=>"America/Chicago"),
		'WY' => array("code"=>"WY","name"=>"Wyoming","timezone"=>"America/Denver"),
	),

	'counties' => array(
		'Lancaster'      => array( 'name' => 'Lancaster' , 'miles' => 'n/a' , 'rate' => 0  , 'delivery' => true, 'delivery_price' => 75 ) ,
		'Adams'          => array( 'name' => 'Adams' , 'miles' => 62 , 'rate' => 190  , 'delivery' => false ) ,
		'Berks'          => array( 'name' => 'Berks' , 'miles' => 33 , 'rate' => 100  , 'delivery' => true ) ,
		'Bucks'          => array( 'name' => 'Bucks' , 'miles' => 93 , 'rate' => 280  , 'delivery' => false )  ,
		'Chester'        => array( 'name' => 'Chester' , 'miles' => 33 , 'rate' => 100  , 'delivery' => true )  ,
		'Cumberland'     => array( 'name' => 'Cumberland' , 'miles' => 67 , 'rate' => 210  , 'delivery' => false )  ,
		'Dauphin'        => array( 'name' => 'Dauphin' , 'miles' => 36 , 'rate' => 110  , 'delivery' => true )  ,
		'Juniata'        => array( 'name' => 'Juniata' , 'miles' => 88 , 'rate' => 270  , 'delivery' => false )  ,
		'Lebanon'        => array( 'name' => 'Lebanon' , 'miles' => 31 , 'rate' => 100  , 'delivery' => false )  ,
		'Lehigh'         => array( 'name' => 'Lehigh' , 'miles' => 68 , 'rate' => 210  , 'delivery' => false )  ,
		'Montgomery'     => array( 'name' => 'Montgomery' , 'miles' => 82 , 'rate' => 250  , 'delivery' => false )  ,
		'Northumberland' => array( 'name' => 'Northumberland' , 'miles' => 100  , 'rate' => 300  , 'delivery' => false )  ,
		'Perry'          => array( 'name' => 'Perry' , 'miles' => 67 , 'rate' => 210  , 'delivery' => false )  ,
		'Philadelphia'   => array( 'name' => 'Philadelphia' , 'miles' => 80  , 'rate' => 240  , 'delivery' => false )  ,
		'Schuylkill'     => array( 'name' => 'Schuylkill' , 'miles' => 66 , 'rate' => 200  , 'delivery' => false )  ,
		'Snyder'         => array( 'name' => 'Snyder' , 'miles' => 87 , 'rate' => 270  , 'delivery' => false )  ,
		'York'           => array( 'name' => 'York' , 'miles' => 36 , 'rate' => 110  , 'delivery' => true )
	) ,

	'deathOccurOption' => array(
		'hospital'         => array( 'name' => 'Hospital' , 'price' => '0' ) ,
		'hospice_facility' => array( 'name' => 'Hospice Facility' , 'price' => '0' ) ,
		'nursing'          => array( 'name' => 'Nursing Home' , 'price' => '0' ) ,
		'office' 		   => array('name' => 'Coroner’s Office' , 'price' => '0'),
		'home_residence'   => array( 'name' => 'Home Residence' , 'price' => '200' )
	) ,


	'month' => array(
		"01" => "January"  ,
		"02" =>	"February" ,
		"03" =>	"March"    ,
		"04" =>	"April"	   ,
		"05" =>	"May"	   ,
		"06" =>	"June"     ,
		"07" =>	"July"     ,
		"08" =>	"August"   ,
		"09" =>	"September",
		"10" =>	"October"  ,
		"11" =>	"November" ,
		"12" =>	"December"
	),

	'education' => array(
		'8th'       => array("name" => "8th Grade or Less" , "title" => "8&#x1D57;&#x02B0; Grade or Less") ,
		'9th'       => array("name" => "No diploma, 9-12 gr" , "title" => "No diploma, 9-12 gr") ,
		'hs'        => array("name" => "High School Grad/GED" , "title" => "High School Grad/GED") ,
		'no'        => array("name" => "Some college credit, no degree" , "title" => "Some college credit, no degree") ,
		'associate' => array("name" => "Associates Degree" , "title" => "Associates Degree") ,
		'bachelor'  => array("name" => "Bachelor’s Degree" , "title" => "Bachelor’s Degree") ,
		'master'    => array("name" => "Master’s Degree" , "title" => "Master’s Degree") ,
		'phd'       => array("name" => "Doctorate or Professional Degree" , "title" => "Doctorate or Professional Degree") ,
	),

	'place_death_choice' => array(
		'Hospital Inpatient',
		'Emerg. Rm.',
		'Nursing Home',
		'Hospice',
		'Residence',
		'Other'
	),

	'decedent_hispanic_choice' => array(
		'not_spanish'  => array('name' => 'No, NOT Spanish/Hispanic/Latino'),
		'yes_mexican'  => array('name' => 'Yes, Mexican/Mexican American/Chicano'),
		'puerto_rican' => array('name' => 'Yes, Puerto Rican'),
		'cuban'        => array('name' => 'Yes, Cuban'),
		'other'        => array('name' => 'Yes, Other (specify)'),
	),

	'race_choice' => array(
		'white'  => array('name' => 'White'),
		'black'  => array('name' => 'Black or African American'),
		'alaska'  => array('name' => 'American Indian or Alaska Native'),
		'indian'  => array('name' => 'Asian Indian'),
		'chinese'  => array('name' => 'Chinese'),
		'filipino'  => array('name' => 'Filipino'),
		'japanese'  => array('name' => 'Japanese'),
		'korean'  => array('name' => 'Korean'),
		'vietnamese'  => array('name' => 'Vietnamese'),
		'other_asian'  => array('name' => 'Other Asian'),
		'hawaiian'  => array('name' => 'Native Hawaiian'),
		'other'  => array('name' => 'Other (Specify)'),
	),

	'includedProduct' => array(
		28 , //container
		45
	) ,

	// 'paymentOption' => array(
	// 	'atneed' => array(
	// 		'visa'       => 'VISA' ,
	// 		'mastercard' => 'MasterCard' ,
	// 		'discover'   => 'Discover' ,
	// 		'amex'       => 'American Express' ,
	// 		'IA'         => 'Insurance Assignment'
	// 	) ,
	// 	'preneed' => array(
	// 		'visa'       => 'VISA' ,
	// 		'mastercard' => 'MasterCard' ,
	// 		'eft'        => 'EFT (Electronic Fund Transfer) Checkings or Savings'
	// 	)
	// ) ,

	'documents' => array(
		"sogs" => array(
			'name'=> "Statement of Goods and Services (PDF)" ,
			'source' => 'assets/pdf/sogs.pdf'
		) ,
		"vital"       => array(
			'name'   => "Information Worksheet Form (PDF)" ,
			'source' => 'assets/pdf/Vital_Stats.pdf'
		) ,
		"auth"        => array(
			'name'   => "Cremation Authorization (PDF)" ,
			'source' => 'assets/pdf/Cremation_Authorization.pdf'
		)
	)



	// 'formPDF' => array(
	// 	"vital"     => array('name'=> "form_vital.pdf" , 'target' => 'form_vital') ,
	// 	"auth"      => array('name'=> "form_auth.pdf" , 'target' => 'form_auth') ,
	// 	"embalming" => array('name'=> "form_embalming.pdf" , 'target' => 'form_embalming') ,
	// 	//'general'   => array('name'=> "form_general.pdf" , 'target' => 'form_general') ,
	// 	"remains"   => array('name'=> "form_remains.pdf" , 'target' => 'form_remains') ,
	// 	"agreement" => array('name'=> "form_agreement.pdf" , 'target' => 'form_agreement') ,
	// 	"sogs"      => array('name'=> "sogs.pdf" ,  'target' => 'sogs')
 // 	)



);
