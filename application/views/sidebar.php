<div class="col-xs-offset-0 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-1">
    <div class="ea-progress-step">
        <?php
        $index = 1;
        foreach($cfg['steps'] as $key => $row){
            $active = ($key == $sidebarActive) ? 'ea-step-active' : null;
        ?>
        <div class="ea-step <?php echo $active;?>"><?php echo $index;?>. <?php echo $row['name'];?></div>
        <?php $index++;
        } ?>
        <div class="ea-step ea-step-dark"><span class="step-shopping-cart">My Shopping Cart</span></div>
        <div class="cartList"></div>

    </div>
</div>