<div class="col-sm-7 col-md-7">
    <h1>Paperwork - Step 1</h1>
    <form action="" id="mainform" name="mainform" method="POST">
        <input type="hidden" name="pkgtype"             id="pkgtype"        value="<?php echo $user_session['pkgtype'];?>" />
        <input type="hidden" name="scriptaction"        id="scriptaction"   value="validate" />
        <input type="hidden" name="notvalidate"         id="notvalidate"    value="" />
        <input type="hidden" name="next_form"           id="next_form"      value="<?php echo $next_form?>" />
        <input type="hidden" name="session_index"       id="session_index"  value="<?php echo $session_index?>" />
        <input type="hidden" name="selected"            id="selected"       value="<?php echo $$session_index;?>" />


    <div class="ea-content">
        <div id="biographical-information-form-area" class="biographicalForms">
            <div style="padding-top:10px;" class="formGroupTitle">
                Biographical Information
            </div>
            <div class="text-plan-merch">All fields below are required to file a death certificate. If you do not know the information and cannot obtain it, please put ‘NA’ in the required field to move on.</div>
            <div class="eaFormBox">
                <?php
                $titleFullName = "Full Name of Deceased";
                if($user_session['pkgtype'] == "preneed"){
                    $titleFullName = "Whom are the arrangements for (enter name)?";
                }
                ?>
                <?php echo $titleFullName; ?>

                <div class="eaFormField">
                    <input type="text" name="deceased_first" class="text validate[required]" value="">
                    <span class="text-bottom-input">(First)</span><br />

                    <input type="text" name="deceased_middle" class="text" value="">
                    <span class="text-bottom-input">(Middle)</span><br />

                    <input type="text" name="deceased_last" class="text validate[required]" value="">
                    <span class="text-bottom-input">(Last)</span>
                </div>
            </div>
            <div class="eaFormBox">
                Street Address:
                <div class="eaFormField">
                    <input type="text" name="deceased_address" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                City/Town:
                <div class="eaFormField">
                    <input type="text" name="deceased_city" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                County of Usual Residence:
                <div class="eaFormField">
                    <input type="text" name="deceased_county" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                State:
                <div class="eaFormField">
                    <span class="payment-options">
                        <select name="address_state" class="input-dropdown validate[required]">
                            <option value="" >Select State</option>
                            <?php foreach($states as $state => $arr){ ?>
                                <option  value="<?php echo $state;?>"><?php echo $state;?></option>
                            <?php } ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="eaFormBox">
                Zip Code:
                <div class="eaFormField">
                    <input type="text" name="deceased_zip" class="text validate[required]">
                </div>
            </div>

            <div class="eaFormBox">
                Social Security Number:
                <div class="eaFormField">
                    <input type="text" name="ssn" class="text ">
                </div>
            </div>
            <div class="eaFormBox">
                Date of Birth:
                <div class="eaFormField">
                        <span class="payment-options col-xs-5">
                        <select style="width: 98%;" id="mob" name="mob" class="validate[required]">
                            <option  value="">Month</option>
                            <?php foreach($cfg['month'] as $monthNumber => $month){?>
                                <option value="<?php echo $month;?>"><?php echo $month;?></option>
                            <?php } ?>
                        </select>
                        </span> <span class="payment-options col-xs-3">
                        <select style="width: 97%;" id="dob" name="dob" class="validate[required]">
                            <option value="">Day</option>
                            <?php for($day= 1; $day<= 31; $day++){
                                $day = substr($day, -2);
                            ?>
                            <option  value="<?php echo $day;?>"><?php echo $day;?></option>
                            <?php } ?>
                        </select>
                        </span> <span class="payment-options col-xs-4">
                        <select style="width: 98%;" id="yob" name="yob" class="validate[required]">
                            <option  value="">Year</option>
                            <?php
                                $currentYear = date("Y");
                                for($year = $currentYear; $year >= 1900; $year-- ){
                            ?>
                                    <option  value="<?php echo $year;?>"><?php echo $year;?></option>
                            <?php } ?>
                        </select>
                        </span>
                    </div>
            </div>

            <?php if($user_session['pkgtype'] == "atneed"){ ?>
            <div class="eaFormBox">
                Date of Death
                <div class="eaFormField">
                    <span class="payment-options col-xs-5" >
                        <select  id="month_of_death" name="mod" data-prompt-position="topLeft" readonly class="validate[required] col-md-12">
                            <option value="">Month</option>
                            <?php foreach($cfg['month'] as $monthNumber => $month) { ?>
                                    <option value="<?php echo $month;?>"><?php echo $month;?></option>
                            <?php } ?>
                        </select>
                    </span>
                    <span class="payment-options col-xs-3" >
                        <select  id="day_of_death" name="dod" data-prompt-position="topLeft" readonly class="validate[required] col-md-12">
                            <option selected="" value="">Day</option>
                            <?php for($day= 1; $day<= 31; $day++){
                                    $day = substr($day, -2);
                            ?>
                                <option  value="<?php echo $day;?>"><?php echo $day;?></option>
                            <?php } ?>
                        </select>
                    </span>
                    <span class="payment-options col-xs-4" >
                        <select  id="year_of_death" name="yod" data-prompt-position="topLeft" class="validate[required] col-md-12">
                            <?php for($year = date("Y"); $year >= 1900; $year--){
                                $selected = ($year == date("Y")) ? 'selected' : NULL;
                            ?>
                            <option <?php echo $selected;?> value="<?php echo $year;?>"><?php echo $year;?></option>
                            <?php }?>
                        </select>
                    </span>
                </div>
            </div>
            <?php } ?>

            <div class="eaFormBox">
                Sex:
                <div class="eaFormField">
                    <span class="planningRadio">
                        <span class="optionRadioInline">
                            <input type="radio" id="femaleRadio" name="sex" value="Female" class="validate[required] radio">
                            <label for="femaleRadio">Female</label>
                        </span>
                        <span class="optionRadioInline">
                            <input  type="radio" id="maleRadio" name="sex" value="Male" class="validate[required] radio">
                            <label for="maleRadio">Male</label>
                        </span>
                   </span>
                </div>
            </div>

            <div class="eaFormBox">
                Birth Place (City/Town, State or Foreign Country):
                <div class="eaFormField">
                    <input type="text" name="bPlace" class="text validate[required]">
                </div>
            </div>

            <div class="eaFormBox">
                Father’s Full Name:
                <div class="eaFormField">
                    <input type="text" name="fatherName" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                Mother’s Full Name(Include Maiden):
                <div class="eaFormField">
                    <input type="text" name="motherName" class="text validate[required]">
                </div>
            </div>

            <div class="eaFormBox">
                Marital Status
                <div class="eaFormField">
                    <span class="planningOptions">
                        <select name="marital" id="marital" class="validate[required]" >
                            <option value="Single">Single</option>
                            <option value="Married">Married</option>
                            <option value="Widowed">Widowed</option>
                            <option value="Divorced">Divorced</option>
                            <option value="Separated">Separated</option>
                            <option value="Never Married">Never Married</option>
                        </select>
                  </span>
                </div>
             </div>

            <div class="eaFormBox">
                Date of Marriage (If Married at Time of Death):
                <div class="eaFormField">
                        <span class="payment-options col-xs-5">
                        <select style="width: 98%;" id="mom" name="mom" >
                            <option selected="" value="">Month</option>
                            <?php foreach($cfg['month'] as $monthNumber => $month){?>
                                    <option  value="<?php echo $monthNumber;?>"><?php echo $month;?></option>
                            <?php } ?>
                        </select>
                        </span> <span class="payment-options col-xs-3">
                        <select style="width: 97%;" id="dom" name="dom" >
                            <option  value="">Day</option>
                            <?php
                                for($day= 1; $day<= 31; $day++){
                                    $day = substr($day, -2);
                                ?>
                                    <option value="<?php echo $day;?>"><?php echo $day;?></option>
                                <?php
                                }
                            ?>
                        </select>
                        </span> <span class="payment-options col-xs-4">
                        <select style="width: 98%;" id="yom" name="yom" >
                            <option selected="" value="">Year</option>
                            <?php
                            $currentYear = date("Y");
                            for($year = $currentYear; $year >= 1900; $year-- ){
                            ?>
                                <option value="<?php echo $year;?>"><?php echo $year;?></option>
                            <?php
                            }
                            ?>
                        </select>
                        </span>
                    </div>
            </div>
            <div class="eaFormBox">
                Surviving Spouse’s Name (Maiden Name):
                <div class="eaFormField">
                    <?php $validateClass = (in_array( Vars::get($vital['marital']) , ['Married' , 'Separated'] )) ? 'validate' : null;?>
                    <input type="text" name="spouse_name" class="text <?php echo $validateClass;?>">
                </div>
            </div>
            <div class="eaFormBox">
                List any Other Cities of Residence:
                <div class="eaFormField">
                    <input type="text" name="spouse_residence" class="text">
                </div>
            </div>
            <div class="eaFormBox">
                Education:
                <div class="eaFormField">
                    <span class="payment-options">
                    <select id="education" name="education" class="validate[required]">
                        <?php foreach($cfg['education'] as $key => $row){ ?>
                        <option value="<?php echo $key;?>"><?php echo $row['title'];?></option>
                        <?php } ?>
                    </select>
                    </span>
                </div>

            </div>
            <div class="eaFormBox">
                Race/Origin:
                <div class="eaFormField">
                    <input type="text" name="race" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                Military Service:
                <div class="eaFormField">
                    <span class="planningRadio">
                        <input id="military_yes" class="validate[required] radio" type="radio" name="military" value="Yes" data-status="not-checked">
                        <label for="military_yes">Yes</label><br style="clear:both;" />

                        <input id="Military_Service2" class="validate[required] radio" type="radio" name="military" value="No" data-status="not-checked">
                        <label for="Military_Service2">No</label><br style="clear:both;" />

                        <input id="Military_Service3" class="validate[required] radio" type="radio" name="military" value="Unsure"
                        data-status="not-checked">
                        <label for="Military_Service3">Unsure</label>
                   </span>
               </div>
            </div>
            <!-- Start Detail Miltary Service -->
            <div class="military-service-detail <?php echo $militaryStatus;?>">
                <div class="eaFormBox"><i>If <u>Yes</u> then please fill out the following</i></div>
                <div class="eaFormBox">
                   Branch:
                    <div class="eaFormField">
                        <input type="text" name="branch" class="text military-required">
                    </div>
                </div>
                <div class="eaFormBox">
                   Rank:
                    <div class="eaFormField">
                        <input type="text" name="rank" class="text military-required">
                    </div>
                </div>
                <div class="eaFormBox">
                   Enlistment Date/Place:
                    <div class="eaFormField">
                        <input type="text" name="enlistment" class="text military-required">
                    </div>
                </div>
                <div class="eaFormBox">
                   Discharge Date/Place:
                    <div class="eaFormField">
                        <input type="text" name="discharge" class="text military-required">
                    </div>
                </div>
                <div class="eaFormBox">
                   Service Number:
                    <div class="eaFormField">
                        <input type="text" name="serviceNumber" class="text military-required">
                    </div>
                </div>
                <div class="eaFormBox">
                   Service in War:
                    <div class="eaFormField">
                        <input type="text" name="war" class="text military-required">
                    </div>
                </div>
            </div>
            <!-- End Detail Miltary Service -->
                <div class="eaFormBox">
                   Occupation:
                    <div class="eaFormField">
                        <input type="text" name="occupation" class="text">
                    </div>
                </div>
                <div class="eaFormBox">
                   Employer:
                    <div class="eaFormField">
                        <input type="text" name="employer" class="text">
                    </div>
                </div>
                <div class="eaFormBox">
                   Position Held:
                    <div class="eaFormField">
                        <input type="text" name="position" class="text">
                    </div>
                </div>
                <div class="eaFormBox">
                   Length of Employment:
                    <div class="eaFormField">
                        <input type="text" name="lengthEmp" class="text">
                    </div>
                </div>
                <div class="eaFormBox">
                   Retirement Date:
                    <div class="eaFormField">
                        <input type="text" name="retirement" class="text">
                    </div>
                </div>



            <div style="padding-top:10px;" class="formGroupTitle">
                Survivor Information/Next of Kin
            </div>
            <div class="eaFormBox">
                Full Name of Next of Kin (Person responsible for this information):
                <div class="eaFormField">
                   
                    <input type="text" name="informant_name" class="text validate[required]" value="">
                    <span class="text-bottom-input">(First)</span><br />
                    <input type="text" name="informant_lastname" class="text validate[required]" value="">
                    <span class="text-bottom-input">(last)</span><br />
                </div>
            </div>
            <div class="eaFormBox">
               Street Address:
                <div class="eaFormField">
                    <input type="text" name="informant_address" class="text validate[required]" value="">
                </div>
            </div>
            <div class="eaFormBox">
                City:
                <div class="eaFormField">
                    <input type="text" name="informant_city" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                State:
                <div class="eaFormField">
                    <span class="payment-options">
                        <select name="informant_state" class="input-dropdown validate[required]">
                            <option value="" >Select State</option>
                            <?php foreach($cfg['states'] as $state => $row){ ?>
                            <option value="<?php echo $state;?>"><?php echo $state;?></option>
                            <?php } ?>
                        </select>
                    </span>
                </div>
            </div>
            <div class="eaFormBox">
                Zip Code:
                <div class="eaFormField">
                    <input type="text" name="informant_zip" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
               Phone:
                <div class="eaFormField">
                    <input type="text" name="informant_phone" class="text validate[required] phoneFormat">
                </div>
            </div>
            <div class="eaFormBox">
               Email:
                <div class="eaFormField">
                    <input type="text" name="informant_email" data-value="sip@frazerconsultants.com" class="text validate[required ,custom[email]]"
                    value=""  />
                </div>
            </div>

            <div class="eaFormBox">
               Physician Name (Optional):
                <div class="eaFormField">
                    <input type="text" name="physician_name" class="text">
                </div>
            </div>
            <div class="eaFormBox">
               Address:
                <div class="eaFormField">
                    <input type="text" name="physician_address" class="text">
                </div>
            </div>
            <div class="eaFormBox">
               Phone:
                <div class="eaFormField">
                    <input type="text" name="physician_phne" class="text phoneFormat">
                </div>
            </div>
            <div class="formGroupTitle" style="padding-top:10px;">
                Disposition of Remains Preference
            </div>
            <div class="eaFormBox">
                <div class="eaFormField">
                    <span class="planningRadio">

                        <input id="burialChoice" type="radio" name="remains" value="Burial" class="validate[required] radio" data-status="not-checked">
                        <label for="burialChoice">Cremation Burial</label><br style="clear:both;" />

                        <input id="entombmentChoice" type="radio" name="remains"
                        value="Entombment" class="validate[required] radio" data-status="not-checked">
                        <label for="entombmentChoice">Cremation Entombment</label>  <br style="clear:both;" />

                        <input id="residenceChoice" type="radio" name="remains" value="Residence" class="validate[required] radio"
                        data-status="not-checked">
                        <label for="residenceChoice">Cremation Residence</label>  <br style="clear:both;" />

                        <input id="scatteringChoice" type="radio" name="remains" value="Scattering" class="validate[required] radio"
                        data-status="not-checked">
                        <label for="scatteringChoice">Cremation Scattering</label>  <br style="clear:both;" />

                         <input id="otherChoice" type="radio" name="remains" value="Other" class="validate[required] radio" data-status="not-checked">
                         <label for="otherChoice">Other (Please Specify):<br />

                         <?php $required = ( getArrayValue($vital , 'remains') == "Other" ) ? 'validate[required]' : null;?>
                         <input type="text" name="remainsInput" class="text <?php echo $required;?>">
                         </label>
                   </span>
                </div>
            </div>
            <!-- Start detail content form of Would you link...-->
            <div class="row">
                <div class="eaFormBox">
                    Would you like to complete obituary/funeral service information (optional)?:
                    <div class="eaFormField">
                        <span class="planningRadio">
                            <input id="obitYes" class="complete-obit-first radio" type="radio" name="obitChoice" data-status="not-checked" value="Yes">
                            <label for="obitYes">Yes</label><br style="clear:both;" />

                            <input id="obitNo" class="complete-obit-second radio" type="radio" data-status="not-checked" name="obitChoice" value="No">
                            <label for="obitNo">No</label>
                       </span>
                   </div>
                </div>
                <div class="complete-obituary <?php echo $obitStatus;?>">
                    <div class="eaFormBox">
                       Employment History and Achievements (Optional):
                        <div class="eaFormField">
                            <textarea name="empHistory" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Children (List names and addresses):
                        <div class="eaFormField">
                            <textarea name="childrenInfor" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Grandparents (List names and addresses):
                        <div class="eaFormField">
                            <textarea name="grandParent" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Brothers (List names and addresses):
                        <div class="eaFormField">
                            <textarea name="brothersInfor" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Sisters (List names and addresses):
                        <div class="eaFormField">
                            <textarea name="sisterSsInfor" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Grandchildren/Great-Grandchildren and any others (List names and addresses):
                        <div class="eaFormField">
                            <textarea name="grandChild" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Funeral Notices to appear in (List desired publications):
                        <div class="eaFormField">
                            <textarea name="funeralNotice" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       <strong>Funeral Service Location Preference:</strong>
                        <div class="eaFormField">
                            <span class="planningRadio">
                                <input id="funeralChapel" type="radio" name="funeralService" class="validate[required] radio" value="Funeral Chapel">
                                <label for="funeralChapel">Funeral Chapel</label><br style="clear:both;" />

                                <input id="funeralChurch" type="radio" name="funeralService" class="validate[required] radio" value="Church">
                                <label for="funeralChurch">Church</label><br style="clear:both;" />

                                <input id="funeralGrave" type="radio" name="funeralService" class="validate[required] radio" value="Graveside Only">
                                <label for="funeralGrave">Graveside Only</label><br style="clear:both;" />

                                <input id="funeralLocation" type="radio" name="funeralService" class="validate[required] radio" value="Other">
                                <label for="funeralLocation">Other</label>
                           </span>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Organizations to participate in Service:
                        <div class="eaFormField">
                            <textarea name="organization"  style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Suggested Readings, Poetry, etc.:
                        <div class="eaFormField">
                            <textarea name="suggest" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Other items needed:
                        <div class="eaFormField">
                            <textarea name="otherNeed" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <?php

                    ?>
                    <div class="eaFormBox obitRecentPhoto">
                       Recent Photograph:
                        <div class="eaFormField text-center">
                            <?php
                            $statusBtn = null;
                            $photoStatus = 'hide';
                            $imageUrl = null;
                            $imageName = null;
                            if(isset($documents['photo']['url']) and !empty($documents['photo']['url'])){
                                $statusBtn = "hide";
                                $photoStatus = null;
                                $imageUrl = $documents['photo']['url'];
                                $imageName = $documents['photo']['name'];
                            }
                            ?>
                            <div id="bootstrapped-fine-uploader" class="col-xs-12 fineUpload__photo <?php echo $statusBtn;?>"></div>
                            <div class="photoResult <?php echo $photoStatus;?>">
                                <a href="<?php echo $imageUrl;?>" class="thumbnail imageLink"  >
                                    <img src="<?php echo $imageUrl;?>" class="photoResult__photo"  alt="<?php echo $imageName;?>">
                                </a>
                                <a href="" data-key="photo" class="btn btn-danger removeFile"><i class="fa fa-trash"></i> Remove</a>
                            </div>
                        </div>
                    </div>

                    <div class="eaFormBox">
                       Clothing, Undergarments, Jewelry Etc:
                        <div class="eaFormField">
                            <textarea name="clothing" style="height:150px;"></textarea>
                        </div>
                    </div>

                    <div class="eaFormBox">
                       Cemetery Location:
                        <div class="eaFormField">
                            <input type="text" name="cemeteryLocation" class="text">
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Section/Lot/Space:
                        <div class="eaFormField">
                            <input type="text" name="section" class="text">
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Marker Preference:
                        <div class="eaFormField">
                            <input type="text" name="markerLocation" class="text">
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Flowers:
                        <div class="eaFormField">
                            <input type="text" name="flowers" class="text">
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Contributions to:
                        <div class="eaFormField">
                            <input type="text" name="contributions" class="text">
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Personal Achievements:
                        <div class="eaFormField">
                            <textarea name="personalAchive" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Religious Affiliation:
                        <div class="eaFormField">
                            <input type="text" name="religious" class="text">
                        </div>
                    </div>

                    <div class="eaFormBox">
                       Clubs, Organizations, Societies:
                        <div class="eaFormField">
                            <textarea name="clubs" style="height:150px;"></textarea>
                        </div>
                    </div>
                    <div class="eaFormBox">
                       Special Requests:
                        <div class="eaFormField">
                            <textarea name="special" style="height:150px;"></textarea>
                        </div>
                    </div>
               </div><!--====  End of complete-obituary  ====-->
            </div>
            <!-- End detail content form of Would you link...-->
            <div style="padding-top:10px;" class="formGroupTitle">
                SIGNATURE OF INFORMANT
            </div>
            <div class="text-plan-merch">
                <strong>Electronic Signature Agreement</strong><br>
                By typing in your name into the 'Signature' field below, you are signing this Agreement electronically. You agree your electronic signature is the legal equivalent of your manual signature on this Agreement. By typing in your name into the 'Signature' field below you consent to be legally bound by this Agreement's terms and conditions.
            </div>
            <div class="eaFormBox">
                <span style="padding-bottom:15px; display:block;">I/We warrant that all representations and statements made herein are true and correct.</span>
                Signature:
                <div class="eaFormField">
                    <span style="display:inline-block; padding-right:5px;">X</span>
                    <input type="text" class="input-signature validate[required] signature-font" data-value="sippaprut" id="signature" name="signature">
                </div>
            </div>
            <div class="eaFormBox">
                Enter Name Again:
                <div class="eaFormField">
                    <input type="text" class="text validate[required,equals[signature]]" id="enterSignature" data-value="sippaprut" name="enterSignature">
                </div>
            </div>
            <div class="eaFormBox">
                *Date:
                <div class="eaFormField">
                    <input type="text" readonly class="text" id="dateSign" name="dateSign" value="<?php echo date("M d , Y") ;?>">
                </div>
            </div>
            <div class="eaFormBox">
                Time:
                <div class="eaFormField">
                    <input type="text" readonly class="text" id="timeSign" name="timeSign" value="<?php echo date('g:i A') ;?>">
                </div>
            </div>
            

            <p></p>

             <div class="ea-btt-prev-next-area">
                <div class="row">
                    <div class="col-sm-5 col-md-5">
                        <a class="ea-btt-prev-next" href="<?php echo $prev_form;?>">Previous Step</a>
                    </div>
                    <div class="col-xs-offset-0 col-sm-5 col-sm-offset-2 col-md-5 col-md-offset-2">
                        <a class="ea-btt-prev-next btnSubmit" href="/ea/step6">Next Step</a>
                    </div>
                </div>
            </div>

        </div><!--====  End of biographical-information-form-  ====-->
    </div>
    </form>
</div>

<?php echo $sidebar;?>


<link rel="stylesheet" href="<?php echo assets_path('js/fineupload/fineupload.css');?>">
<script type="text/javascript" src="<?php echo assets_path('js/fineupload/fineupload.js');?>"></script>
<script type="text/javascript" src="<?php echo assets_path('js/fineupload/customFineUpload.js');?>"></script>

<script src="<?php echo assets_path('js/template/vital/vital.min.js?_='.strtotime('now'));?>"></script>

