<div class="col-sm-7 col-md-7 paperwork-step">
    <h1>Paperwork - Step 1</h1>
    <form action="" id="mainform" name="mainform" method="POST">
        <input type="hidden" name="pkgtype"             id="pkgtype"        value="<?php echo $user_session['pkgtype'];?>" />
        <input type="hidden" name="scriptaction"        id="scriptaction"   value="validate" />
        <input type="hidden" name="notvalidate"         id="notvalidate"    value="" />
        <input type="hidden" name="next_form"           id="next_form"      value="<?php echo $next_form?>" />
        <input type="hidden" name="session_index"       id="session_index"  value="<?php echo $session_index?>" />
        <input type="hidden" name="selected"            id="selected"       value="<?php echo $$session_index;?>" />

		 <div class="ea-content">
	        <div id="biographical-information-form-area" class="biographicalForms">
	            <div style="padding-top:10px;" class="formGroupTitle">
	                Biographical Information
	            </div>
	            <div class="text-plan-merch">All fields below are required to file a death certificate. If you do not know the information and cannot obtain it, please put ‘NA’ in the required field to move on.</div>
	            <div class="eaFormBox">
	                <?php
	                $titleFullName = "Full Name of Deceased";
	                if($user_session['pkgtype'] == "preneed"){
	                    $titleFullName = "Whom are the arrangements for (enter name)?";
	                }
	                ?>
	                <?php echo $titleFullName; ?>

	                <div class="eaFormField">
	                    <input type="text" name="deceased_first" class="text validate[required]" value="">
	                    <span class="text-bottom-input">(First)</span><br />

	                    <input type="text" name="deceased_middle" class="text" value="">
	                    <span class="text-bottom-input">(Middle)</span><br />

	                    <input type="text" name="deceased_last" class="text validate[required]" value="">
	                    <span class="text-bottom-input">(Last)</span>
	                </div>
	            </div>
	            <div class="eaFormBox">
	                Street Address:
	                <div class="eaFormField">
	                    <input type="text" name="deceased_address" class="text validate[required]">
	                </div>
	            </div>
	            <div class="eaFormBox">
	                City/Town:
	                <div class="eaFormField">
	                    <input type="text" name="deceased_city" class="text validate[required]">
	                </div>
	            </div>
	            <div class="eaFormBox">
	                Twp:
	                <div class="eaFormField">
	                    <input type="text" name="deceased_county" class="text validate[required]">
	                </div>
	            </div>
	            <div class="eaFormBox">
	                State:
	                <div class="eaFormField">
	                    <span class="payment-options">
	                        <select name="address_state" class="input-dropdown validate[required]">
	                            <option value="" >Select State</option>
	                            <?php foreach($states as $state => $arr){ ?>
	                                <option  value="<?php echo $state;?>"><?php echo $state;?></option>
	                            <?php } ?>
	                        </select>
	                    </span>
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Zip Code:
	                <div class="eaFormField">
	                    <input type="text" name="deceased_zip" class="text validate[required]">
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Telephone:
	                <div class="eaFormField">
	                    <input type="text" name="deceased_phone" class="text phoneFormat validate[required]">
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Social Security Number:
	                <div class="eaFormField">
	                    <input type="text" name="ssn" class="text ">
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Date of Birth:
	                <div class="eaFormField">
	                        <span class="payment-options col-xs-5">
	                        <select style="width: 98%;" id="mob" name="mob" class="validate[required]">
	                            <option  value="">Month</option>
	                            <?php foreach($cfg['month'] as $monthNumber => $month){?>
	                                <option value="<?php echo $month;?>"><?php echo $month;?></option>
	                            <?php } ?>
	                        </select>
	                        </span> <span class="payment-options col-xs-3">
	                        <select style="width: 97%;" id="dob" name="dob" class="validate[required]">
	                            <option value="">Day</option>
	                            <?php for($day= 1; $day<= 31; $day++){
	                                $day = substr($day, -2);
	                            ?>
	                            <option  value="<?php echo $day;?>"><?php echo $day;?></option>
	                            <?php } ?>
	                        </select>
	                        </span> <span class="payment-options col-xs-4">
	                        <select style="width: 98%;" id="yob" name="yob" class="validate[required]">
	                            <option  value="">Year</option>
	                            <?php
	                                $currentYear = date("Y");
	                                for($year = $currentYear; $year >= 1900; $year-- ){
	                            ?>
	                                    <option  value="<?php echo $year;?>"><?php echo $year;?></option>
	                            <?php } ?>
	                        </select>
	                        </span>
	                    </div>
	            </div>

	            <?php if($user_session['pkgtype'] == "atneed"){ ?>
	            <div class="eaFormBox">
	                Date of Death
	                <div class="eaFormField">
	                    <span class="payment-options col-xs-5" >
	                        <select  id="month_of_death" name="mod" data-prompt-position="topLeft" readonly class="validate[required] col-md-12">
	                            <option value="">Month</option>
	                            <?php foreach($cfg['month'] as $monthNumber => $month) { ?>
	                                    <option value="<?php echo $month;?>"><?php echo $month;?></option>
	                            <?php } ?>
	                        </select>
	                    </span>
	                    <span class="payment-options col-xs-3" >
	                        <select  id="day_of_death" name="dod" data-prompt-position="topLeft" readonly class="validate[required] col-md-12">
	                            <option selected="" value="">Day</option>
	                            <?php for($day= 1; $day<= 31; $day++){
	                                    $day = substr($day, -2);
	                            ?>
	                                <option  value="<?php echo $day;?>"><?php echo $day;?></option>
	                            <?php } ?>
	                        </select>
	                    </span>
	                    <span class="payment-options col-xs-4" >
	                        <select  id="year_of_death" name="yod" data-prompt-position="topLeft" class="validate[required] col-md-12">
	                            <?php for($year = date("Y"); $year >= 1900; $year--){
	                                $selected = ($year == date("Y")) ? 'selected' : NULL;
	                            ?>
	                            <option <?php echo $selected;?> value="<?php echo $year;?>"><?php echo $year;?></option>
	                            <?php }?>
	                        </select>
	                    </span>
	                </div>
	            </div>
	            <?php } ?>

	            <div class="eaFormBox">
	                Sex:
	                <div class="eaFormField">
	                    <span class="planningRadio">
	                        <span class="optionRadioInline">
	                            <input type="radio" id="femaleRadio" name="sex" value="Female" class="validate[required] radio">
	                            <label for="femaleRadio">Female</label>
	                        </span>
	                        <span class="optionRadioInline">
	                            <input  type="radio" id="maleRadio" name="sex" value="Male" class="validate[required] radio">
	                            <label for="maleRadio">Male</label>
	                        </span>
	                   </span>
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Birth Place (City/Town, State or Foreign Country):
	                <div class="eaFormField">
	                    <input type="text" name="b_place" class="text validate[required]">
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Marital Status
	                <div class="eaFormField">
	                    <span class="planningOptions">
	                        <select name="marital" id="marital" class="validate[required]" >
	                        	<option value="">Select a Marital Status</option>
	                            <option value="Never Married">Never Married</option>
	                            <option value="Married">Married</option>
	                            <option value="Divorced">Divorced</option>
	                            <option value="Widowed">Widowed</option>                        
	                        </select>
	                  </span>
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Date of Marriage (If Married at Time of Death):
	                <div class="eaFormField">
	                        <span class="payment-options col-xs-5">
	                        <select style="width: 98%;" id="mom" name="mom" >
	                            <option selected="" value="">Month</option>
	                            <?php foreach($cfg['month'] as $monthNumber => $month){?>
	                                    <option  value="<?php echo $month;?>"><?php echo $month;?></option>
	                            <?php } ?>
	                        </select>
	                        </span> <span class="payment-options col-xs-3">
	                        <select style="width: 97%;" id="dom" name="dom" >
	                            <option  value="">Day</option>
	                            <?php
	                                for($day= 1; $day<= 31; $day++){
	                                    $day = substr($day, -2);
	                                ?>
	                                    <option value="<?php echo $day;?>"><?php echo $day;?></option>
	                                <?php
	                                }
	                            ?>
	                        </select>
	                        </span> <span class="payment-options col-xs-4">
	                        <select style="width: 98%;" id="yom" name="yom" >
	                            <option selected="" value="">Year</option>
	                            <?php
	                            $currentYear = date("Y");
	                            for($year = $currentYear; $year >= 1900; $year-- ){
	                            ?>
	                                <option value="<?php echo $year;?>"><?php echo $year;?></option>
	                            <?php
	                            }
	                            ?>
	                        </select>
	                        </span>
	                    </div>
	            </div>

	            <div class="eaFormBox">
	                Surviving Spouse’s Name (Maiden Name):
	                <div class="eaFormField">
	                    <input type="text" name="spouse_name" class="text <?php echo $validateClass;?>">
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Father’s Full Name:
	                <div class="eaFormField">
	                    <input type="text" name="father_name" class="text validate[required]">
	                </div>
	            </div>
	            <div class="eaFormBox">
	                Mother’s Full Maiden Name:
	                <div class="eaFormField">
	                    <input type="text" name="mother_name" class="text validate[required]">
	                </div>
	            </div>

				<?php if ( $user_session['pkgtype'] == "atneed") { ?>
	            <div class="eaFormBox">
	                Place of Death:
	                <div class="eaFormField">
	                    <span class="planningRadio">
	                    	<?php foreach ( $cfg['place_death_choice'] as $place) { ?>
	                        <span class="optionRadioInline">
	                            <input type="radio" id="hospitalInpatientRadio" name="pod" value="<?php echo $place;?>" class="validate[required] radio">
	                            <label for="hospitalInpatientRadio"><?php echo $place;?></label>
	                        </span><br style="clear:both;" />
	                    	<?php } ?>
	                   </span>
	                </div>
	                <div class="eaFormField">
	                    <input type="text" name="other_pob" class="text">
	                </div>
	            </div>
	        	<?php } ?>

	            <div class="eaFormBox">
	                Was decedent a veteran? :
	                <div class="eaFormField">
	                    <span class="planningRadio">
	                        <span class="optionRadioInline">
	                            <input type="radio" id="decedentVeteranYes" name="decedentVeteran" value="Yes" class="validate[required] radio">
	                            <label for="decedentVeteranYes">Yes  </label>
	                        </span>
	                        <span class="optionRadioInline">
	                            <input  type="radio" id="decedentVeteranNo" name="decedentVeteran" value="No" class="validate[required] radio">
	                            <label for="decedentVeteranNo">No</label>
	                        </span>
	                   </span>
	                </div>
	            </div>

	            <div class="eaFormBox">
	               If yes, What branch:
	                <div class="eaFormField">
	                    <input type="text" name="decedent_veteran_branch" class="text">
	                </div>
	            </div>

	            <div class="eaFormBox">
	                What rank:
	                <div class="eaFormField">
	                    <input type="text" name="what_rank" class="text">
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Education:
	                <div class="eaFormField">
	                    <span class="payment-options">
	                    <select id="education" name="education" class="validate[required]">
	                    	<option value="">Select a Education Level</option>
	                    	<?php foreach ($cfg['education'] as $id => $row) { ?>
	                        	<option value="<?php echo $id;?>"><?php echo $row['title'];?></option>
	                    	<?php } ?>
	                    </select>
	                    </span>
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Decedent of Hispanic Origin?:
	                <div class="eaFormField">
	                    <span class="planningRadio">
	                    	<?php foreach ($cfg['decedent_hispanic_choice'] as $id => $row) { ?>
		                        <input id="hispanic_origin_<?php echo $id;?>" class="validate[required] radio" type="radio" name="hispanic_origin" value="<?php echo $id;?>" data-status="not-checked">
		                        <label for="hispanic_origin_<?php echo $id;?>"><?php echo $row['name'];?></label>
		                        <br style="clear:both;" />
	                    	<?php } ?>
	                        <input type="text" name="hispanic_origin_other" id="hispanic_origin_other" class="text">
	                   </span>
	               </div>
	            </div>

	            <div class="eaFormBox">
	                Decedent’s Race :
	                <div class="eaFormField">
	                    <span class="planningRadio">
	                    	<?php foreach ( $cfg['race_choice'] as $key => $row ) { ?>
	                        <span class="optionCheck">
	                        	<input id="race_choice_<?php echo $key;?>" class="validate[required] radio" type="radio" name="race" value="<?php echo $key;?>" data-status="not-checked">
	                        	<label for="race_choice_<?php echo $key;?>"><?php echo $row['name'];?></label>
	                    	</span>
	                    	<br style="clear:both;" />
	                    	<?php } ?>
	                        <input type="text" name="race_other" class="text">
	                   </span>
	               </div>
	            </div>

	            <div class="eaFormBox">
	               Decedent’s Single Race Self-Designation: Same as Above / Other (list) :
	                <div class="eaFormField">
	                    <input type="text" name="single_race" class="text">
	                </div>
	            </div>

	            <div class="eaFormBox">
	               Occupation:
	                <div class="eaFormField">
	                    <input type="text" name="occupation" class="text">
	                </div>
	            </div>

	            <div class="eaFormBox">
	               Type of Business:
	                <div class="eaFormField">
	                    <input type="text" name="type_business" class="text">
	                </div>
	            </div>

	            <div class="eaFormBox">
	               Informant’s Name:
	                <div class="eaFormField">
	                    <input type="text" name="informant_name" class="text">
	                </div>
	            </div>

	            <div class="eaFormBox">
	                Relationship to Decedent:
	                <div class="eaFormField">
	                    <input type="text" name="informant_relationship" class="text">
	                </div>
	            </div>


	            <div class="eaFormBox">
	               Street Address:
	                <div class="eaFormField">
	                    <input type="text" name="informant_address" class="text validate[required]" value="">
	                </div>
	            </div>
	            <div class="eaFormBox">
	                City:
	                <div class="eaFormField">
	                    <input type="text" name="informant_city" class="text validate[required]">
	                </div>
	            </div>
	            <div class="eaFormBox">
	                State:
	                <div class="eaFormField">
	                    <span class="payment-options">
	                        <select name="informant_state" class="input-dropdown validate[required]">
	                            <option value="" >Select State</option>
	                            <?php foreach($cfg['states'] as $state => $row){ ?>
	                            <option value="<?php echo $state;?>"><?php echo $state;?></option>
	                            <?php } ?>
	                        </select>
	                    </span>
	                </div>
	            </div>
	            <div class="eaFormBox">
	                Zip Code:
	                <div class="eaFormField">
	                    <input type="text" name="informant_zip" class="text validate[required]">
	                </div>
	            </div>
	            <div class="eaFormBox">
	               Phone:
	                <div class="eaFormField">
	                    <input type="text" name="informant_phone" class="text validate[required] phoneFormat">
	                </div>
	            </div>
	            <div class="eaFormBox">
	               Email:
	                <div class="eaFormField">
	                    <input type="text" name="informant_email" data-value="sip@frazerconsultants.com" class="text validate[required ,custom[email]]"
	                    value=""  />
	                </div>
	            </div>

	            <div class="eaFormBox">
	               Mobile:
	                <div class="eaFormField">
	                    <input type="text" name="informant_mobile" class="validate[required]" class="text">
	                </div>
	            </div>

				<div style="padding-top:10px;" class="formGroupTitle">
	                SIGNATURE OF INFORMANT
	            </div>
	            <div class="text-plan-merch">
	                <strong>Electronic Signature Agreement</strong><br>
	                By typing in your name into the 'Signature' field below, you are signing this Agreement electronically. You agree your electronic signature is the legal equivalent of your manual signature on this Agreement. By typing in your name into the 'Signature' field below you consent to be legally bound by this Agreement's terms and conditions.
	            </div>
	            <div class="eaFormBox">
	                <span style="padding-bottom:15px; display:block;">I/We warrant that all representations and statements made herein are true and correct.</span>
	                Signature:
	                <div class="eaFormField">
	                    <span style="display:inline-block; padding-right:5px;">X</span>
	                    <input type="text" class="input-signature validate[required] signature-font" data-value="sippaprut" id="signature" name="signature">
	                </div>
	            </div>
	            <div class="eaFormBox">
	                Enter Name Again:
	                <div class="eaFormField">
	                    <input type="text" class="text validate[required,equals[signature]]" id="enterSignature" data-value="sippaprut" name="enterSignature">
	                </div>
	            </div>
	            <div class="eaFormBox">
	                *Date:
	                <div class="eaFormField">
	                    <input type="text" readonly class="text" id="dateSign" name="dateSign" value="<?php echo date("M d , Y") ;?>">
	                </div>
	            </div>
	            <div class="eaFormBox">
	                Time:
	                <div class="eaFormField">
	                    <input type="text" readonly class="text" id="timeSign" name="timeSign" value="<?php echo date('g:i A') ;?>">
	                </div>
	            </div>

	            <p></p>

	            <div class="ea-btt-prev-next-area">
	                <div class="row">
	                    <div class="col-sm-5 col-md-5">
	                        <a class="ea-btt-prev-next" href="<?php echo $prev_form;?>">Previous Step</a>
	                    </div>
	                    <div class="col-xs-offset-0 col-sm-5 col-sm-offset-2 col-md-5 col-md-offset-2">
	                        <a class="ea-btt-prev-next btnSubmit" href="/ea/step6">Next Step</a>
	                    </div>
	                </div>
	            </div>
	        </div><!--====  End of biographical-information-form-  ====-->
	    </div>



   </form>
</div><!--====  End of paperwork-step  ====-->

<?php echo $sidebar;?>


<script src="<?php echo assets_path('js/template/vital/vital.min.js?_='.strtotime('now'));?>"></script>

