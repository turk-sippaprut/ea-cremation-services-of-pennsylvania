<div class="col-sm-7 col-md-7">
    <div class="thanks-message">
        <h1>Thank You</h1>
        <div class="ea-content ">
            <p><strong>Your arrangement ID is <?php echo $user_session['uniqid'];?>.</strong></p>
            <?php
            if(isset($user_session[$paymentprefix]['paybycash']) and
                ( $user_session[$paymentprefix]['paybycash'] == "check/cash" or $user_session[$paymentprefix]['paybycash'] == "N/A") ){
                $titleAmount = 'The amount of $'.number_format($user_session['summary']['total'] , 2).' is owed.';
            } else {
                $titleAmount = 'The amount of $'.number_format($user_session['summary']['total'] , 2).' has been charged to your credit card';
            }
            ?>
            <p><?php echo $titleAmount;?></p>
            <p>You will be e-mailed a confirmation of your information and payment within 24 hours. Our office will call you to confirm your order and answer any questions that you may have.</p>
            <p style="color:#3f819f;">If the death has already occurred and you need our staff to respond to the location of death, please contact us NOW at <?php echo $settings['client_phone_local'];?>.</p>
            <p>Thank you for allowing <?php echo $settings['client_company_name'];?> to serve your family. If you have any questions please contact us at <?php echo $settings['client_phone_local'];?>.</p>
            <div class="ea-box-content ea-box-content-no-bg">
                <div class="ea-box-inner-no-bg">
                    <h4 class="h4__headline">Important:</h4>
                    <ul>
                        <li>You may print your
                            <a href="<?php echo $user_session['sogs_document'];?>" target="_blank">
                            Statement of Goods and Services</a>.
                        </li>
                        <li class="hidden-print">
                            You may <a href=".thanks-message" class="printThisPage">print this page</a> with your order ID for records
                        </li>
                        <li>If you have any other questions you can email <a href="mailto:<?php echo $settings['client_email'];?>">
                            <?php echo $settings['client_email'];?></a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="eaFormBox hidden-print">
                Email plan details to family member(s):
                <div class="eaFormField">
                    <form action="" id="form-family" name="form-family" method="POST">
                        <input type="text" name="family_email" id="family_email" class="text"
                        style="width:80%;" placeholder="separate email address with ;">
                        <input type="submit" value="Submit" class="btn-submit-email">
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="ea-btt-prev-next-area">
        <div class="row">
            <div class="ea-btn-home">
                <a class="ea-btt-prev-next" href="/">Home</a>
            </div>
        </div>
    </div>

</div>
<?php echo $sidebar;?>
<script src="<?php echo assets_path('js/template/thankyou/thankyou.min.js?_='.strtotime('now'));?>"></script>
