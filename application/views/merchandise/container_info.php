<div id="choose-container-more-info" class="white-popup mfp-hide popup-more-info">
    <p>Container Options for Cremation include: Hardwood Caskets, for those desiring something more traditional; Cremation Caskets for those wanting a simple yet dignified casket constructed specifically for cremation; and Basic Cremation Containers.
    </p>
 </div>