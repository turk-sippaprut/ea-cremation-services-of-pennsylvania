<div class="col-sm-7 col-md-7">
    <h1>Arrangement Online - Step 2</h1>
    <form action="" id="mainform" name="mainform" method="POST">
        <input type="hidden" name="pkgtype"             id="pkgtype"        value="<?php echo $user_session['pkgtype'];?>" />
        <input type="hidden" name="scriptaction"        id="scriptaction"   value="validate" />
        <input type="hidden" name="notvalidate"         id="notvalidate"    value="" />
        <input type="hidden" name="next_form"           id="next_form"      value="<?php echo $next_form?>" />
        <input type="hidden" name="session_index"       id="session_index"  value="<?php echo $session_index?>" />
        <input type="hidden" name="selected"            id="selected"       value="<?php echo $$session_index;?>" />

    <div class="ea-content">
        <div class="text-plan-merch">
            All plans include a basic plastic temporary urn and a minimum cardboard cremation container, however you may choose to upgrade for an additional cost.
        </div>

            <?php foreach($merchant as $key => $category){ ?>
        <div class="ea-box-content">
            <div class="title-merch-box-area shadow-title-plan relative">
                <span class=" merch-more__icon">
                    <i class="iconCaret fa <?php echo ($category['active']) ? 'fa-caret-down' : 'fa-caret-right';?>  fa-3x"></i>
                </span>
                <a class="show-package tabsSelect" data-key="<?php echo $key;?>"><?php echo $category['title'];?></a>
                <a class="more-info-merch-link merch-more__link popUpElement" href="#choose-<?php echo $key;?>-more-info">More Info</a>
                <?php echo $category['pageinfo'];?>
            </div>

            <div class="ea-box__data <?php echo ($category['active']) ? null : 'hide';?>">
                <div class="ea-box-content-inner-merch">
                     <div class="ea-merch-product">
                        <?php $sliderName = ($category['slider']) ? "ea-merch-slide" : null;?>
                        <div class="<?php echo $sliderName;?>" data-key="<?php echo $key;?>">
                        <?php $start = 0;
                        $page = 0;
                        $perpage = 6;
                        $size = sizeof($category['data']);
                       // $pageSize = ceil($size / $perpage);
                        foreach($category['data'] as $id => $row){
                           echo ($page == 0 ) ? '<div> <div class="row border-bottom-merch">' : null;

                           $showPrice = (in_array($id , $cfg['includedProduct'] )) ? "No Charge" : "$".number_format($row['price'] , 2);
                           $includedClass = (in_array($id , $cfg['includedProduct'] )) ? $key."_included included" : null;
                           $buttonName = isset($user_session[$session_index][$key][$id]) ? "REMOVE ITEM" : "SELECT ITEM";
                           $buttonAction = isset($user_session[$session_index][$key][$id]) ? "button-selected cartRemove" : "cartAdd";
                           if( isset($user_session[$session_index][$key][$id]) and in_array($id , $cfg['includedProduct'] )){
                                $buttonName = "ITEM SELECTED";
                           }

                        ?>
                                    <div class="col-sm-6 col-md-4 text-center productSection">
                                        <div class="images-merch">
                                            <img src="<?php echo pathImage($row['type']."/".$id."_thumbnail.jpg");?>"
                                            alt="<?php echo $row['name'];?>"
                                            width="133" height="133">
                                        </div>
                                        <div class="name-merch"><?php echo $row['name'];?></div>
                                        <div class="view-detail-merch-link">
                                            <a class="view-detail-merch popUpElement merch-more__link" href="#<?php echo $key."-".$id;?>">
                                            View Details</a>
                                        </div>
                                        <div id="<?php echo $key."-".$id;?>" class="white-popup mfp-hide">
                                            <div class="merchandise-light-box row">
                                                <div class="thumb-product col-md-4 thumbnail">
                                                    <img src="<?php echo pathImage($row['type']."/".$id."_image.jpg");?>">
                                                </div>
                                                <div class="title-product col-md-6">
                                                    <h2><?php echo $row['name'];?></h2>
                                                    <p>Price:
                                                        <strong>
                                                            <?php echo $showPrice;?>
                                                        </strong>
                                                    </p>
                                                </div>
                                                <div class="description-merch col-md-12">
                                                    <p class="title">Description</p>
                                                    <p class="text"><?php echo $row['desc1'];?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($category['hasQuan']){ ?>
                                        <div class="select-style">
                                            <select  class="js-example-basic-single select-style-merch cartQuan">
                                                <?php for($quan = 1; $quan <= 5; $quan++){ ?>
                                                <?php $selected = (isset($user_session[$session_index][$key][$id]['quan']) and
                                                                  $user_session[$session_index][$key][$id]['quan'] == $quan) ?
                                                                    'selected' : null;
                                                ?>
                                                <option value="<?php echo $quan;?>" <?php echo $selected;?>>
                                                    <?php echo $quan;?> - $<?php echo number_format($row['price'] * $quan  , 2);?>
                                                </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <?php } else{ ?>
                                        <div class="price-merch">
                                            <?php echo $showPrice;?>
                                        </div>
                                        <?php } ?>
                                        <div class="btt-select-merch-area">
                                            <div class="select-item  <?php echo $includedClass;?> <?php echo $buttonAction;?>"
                                            data-id="<?php echo $id;?>"
                                            data-name="<?php echo jsSpecialchars($row['name']);?>"
                                            data-group="<?php echo $key;?>"
                                            data-hasquan="<?php echo $category['hasQuan'];?>">
                                                <?php echo $buttonName;?>
                                            </div>
                                        </div><!--====  End of btt-select-merch-area  ====-->
                                    </div><!--====  End of productSection  ====-->

                            <?php $start++;
                            $page++;
                            if($page >= $perpage){
                                $page = 0;
                                echo ' </div> </div><!--end page '.$page.'-->';
                            } else if($start == $size){
                                echo ' </div> </div><!--end page '.$start.'-->';
                            }
                            ?>
                        <?php
                        } ?>
                        </div><!--====  End of ea-merch-slide  ====-->
                     </div><!--====  End of ea-merch-product  ====-->
                </div><!--====  End of inner-merch  ====-->
            </div>
            </div><!--====  End of ea-box content  ====-->
            <div class="ea-line-box-content-merch"></div>
            <br />
            <?php } ?>

        <br /><br />
        <h3>Additional Merchandise</h3>
        <?php foreach($additionalMerch as $key => $row){ ?>
        <div class="row ea-options-list-area">
            <div class="col-sm-6 col-md-8 option-list-text"><?php echo $row['name'];?>:
                <?php echo ($row['show_price'] == 1) ? number_format($row['price'] , 2) : null;?>
                <?php if($row['show_detail'] == 0){ ?>
                <a class="more-info showMore" href=".memorial-<?php echo $row['id'];?>">More Info</a>
                <?php } ?>

                <div id="memorial-package-detail" class="memorial-<?php echo $row['id'];?> col-md-12">
                    <div class="text-mor-info-detail">
                       <?php echo decode_data($row['description'] , true);?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 ea-select-style">
                <div class="select-options">
                    <label>
                        <?php
                            $checked = isset($user_session[ $session_index ][ $row['type'] ][ $row['id'] ]) ? 'checked' : null;
                        ?>
                        <input type="checkbox" <?php echo $checked;?> class="option-yes-no selectOption"
                        name="<?php echo $row['type'];?>_select"
                        data-id="<?php echo $row['id'];?>"
                        data-group="<?php echo $row['type'];?>"
                        data-name="<?php echo jsSpecialchars($row['name']);?>"
                        data-title="<?php echo jsSpecialchars($row['name']);?>"
                        data-quan="1"
                        value="<?php echo $row['id'];?>" />
                    </label>
                </div>
                <?php if($row['id'] == 13){ ?>
                <select name="option-width" id="option-width" class="select2__width">
                    <?php
                    $selectedKey = isset($user_session[$session_index][$row['type']][ $row['id'] ]['keywidth'])
                        ? $user_session[$session_index][$row['type']][ $row['id'] ]['keywidth']
                        : 1;
                    foreach($cfg['memorialPortrait'] as $key => $arr){ ?>
                    <?php $selected = ($selectedKey == $key) ? 'selected' : null;?>
                    <option <?php echo $selected;?> data-title="<?php echo $arr['title'];?>" value="<?php echo $key;?>"><?php echo $arr['name'];?></option>
                    <?php } ?>
                </select>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
        <br /><br />
    </div>


    <div class="ea-btt-prev-next-area">
        <div class="row">
            <div class="col-sm-5 col-md-5">
                <a class="ea-btt-prev-next" href="<?php echo $prev_form;?>">Previous Step</a>
            </div>
            <div class="col-xs-offset-0 col-sm-5 col-sm-offset-2 col-md-5 col-md-offset-2">
                <a class="ea-btt-prev-next btnSubmit" href="">Next Step</a>
            </div>
        </div>
    </div>

    </form>
</div>
<?php echo $sidebar;?>
<script src="<?php echo assets_path('js/template/merchandise/merchandise.min.js');?>"></script>
