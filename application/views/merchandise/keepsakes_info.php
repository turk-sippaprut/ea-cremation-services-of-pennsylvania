<div id="choose-keepsakes-more-info" class="white-popup mfp-hide popup-more-info">
    <p>Family members or special friends often want to memorialize their loved ones by retaining a small portion of the cremated remains. A variety of small keepsake pendants are available for this purpose.</p>
 </div>