<div class="col-sm-7 col-md-7">
    <h1>Paperwork - Step 2</h1>
    <form action="" id="mainform" name="mainform" method="POST">
        <input type="hidden" name="pkgtype"             id="pkgtype"        value="<?php echo $user_session['pkgtype'];?>" />
        <input type="hidden" name="scriptaction"        id="scriptaction"   value="validate" />
        <input type="hidden" name="notvalidate"         id="notvalidate"    value="" />
        <input type="hidden" name="next_form"           id="next_form"      value="<?php echo $next_form?>" />
        <input type="hidden" name="session_index"       id="session_index"  value="<?php echo $session_index?>" />
        <input type="hidden" name="selected"            id="selected"       value="<?php echo $$session_index;?>" />


    <div class="ea-content">
        <div class="row">
           <div class="formGroupTitle">
                AUTHORITY TO CREMATE AND ORDER FOR DISPOSITION
           </div>
           <div class="text-plan-merch">Subject to the rules and regulations of <strong>Cremation Services of Pennsylvania</strong>, herein referred to as the Company, or it’s duly authorized agent, you are hereby authorized to take possession of and directed to cremate or cause to be cremated the remains of:</div>
        </div>
        <div class="row">
            <div class="eaFormBox">
                Name:
                <div class="eaFormField">
                    <?php $person = $vital['deceased_first'] ." ".$vital['deceased_middle']." ".$vital['deceased_last'];?>
                    <input type="text" name="person" class="text" readonly value="<?php echo $person;?>">
                </div>
            </div>
            <?php if($user_session['pkgtype'] == "atneed"){ ?>
            <div class="eaFormBox">
                Date of Death:
                <div class="eaFormField">
                    <span class="payment-options col-xs-5">
                    <select style="width: 98%;" id="mod" name="mod" class="validate[required]"  >
                        <option selected="" value="">Month</option>
                        <?php foreach($cfg['month'] as $monthNumber => $month){
                            $selected = ($month == $vital['mod']) ? 'selected' : null;
                        ?>
                            <option <?php echo $selected;?>  value="<?php echo $month;?>"><?php echo $month;?></option>
                        <?php } ?>
                    </select>
                    </span> <span class="payment-options col-xs-3">
                    <select style="width: 97%;" id="dod" name="dod" class="validate[required]">
                        <option selected="" value="">Day</option>
                        <?php for($day= 1; $day<= 31; $day++){
                            $day = substr($day, -2);
                            $selected = ($day == $vital['dod']) ? 'selected' : null;
                        ?>
                            <option <?php echo $selected;?> value="<?php echo $day;?>"><?php echo $day;?></option>
                        <?php } ?>
                    </select>
                    </span> <span class="payment-options col-xs-4">
                    <select style="width: 98%;" id="yod" name="yod" class="validate[required]">
                        <option selected="" value="">Year</option>
                        <?php for($year = date("Y"); $year >= 1900; $year--){
                            $selected = ($year == $vital['yod']) ? 'selected' : NULL;
                        ?>
                        <option <?php echo $selected;?> value="<?php echo $year;?>"><?php echo $year;?></option>
                        <?php }?>
                    </select>
                    </span>
                </div>
            </div>
            <?php } ?>
            <div class="eaFormBox containerInit">
                DISCLOSURES, WARRANTIES AND PERMISSIONS (INITIAL EACH)
                <div class="eaFormField">
                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                            1. <input type="text" name="init_1" id="init_1" class="initial input-form-left validate[required]">
                        </span>
                        <span class="ea-form-right">
                            I have read and understand the disclosures regarding the cremation process, commingling and casket preparations found <a class="popUpElement" href=".authAgent">HERE</a>
                            <?php echo $viewAuthagent;?>
                        </span>
                    </div>
                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                            2. <input type="text" name="init_2" id="init_2" class="initial input-form-left validate[required]">
                        </span>
                        <span class="ea-form-right">I understand that if I wish to remove and/or retain any item from the remains, I must do so directly or by authorized agent prior to the cremation process.</span>
                    </div>
                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                            3. <input type="text" name="init_3" id="init_3" class="initial input-form-left validate[required]"></span>
                        <span class="ea-form-right">
                            I give my express permission for the following:<br />
                            a)  The incidental or inadvertent commingling of the cremated remains.<br />
                            b)  The processing of the cremated remains, resulting in the incidental commingling of the cremated remains.<br />
                            c)  The disposal, by the Company and Crematory, of metal or other non-human materials recovered to which may be affixed bone particles or other human residue.

                        </span>
                    </div>
                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                        4. <input type="text" name="init_4" class="initial input-form-left validate[required]">
                        </span>
                        <span class="ea-form-right">I have been offered the opportunity to personally identify the remains. (This was offered as “Pre-Cremation Identification” in step 1 of the e-Arrangement process).
                            <div class="ea-form-input-in-right-col">
                                <span class="planningRadio">

                                <input id="disclosures_4_1" type="radio" name="init_4_radio" value="Accepted" class="radio validate[required]" >
                                <label for="disclosures_4_1">Accepted</label><br style="clear:both;" />

                                <input id="init_4_radio_2" type="radio" name="init_4_radio" value="Declined" class="radio validate[required]" >
                                <label for="init_4_radio_2">Declined</label>

                               </span>
                            </div>
                        </span>
                    </div>
                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                            5. <input type="text" name="init_5" id="init_5" class="initial input-form-left validate[required]">
                        </span>
                        <span class="ea-form-right">I have been offered the opportunity the opportunity to witness the cremation. (This was offered as “Witnessed Cremation” in step 1 of the e-Arrangement process).
                            <div class="ea-form-input-in-right-col">
                                <span class="planningRadio">
                                <input id="init_5_radio_acc" type="radio" name="init_5_radio" value="Accepted" class="radio validate[required]"  >
                                <label for="init_5_radio_acc">Accepted</label><br style="clear:both;" />

                                <input id="init_5_radio_dec" type="radio" name="init_5_radio" value="Declined" class="radio validate[required]" >
                                <label for="init_5_radio_dec">Declined</label>
                                </span>
                            </div>
                        </span>
                    </div>
                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                            6. <input type="text" name="init_6" id="init_6" class="initial input-form-left validate[required]">
                        </span>
                        <span class="ea-form-right">Cremation Services of Pennsylvania reserves the right to schedule the cremation at their discretion unless any of the following is required to precede the cremation:<br />
                            <div class="ea-form-input-in-right-col">
                                <input type="checkbox" value="Private Autopsy" name="private" id="private" class="checkbox check-box-form  checkbox__row init_6-check">
                                <span class="label-check-box">
                                    <label class="" for="private">Private Autopsy</label>
                                </span><br />

                                <input type="checkbox" value="After Identification Viewing" name="identify" id="identify"
                                class="checkbox check-box-form  checkbox__row init_6-check">
                                <span class="label-check-box">
                                    <label class="" for="identify">After Identification Viewing</label>
                                </span><br />

                                <input type="checkbox" value="Nothing is to precede cremation" name="nothing" id="nothing"
                                class="checkbox check-box-form  checkbox__row init_6-check">
                                <span class="label-check-box">
                                    <label class="" for="nothing">Nothing is to precede cremation</label>
                                </span>

                            </div>

                        </span>
                    </div>

                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                            7. <input type="text" name="init_7" id="init_7" class="initial input-form-left validate[required]">
                        </span>
                        <span class="ea-form-right">I understand that in the event the cremated remains have not been permanently interred or picked up by me or my designated representative within 30 days from the date of cremation, the company is authorized to inter the cremated remains in a common place including the commingling of said remains with other human cremated remains which thereafter are non-recoverable.</span>
                    </div>

                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                            8. <input type="text" name="init_8" id="init_8" class="initial input-form-left validate[required]">
                        </span>
                        <span class="ea-form-right">Does the deceased have a <strong>PACEMAKER, BREAST IMPLANTS, RADIOACTIVE IMPLANTS or OTHER EXPLODABLE IMPLANTS?</strong>
                        <div class="ea-form-input-in-right-col">
                                <span class="planningRadio">
                                    <input id="init_8_yes" type="radio" name="init_8_radio" value="Yes" class="radio validate[required]" >
                                    <label for="init_8_yes">Yes</label><br style="clear:both;" />

                                    <input id="init_8_n" type="radio" name="init_8_radio" value="No" class="radio validate[required]" >
                                    <label for="init_8_n">No</label>
                               </span>
                               If YES, please list items:<br />
                               <input type="text" name="init_8_inputYes" class="init8_radioYes">
                            </div>
                        </span>
                    </div>

                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                            9. <input type="text" name="init_9" id="init_9" class="initial input-form-left validate[required]">
                        </span>
                        <span class="ea-form-right">This cremation will be performed at Charles F. Snyder Funeral Home & Crematory, 3110 Lititz Pike, Lititz, PA 17543 OR
                            <div class="ea-form-input-in-right-col">
                                <span class="planningRadio">
                                    <input type="checkbox"  value="Yes" name="init_9_checkbox" id="init_9_checkbox"
                                    class="check-box-form checkbox__row checkbox">

                                    <span class="label-check-box">
                                        <label for="init_9_checkbox">Other (please specify):</label>
                                    </span><br />
                                    <input type="text" name="init_9_other">
                                </span>
                            </div>
                        </span>
                    </div>
                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                            10. <input type="text" name="init_10" id="init_10" class="initial input-form-left validate[required]">
                        </span>
                        <span class="ea-form-right">Was the decedent legally married at the time of death?
                        <div class="ea-form-input-in-right-col">
                                <span class="planningRadio">
                                    <input type="radio" name="init_10_radio" id="init_10_radioY" value="Yes" class="radio validate[required]" >
                                    <label for="init_10_radioY">Yes</label><br style="clear:both;" />
                                    <input type="radio" name="init_10_radio" id="init_10_radioN" value="No" class="radio validate[required]"  >
                                    <label for="init_10_radioN">No</label>
                               </span>
                            </div>
                        </span>
                    </div>
                    <div class="row ea-space-bottom-form">
                        <span class="ea-form-left">
                            11. <input type="text" name="init_11" id="init_11" class="initial input-form-left validate[required]">
                        </span>
                        <span class="ea-form-right">Does the decedent have any living children?
                        <div class="ea-form-input-in-right-col">
                                <span class="planningRadio">
                                    <input id="init_11Y" type="radio" name="init_11_radio" class="radio validate[required]"  value="Yes">
                                    <label for="init_11Y">Yes</label><br style="clear:both;" />

                                    <input id="init_11N" type="radio" name="init_11_radio" class="radio validate[required]"  value="No">
                                    <label for="init_11N">No</label>
                               </span>
                               If YES, how many?<br />
                               <input type="text" name="init_11_inputYes" class="number">
                            </div>
                        </span>
                    </div>

                </div>
            </div>
        </div>

        <div class="ea-box-content ea-box-content-no-bg">
            <div class="ea-box-inner-no-bg">
                <div class="ea-form-order-desposition">
                    <div class="formGroupTitle text-center">
                        ORDER FOR DISPOSITION
                    </div>
                    <div class="row">
                        <div class="eaFormBox">
                            Cremation Container:
                            <div class="eaFormField">
                                <input type="text" name="container" class="text" value="<?php echo $container;?>" readonly>
                            </div>
                        </div>
                        <div class="eaFormBox">
                            Urn:
                            <div class="eaFormField">
                                <input type="text" name="urns" readonly class="text" value="<?php echo $urns;?>">
                            </div>
                        </div>
                     </div>
                    <div class="row">
                        <div class="eaFormBox">
                            <div class="text-center">I HEREBY DIRECT AND AUTHORIZE THE RELEASE/DELIVERY/OR SHIPMENT
            OF SAID CREMATED REMAINS. (INITIAL ONE)</div><br />
                            <div class="eaFormField">
                                <?php
                                $fullName = $vital['informant_name']." ".$vital['informant_lastname'];
                                $address = $vital['informant_address'];
                                $city = $vital['informant_city'] . ", " . $vital['informant_state'] .", ". $vital['informant_zip'];
                                $phone = $vital['informant_phone'];
                                ?>
                                <?php if(isset($user_session['packageSelected']['handling'][3]) ){ ?>
                                <div class="row ea-space-bottom-form">
                                    <span class="ea-form-left">
                                        <input type="text" name="next_init" class="input-form-left validate[required]">
                                    </span>
                                    <span class="ea-form-right">Next of Kin or Duly Authorized Agent to pick up said cremated remains from Cremation Services of Pennsylvania by appointment only.<br />

                                     Name:<br />
                                     <input type="text" name="familyName" class="validate[required]" value="<?php echo $fullName;?>"><br />

                                     Relationship:<br />
                                     <input type="text" name="familyRelation" class="validate[required]" value="">
                                    </span>
                                </div>
                                <?php } ?>

                                <?php if(isset($user_session['packageSelected']['handling'][4]) ){ ?>
                                <div class="row ea-space-bottom-form">
                                    <span class="ea-form-left">
                                        <input type="text" name="deliver_init" class="input-form-left validate[required]">
                                    </span>
                                    <span class="ea-form-right">Deliver said cremated remains to:<br />
                                     Name:<br />
                                     <input type="text" name="deliver_name" value="<?php echo $fullName;?>" class="validate[required]"><br />

                                     Address:<br />
                                     <input type="text" name="deliver_address" value="<?php echo $address;?>" class="validate[required]">
                                     <br />

                                     City/State/Zip Code:<br />
                                     <input type="text" name="deliver_city" value="<?php echo $city;?>" class="validate[required]"><br />

                                     Phone Number:<br />
                                     <input type="text" name="deliver_phone" value="<?php echo $phone;?>" class="validate[required] phoneFormat">
                                    </span>
                                </div>
                                <?php } ?>

                                <?php if(isset($user_session['packageSelected']['handling'][5]) ){ ?>
                                <div class="row ea-space-bottom-form">
                                    <span class="ea-form-left">
                                        <input type="text" name="ship_init" class="input-form-left validate[required]">
                                    </span>
                                    <span class="ea-form-right">I appoint Cremation Services of Pennsylvania as my agent to make shipment of said cremated remains via U.S. Postage Mail or scheduled air shipment.  I am aware that the Company’s services have been fully completed when the cremated remains leave the Company and that the Company is only acting as my agent for my accommodation only in carrying out these instructions.<br />
                                     SHIP TO<br />

                                     Name:<br />
                                     <input type="text" name="ship_name" value="<?php echo $fullName;?>" class="validate[required]"><br />
                                     Address:<br />
                                     <input type="text" name="ship_address" value="<?php echo $address;?>" class="validate[required]"><br />

                                     City/State/Zip Code:<br />
                                     <input type="text" name="ship_city" value="<?php echo $city;?>" class="validate[required]"><br />

                                     Phone Number:<br />
                                     <input type="text" name="ship_phone" value="<?php echo $phone;?>" class="validate[required] phoneFormat">
                                    </span>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
        </div>

        <div class="eaFormBox">
            SIGNATURE AND INDEMNITY
        </div>
        <!-- <div class="text-plan-merch">
            <strong>Electronic Signature Agreement</strong><br>
By typing in your name into the 'Signature' field below, you are signing this Agreement electronically. You agree your electronic signature is the legal equivalent of your manual signature on this Agreement. By typing in your name into the 'Signature' field below you consent to be legally bound by this Agreement's terms and conditions.
        </div> -->
        <div class="text-plan-merch">
            I declare under penalty of perjury that the forgoing is true and correct, and that I make this statement to induce Cremation Services of Pennsylvania to arrange to cremate or cause to be cremated the remains of said decedent.  I agree to hold harmless and indemnify and defend Cremation Services of Pennsylvania against any claims, liabilities or damages which may result from this authorization and order including shipping, identify, kinship, explodable or harmful implant, infectious disease, other persons claiming rights to control disposition of the remains or any other cause.
        </div>

        <div class="eaFormBox">
            <span style="padding-bottom:15px; display:block;">I/We warrant that all representations and statements made herein are true and correct.</span>
            Signature of Authorizing Agent:
            <div class="eaFormField">
                <span style="display:inline-block; padding-right:5px;">X</span>
                <input type="text" name="signature" id="signature"  class="input-signature signature-font validate[required]">
            </div>
        </div>

        <div class="eaFormBox">
            Type Name Again:
            <div class="eaFormField">
                <input type="text" name="enterSignature" id="enterSignature" class="text  validate[required]">
            </div>
        </div>

        <div class="eaFormBox">
            Relationship:
            <div class="eaFormField">
                <input type="text" name="relation" class="text validate[required]">
            </div>
        </div>
        <div class="eaFormBox">
            Address:
            <div class="eaFormField">
                <input type="text" name="sig_address" class="text validate[required]">
            </div>
        </div>
        <div class="eaFormBox">
            Phone Number:
            <div class="eaFormField">
                <input type="text" name="sig_phone" class="text phoneFormat validate[required]">
            </div>
        </div>
        <div class="eaFormBox">
            Date:
            <div class="eaFormField">
                <input type="text" name="date_sign" value="<?php echo date("M d , Y") ;?>" class="text" readonly>
            </div>
        </div>
        <div class="eaFormBox">
            Time:
            <div class="eaFormField">
                <input type="text" name="time_sign" class="text" value="<?php echo date("g:i A") ;?>" readonly>
            </div>
        </div>

        <div class="eaFormBox">
            <div class="eaFormField">

                <input id="signature-other" class="radio" type="checkbox" name="signature_other" value="1" data-status="not-checked">
                <label for="signature-other">I need to add another signature</label>

           </div>
        </div>

        <div id="signature-other-detail" class="<?php echo (isset($auth['signature_other'])) ? null : 'hide';?>">

            <div class="eaFormBox">
            Signature of Authorizing Agent:
            <div class="eaFormField">
                <span style="display:inline-block; padding-right:5px;">X</span>
                <input type="text" name="signature_another" class="input-signature signature-font validate">
            </div>
            </div>
            <div class="eaFormBox">
                Type Name Again:
                <div class="eaFormField">
                    <input type="text" name="entersignature_another" class="text  validate">
                </div>
            </div>
            <div class="eaFormBox">
                Relationship:
                <div class="eaFormField">
                    <input type="text" name="relation_another" class="text validate">
                </div>
            </div>
            <div class="eaFormBox">
                Address:
                <div class="eaFormField">
                    <input type="text" name="address_another" class="text validate">
                </div>
            </div>
            <div class="eaFormBox">
                Phone Number:
                <div class="eaFormField">
                    <input type="text" name="phone_another" class="text phoneFormat validate">
                </div>
            </div>
            <div class="eaFormBox">
                Date:
                <div class="eaFormField">
                    <input type="text" name="date_another" readonly value="<?php echo date("M d , Y") ;?>" class="text">
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="text-plan-merch">
            <strong>Electronic Signature Agreement</strong><br>
            By signing this Electronic Signature Acknowledgement, I agree that my electronic signature is the legally binding equivalent to my handwritten signature and that I will not, at any time in the future, repudiate the meaning of my electronic signature or claim that my electronic signature is not legally binding.
        </div>

         <div class="ea-box-content ea-box-content-no-bg">
            <div class="ea-box-inner-container">
                <div class="ea-form-order-desposition">
                    <div class="inner-title text-center">
                        Electronic Signature
                    </div>
                    <div class="row ea-form-detail">

                        <div class="eaFormBox">
                            Please type your first and last name:
                            <div class="eaFormField">
                                <input type="text" name="electironic_name"  value="" class="text signature-font validate[required]">
                            </div>
                        </div>

                        <div class="eaFormBox">
                            Date:
                            <div class="eaFormField">
                                <input type="text" id="electironic_date" name="electironic_date" readonly value="<?php echo date("F d, Y") ;?>" class="text">
                            </div>
                        </div>

                        <div class="text-plan-merch">
                            <input type="checkbox" value="1" name="electironic_understand" class="validate[required] checkbox check-box-form">
                            <span style="margin-left:5px;">
                                I understand that checking this box constitutes a legal signature confirming that I acknowledge and warrant the truthfulness of the information provided in this document.
                            </span>
                        </div>

                        <input type="hidden" name="electironic_time" id="electironic_time" value="<?php echo date('g:i A');?>">

                        <input type="hidden" name="electironic_ip" value="<?php echo $_SERVER['REMOTE_ADDR'];?>">
                        


                    </div>
                </div>
            </div><!--====  End of ea-box-inner-no-bg  ====-->
        </div><!--====  End of ea-box-content  ====-->




    </div>

    <div class="ea-btt-prev-next-area">
        <div class="row">
            <div class="col-sm-5 col-md-5">
                <a class="ea-btt-prev-next" href="<?php echo $prev_form;?>">Previous Step</a>
            </div>
            <div class="col-xs-offset-0 col-sm-5 col-sm-offset-2 col-md-5 col-md-offset-2">
                <a class="ea-btt-prev-next btnSubmit" href="/ea/thank-you">Next Step</a>
            </div>
        </div>
    </div>
    </form>
</div>
<?php echo $sidebar;?>
<script src="<?php echo assets_path('js/template/authorization/authorization.min.js?_='.strtotime('now'));?>"></script>

