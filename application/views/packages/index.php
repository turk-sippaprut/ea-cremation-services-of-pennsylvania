<div class="col-sm-7 col-md-7">
<div class="ea-content">
    <h1>Arrange Online - Step 1</h1>
    <form action="" id="mainform" name="mainform" method="POST">
        <input type="hidden" name="pkgtype"             id="pkgtype"        value="<?php echo $user_session['pkgtype'];?>" />
        <input type="hidden" name="scriptaction"        id="scriptaction"   value="validate" />
        <input type="hidden" name="notvalidate"         id="notvalidate"    value="" />
        <input type="hidden" name="next_form"           id="next_form"      value="<?php echo $next_form?>" />
        <input type="hidden" name="session_index"       id="session_index"  value="<?php echo $session_index?>" />
        <input type="hidden" name="selected"            id="selected"       value="<?php echo $$session_index;?>" />
        <input type="hidden" name="zoneCounties"        id="zoneCounties"   value="<?php echo $zoneCounties;?>" />


    <div class="ea-box-content">
        <a class="show-package shadow-title-plan" >
        Select a Plan</a>

        <div id="ea-recommended-plan"  style="display:block;">
            <div class="errorPackage error-message relative"></div>
            <div class="ea-box-content-inner">
                <?php foreach($packages as $id => $row){ ?>
                <?php $groupIndex = "package";?>
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <div class="ea-img-plan">
                            <img src="<?php echo assets_path('images/'.$row['picture']);?>" />
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <div class="title-text-plan"><?php echo $row['name'];?> - $<?php echo number_format($row['price'] , 2);?></div>
                        <div class="text-plan"><?php echo $row['short_detail'];?></div>
                        <div class="btt-plan-area">
                        <div class="btt-more-info-area">
                            <div class="btt-more-info">
                                <a class="more-info showMore"  href="#descriptionPackage__<?php echo $row['id'];?>">More Info</a>
                            </div>
                        </div>
                        <div class="btt-plan-select-area">
                            <?php $selected = (isset($user_session[$session_index][$groupIndex][$id])) ? 1 : 0;?>
                            <div class="select-package <?php echo ($selected) ? 'button-selected' : null;?>"
                                data-group="<?php echo $groupIndex;?>"
                                data-id="<?php echo $row['id'];?>"
                                data-name="<?php echo jsSpecialchars( $row['name'] );?>">
                                <?php echo ($selected) ? 'PLAN SELECTED' : 'SELECT PLAN';?>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-12" id="descriptionPackage__<?php echo $row['id'];?>" style="display:none;">
                        <div class="text-plan">Also Included:
                            <?php echo $row['full_detail'];?>
                        </div>
                    </div>
                </div>
                <div class="line-plan <?php echo ($id == 3 ) ? 'hide' : null;?>"></div>
                <?php } ?>
            </div>
        </div>
    </div>

    <br /><br /><br /><br />

    <h3>State and Transportation Fees
        <?php if($user_session['pkgtype'] == "preneed"){ ?>
        <small class="h3-small">(Selections are optional. Transportation Fees may vary if death occurs outside selected county)</small>
        <?php } ?>
    </h3>

    <div class="row ea-options-list-area">
        <div class="col-sm-6 col-md-7">
            <div class="text-label">Select County (Location of Deceased):</div>
        </div>
        <div class="col-sm-6 col-md-5 ea-select-style">
                <select name="counties" id="counties" class="js-example-basic-single validate[required]" data-group="counties">
                    <?php echo ($user_session['pkgtype'] == "preneed") ? '<option value="">Select County</option>' : null;?>
                    <?php foreach($cfg['counties'] as $key => $county){  ?>
                    <?php $selected = (isset($counties_selected[$county['name']])) ? 'selected' : null; ?>
                        <option <?php echo $selected;?>
                        data-deliverprice="<?php echo Vars::get($county['delivery_price']);?>"
                        data-rate="<?php echo $county['rate'];?>" value="<?php echo $county['name'];?>"><?php echo $county['name'];?></option>
                    <?php } ?>
                </select>
        </div>
    </div>


    <?php foreach($transport as $id => $row){ ?>
        <?php
            if( $user_session['pkgtype'] == "atneed" ){
                if( ($row['id'] == 13) ){
                    continue;
                }
            }
        if( $user_session['pkgtype'] == "preneed"){
            if( ($row['id'] == 14) ){
                    continue;
            }
        }
        ?>
   <div class="row ea-options-list-area relative">
        <div class="error-message errorOption "></div>
        <div class="col-sm-8 col-md-8 option-list-text">
            <?php echo $row['name'];?> <br/>
            <?php if($row['show_detail'] == 0 and strip_tags($row['description']) != "" ){ ?>
                <a class="more-info showMore" href=".descriptionSerive__<?php echo $row['id'];?>" >More Info</a>
                <div id="transfer-detail" style="display:none;" class="descriptionSerive__<?php echo $row['id'];?> col-md-12">
                    <div class="text-mor-info-detail">
                        <?php echo $row['description'];?>
                    </div>
                </div>
            <?php } else{ ?>
            <div class="text-mor-info-detail"><?php echo $row['description'];?></div>
            <?php } ?>
        </div>

        <div class="col-sm-4 col-md-4 ea-select-style relative">

            <?php if($row['id'] == 1){ ?>
            <?php
            $price = isset($user_session[$session_index][$row['type']][ $row['id']]['summary'])
                         ? $user_session[$session_index][$row['type']][ $row['id']]['summary']
                         : 0;
            ?>
                <?php if ( $row['show_price'] == 1) { ?>
                <div class="text-merch transport<?php echo $row['id'];?>__price">$<?php echo number_format($price);?></div>
                <?php } ?>
            <?php } else if($row['id'] == 2){ ?>
                <select name="death_quan" class="js-example-basic-single merchandise-option-select" id="deathCerti" style="font-size:1.0em;"
                    data-group="<?php echo $row['type'];?>"
                    data-id="<?php echo $row['id'];?>"
                    data-name="<?php echo jsSpecialchars($row['name']);?>" >
                    <?php //echo ($user_session['pkgtype'] == "preneed") ? '<option value="0">0</option>' : null;?>
                    <?php for($index = 1; $index <= 20; $index++ ){ ?>
                        <?php
                        $selected = (isset( $transport_selected[$row['id']]['quan'] ) and $transport_selected[$row['id']]['quan'] == $index)
                                            ? 'selected' : null;
                        ?>
                        <option <?php echo $selected;?> value="<?php echo $index;?>"><?php echo $index;?></option>
                    <?php } ?>
                </select>
            <?php } else if($row['id'] == '14'){ ?>

                <select name="death_occur" class="js-example-basic-single validate[required]" id="death_occur"
                    data-group="<?php echo $row['type'];?>"
                    data-id="<?php echo $row['id'];?>"
                    data-name="<?php echo jsSpecialchars($row['name']);?>" >
                        <option value="">Select Option</option>
                        <?php foreach($cfg['deathOccurOption'] as $index => $deathRow){
                        $selected = null;
                        if(isset( $transport_selected[$row['id']]['choice'] ) and $transport_selected[$row['id']]['choice'] == $index){
                            $selected = 'selected';
                        }
                        ?>
                        <option <?php echo $selected;?> value="<?php echo $index;?>"><?php echo $deathRow['name'];?></option>
                        <?php } ?>
                </select>
            <?php  }
            else {
                $classAutoSelected = ($row['is_selected'] == 1) ? 'options-auto-selected' : '';
            ?>
                <div class="text-merch transport<?php echo $row['id'];?> <?php echo $classAutoSelected ;?>"
                    data-id="<?php echo $row['id'];?>"
                    data-name="<?php echo jsSpecialchars($row['name']);?>"
                    data-group="<?php echo $row['type'];?>"
                    >$<?php echo number_format($row['price']);?></div>
            <?php 
            }
            if($row['id'] == 1){
            ?>
                <input type="hidden" name="tranOption" id="tranOption" value="<?php echo $row['id'];?>"
                    data-name="<?php echo jsSpecialchars($row['name']);?>"
                    data-id="<?php echo $row['id'];?>"
                    data-group="<?php echo $row['type'];?>"
                >
            <?php
            }
            ?>
        </div>
    </div>
    <?php } ?>
    <br /><br /><br />

    <h3>Handling of Cremated Remains</h3>
    <div class="optionSection">
        <?php foreach($handling as $key => $row){ ?>
        <div class="row ea-options-list-area">
            <div class="col-sm-8 col-md-9 option-list-text">
                <?php $priceText = ($row['price'] > 0) ? "$".number_format($row['price'] , 2) : "No Charge";?>
                <?php echo $row['name'];?>: <span class="price-text"><?php echo ($row['show_price']) ? $priceText : null;?></span>  <br/>
                <?php if($row['show_detail'] == 0){ ?>
                <a class="more-info showMore" href=".descriptionSerive__<?php echo $row['id'];?>">More Info</a>
                <div id="pickup-location-detail" style="display:none;" class="descriptionSerive__<?php echo $row['id'];?> col-md-12">
                    <div class="text-mor-info-detail">
                        <?php echo $row['description'];?>
                    </div>
                </div>
                <?php } else{ ?>
                <div class="text-mor-info-detail">
                        <?php echo $row['description'];?>
                </div>
                <?php } ?>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="select-options">
                    <label>
                        <?php
                            $checked = isset($user_session[ $session_index ][ $row['type'] ][ $row['id'] ]) ? 'checked' : null;
                            $includedOption = ( $row['included'] == 1) ? 'includedOption' : null;
                        ?>
                        <input type="checkbox" <?php echo $checked;?> class="option-yes-no selectOption <?php echo $includedOption;?>"
                        id="selectOption-<?php echo $row['id'];?>"
                        name="<?php echo $row['type'];?>_select"
                        data-id="<?php echo $row['id'];?>"
                        data-group="<?php echo $row['type'];?>"
                        data-showprice="<?php echo $row['price'];?>"
                        data-name="<?php echo jsSpecialchars($row['name']);?>"
                        value="<?php echo $row['id'];?>" />
                    </label>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>


    <br /><br /><br />
    <h3>Customize Your Plan</h3>
    <div class="optionSection">
        <?php foreach($addition as $key => $row){ ?>
        <div class="row ea-options-list-area">
            <div class="col-sm-8 col-md-9 option-list-text">
                <?php $priceText = ($row['price'] > 0) ? "$".number_format($row['price'] , 2) : "No Charge";
                      $priceText = ($row['id'] == 9) ? '$TBD' : $priceText; ?>
                <?php echo $row['name'];?>: <?php echo ($row['show_price']) ? $priceText : null;?>  <br/>
                <?php if($row['show_detail'] == 0){ ?>
                <a class="more-info showMore" href=".descriptionSerive__<?php echo $row['id'];?>" >More Info</a>
                <div id="pickup-location-detail" style="display:none;" class="descriptionSerive__<?php echo $row['id'];?> col-md-12">
                    <div class="text-mor-info-detail">
                        <?php echo $row['description'];?>
                    </div>
                </div>
                <?php } else{ ?>
                <div class="text-mor-info-detail">
                    <?php echo $row['description'];?>
                </div>
                <?php } ?>
            </div>
            <div class="col-sm-4 col-md-3">
                <div class="select-options">
                    <label>
                        <?php
                        $checked = isset($user_session[ $session_index ][ $row['type'] ][ $row['id'] ]) ? 'checked' : null;
                        $includedOption = ($row['included'] == 1) ? 'includedOption' : null;
                        ?>
                        <input type="checkbox" <?php echo $checked;?> class="option-yes-no selectOption <?php echo $includedOption;?>"
                        name="<?php echo $row['type'];?>_select"
                        data-id="<?php echo $row['id'];?>"
                        data-group="<?php echo $row['type'];?>"
                        data-name="<?php echo jsSpecialchars($row['name']);?>"
                        value="<?php echo $row['id'];?>" />
                    </label>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
    <br /><br />

    <div class="ea-btt-prev-next-area">
        <div class="row">
            <div class="col-xs-offset-0 col-sm-5 col-sm-offset-7 col-md-5 col-md-offset-7">
                <a class="ea-btt-prev-next btnSubmit" href="">Next Step</a>
            </div>
        </div>
    </div>
    </form>
</div><!--=====  End of ea-content  ======-->

</div>
<?php echo $sidebar;?>
<script src="<?php echo assets_path('js/template/packages/packages.min.js');?>"></script>



