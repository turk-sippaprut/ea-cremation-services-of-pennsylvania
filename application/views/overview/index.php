<div class="col-sm-7 col-md-7">
    <h1>Review Your Selections - Step 4</h1>
    <div class="ea-content">
        <div class="text-plan-merch">If all the selections below are correct, please continue. If you need to adjust the selections you may do so below.</div>

        <?php foreach($user_session['packageSelected']['package'] as $key => $row){ ?>
        <div class="ea-box-content ea-box-content-no-bg">
            <div class="ea-box-inner-no-bg">
                <div class="step-titletop">
                    <?php echo $row['name'];?> <span class="price-top">$<?php echo number_format($row['summary'] , 2);?></span>
                </div>
                <div class="step-innertext">
                     <?php echo $packages[$row['id']]['full_detail']; ?>
                </div>
                <div class="btn-edit"><a href="<?php echo $cfg['root'];?>/">EDIT/CHANGE</a></div>
            </div>
        </div>
        <?php } ?>

        <div class="ea-box-content ea-box-content-no-bg">
            <div class="ea-box-inner-no-bg">

            <div class="step-titletop">
                Customize Your Plan <span class="price-top">$<?php echo number_format($user_session['summary']['optionTotal'] , 2);?></span>
            </div>
            <?php if( isset($user_session['packageSelected']['addition']) ) { ?>
            <?php foreach($user_session['packageSelected']['addition'] as $key => $row){ ?>
            <div class="row">
                <div class="col-sm-8 col-md-9 option-list-text"><?php echo $row['name'];?></div>
                <div class="col-sm-4 col-md-3">
                    <div class="select-options">
                        <div class="text-merch">
                            $<?php echo !is_numeric($row['summary']) ? $row['summary']  : number_format((float)$row['summary'] , 2);?>
                        </div>
                    </div>
                </div>
            </div>
                <?php } ?>
            <?php } ?>

            <div class="step-titletop">Handling of Cremated Remains</div>
            <?php if( isset($user_session['packageSelected']['handling']) ) { ?>
            <?php foreach($user_session['packageSelected']['handling'] as $key => $row){ ?>
                <div class="row">
                    <div class="col-sm-8 col-md-9 option-list-text"><?php echo $row['name'];?></div>
                    <div class="col-sm-4 col-md-3">
                        <div class="select-options">
                            <div class="text-merch">$<?php echo number_format($row['summary'] , 2);?></div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            <?php } ?>

            <div class="step-titletop">State Required Fees</div>
            <?php if( isset($user_session['packageSelected']['counties']) ) { ?>
            <?php foreach($user_session['packageSelected']['counties'] as $key => $row){ ?>
                <div class="row">
                    <div class="col-sm-8 col-md-9 option-list-text">
                        Select County (Location of Deceased):
                    </div>
                    <div class="col-sm-4 col-md-3">
                        <div class="select-options">
                            <div class="text-merch"><?php echo $row['id'];?></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php } ?>

            <?php if( isset($user_session['packageSelected']['transport']) ) { ?>
             <?php foreach($user_session['packageSelected']['transport'] as $key => $row){ ?>
                <div class="row">
                    <div class="col-sm-8 col-md-9 option-list-text">
                        <?php echo $row['name'];
                        if($row['id'] == 2){
                            // echo ": ". $row['quan'] . " at $".number_format($cfg['death_certificate'] ,2) . " each";
                            $label = ": ". $row['quan'] . " at $".number_format($row['price'] ,2) . " each";
                            if(@$row['shipping']) {
                                $label .= ", Shipping $".number_format($row['shipping'] ,2);
                            }
                            echo $label;
                        } else if($row['id'] == 14){
                            echo " ( ". $cfg['deathOccurOption'][ $row['choice'] ]['name'] ." )";
                        }
                        ?>
                    </div>
                    <div class="col-sm-4 col-md-3">
                        <div class="select-options">
                            <div class="text-merch">$<?php echo number_format($row['summary'] , 2);?></div>
                       </div>
                    </div>
                </div>
                <?php } ?>
            <?php } ?>
            <div class="clearfix"><br/></div>
            <div class="btn-edit"><a href="<?php echo $cfg['root'];?>/">EDIT/CHANGE</a></div>

        </div><!--====  End of ea-box-inner-no-bg  ====-->
        </div>

        <div class="ea-box-content ea-box-content-no-bg">
            <div class="ea-box-inner-no-bg">
                <div class="step-titletop">
                    Memorial Products
                    <span class="price-top">Total: $<?php echo number_format($user_session['summary']['each']['merchandise'] , 2);?></span>
                </div>
                <?php foreach($user_session['cart'] as $key => $category){
                        if($key == "merchandise"){ continue; }
                        foreach($category as $id => $row){
                ?>
                    <div class="row merch-product-list">
                        <div class="col-xs-4 col-sm-4 col-md-4 images-merch ">
                                <a href="<?php echo pathImage( $products[$id]['type']."/".$id."_image.jpg" );?>" class="thumbnail imageLink">
                                    <img alt="<?php echo $row['name'];?>"
                                    src="<?php echo pathImage( $products[$id]['type']."/".$id."_thumbnail.jpg" );?>">
                                </a>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 merch-descrip">
                            <?php echo ($row['quan'] > 1) ? $row['quan'] ." x " . $row['name'] :  $row['name'];?>
                            <br><?php echo $products[$id]['type'];?>
                            <?php echo ($row['summary'] > 0) ? number_format($row['summary'] , 2) : '(Included)';?>
                        </div>
                    </div>
                    <?php } ?>
                <?php } ?>

                <?php foreach($user_session['cart'] as $key => $category){
                        if($key != "merchandise"){ continue; }
                        foreach($category as $id => $row){
                ?>
                        <div class="row overview__row">
                            <div class="col-sm-8 col-md-8 option-list-text">
                                <?php echo $row['name'];?>
                            </div>
                            <div class="col-sm-4 col-md-4 ea-select-style">
                                <div class="select-options">
                                    <div class="text-merch">$<?php echo number_format($row['summary'] , 2);?></div>
                                </div>
                            </div>
                        </div>
                <?php   } ?>
                <?php } ?>

                <div class="btn-edit"><a href="<?php echo $cfg['root'];?>merchandise">EDIT/CHANGE</a></div>
            </div>
        </div>

        <div class="total-bottom-area">
            <div class="total">
                Total: $<?php echo number_format($user_session['summary']['total'] , 2);?>
            </div>
        </div>
    </div>


    <div class="ea-btt-prev-next-area">
        <div class="row">
            <div class="col-sm-5 col-md-5">
                <a class="ea-btt-prev-next" href="<?php echo $prev_form;?>">Previous Step</a>
            </div>
            <div class="col-xs-offset-0 col-sm-5 col-sm-offset-2 col-md-5 col-md-offset-2">
                <a class="ea-btt-prev-next btnSubmit" href="<?php echo $next_form;?>">Next Step</a>
            </div>
        </div>
    </div>


</div>
<?php echo $sidebar;?>
<script src="<?php echo assets_path('js/template/overview/overview.min.js');?>"></script>

