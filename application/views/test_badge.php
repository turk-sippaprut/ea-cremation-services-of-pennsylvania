
<div class="test_badge">
		<div class="block">
			<a href="<?php echo base_url();?>atneed">Atneed</a> |
			<a href="<?php echo base_url();?>preneed">Preneed</a>
		</div>


		<div class="topleft">
			<span>
				<div><a target="_blank" href="<?php echo base_url();?>debuger/print_session">Session</a></div>
			</span>
			<span>|</span>
			<span>
				<div><a href="#" onclick="propulateForm(1); return false;">Populate</a></div>
			</span>
			<span>|</span>
			<span>
				<div><a href="#" onclick="getfieldname(); return false;">See Field Name</a></div>
			</span>

		</div>
		<br/>
		<div class="topright">
			<span>
				<div><a href="<?php echo base_url();?>debuger/destory_session">Clear Session</a></div>
			</span>
			<span>|</span>
			<span>
				<div><a target="_blank" href="<?php echo base_url();?>debuger/clear_cache">Clear Cache</a></div>
			</span>
			<span>|</span>

		</div>
		<br/>
		<div class="bottomleft"><?=ucfirst(@$user_session['pkgtype'])?> (<?php echo ENVIRONMENT;?>)</div>
		<div class="bottomright">
			{elapsed_time} sec.
		</div>

		Dev badge

		<style>
		.test_badge {
		    background-color: #BF0000;
		    border: 5px solid #BF0000;
		    color: #FFFFFF;
		    font-family: verdana;
		    font-size: 24px;
		    font-weight: bold;
		    padding: 24px 30px 25px;
		    position: fixed;
		    right: 0;
		    z-index: 9999999;
		}
		.test_badge > div {
		    font-size: 10px;
		    position: absolute;
		}
		.test_badge > div.topleft {
		    left: 0;
		    top: 0;
		}
		.test_badge > div.topright {
		    left: 0;
		    top: 10;
		}
		.test_badge > div.bottomleft {
		    left: 0;
		    bottom: 0;
		}
		.test_badge > div.bottomright {
		    right: 0;
		    bottom: 0;
		}
		.test_badge > div > span {
		    float: left;
		    margin: 0 1px;
		    min-height: 30px;
		}
		.test_badge > div.topleft > span > div{
		    text-align: left;
		}
		.test_badge > div.topright > span > div{
		    text-align: right;
		}
		.test_badge > div  a:link,
		.test_badge > div  a:visited {
		    color: inherit;
		    text-decoration: none;
		}
		.test_badge > div  a:hover,
		.test_badge > div  a:active {
		    text-decoration: underline;
		}
		@media (max-width: <?=@$breakpoint['sm-max']?>px){
			.test_badge{
				display:none;
			}
		}
		@media print{
			.test_badge{
				display:none;
			}
		}
		</style>


</div>

<script src="<?php echo $cfg['root']?>assets/js/test_badge.js"></script>
<script>
	$(function(){
		$('body').prepend($('.test_badge'));
	});</script>
