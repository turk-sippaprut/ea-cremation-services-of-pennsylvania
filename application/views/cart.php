<?php
//Package
if(isset($user_session['packageSelected']['package'])){
    foreach($user_session['packageSelected']['package'] as $key => $row){
?>
    <div class="ea-step">
        <div class="ea-shoping-detail">
            <div class="row">
                <div class="col-xs-1 col-sm-1 col-md-1 ea-shoping-detail-list">
                    <img src="<?php echo assets_path('images/shopping-list-icon.png');?>" />
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 ea-shopping-name">
                    <div class="col-xs-12 col-sm-12 col-md-8 ea-shopping__label">
                        <span class="ea-shoping-text-bold"><?php echo $row['name'];?></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 ea-shopping-price">$<?php echo number_format( $row['summary'] , 2);?></div>
                </div>
            </div>
        </div>
    </div>
<?php }
}
?>



<?php
//transport
if(isset($user_session['packageSelected']['transport'])){
    foreach($user_session['packageSelected']['transport'] as $key => $row){
        $name = clean_input( $row['name'] );
        $name = ($row['id'] == 2 and $row['quan'] > 1) ? $row['quan'] ." x " . $name : $name;
        if($row['id'] == 1){
            $name .= "<br/>( ".getFirstKey($user_session['packageSelected']['counties'])." )";
        } else if($row['id'] == 14){
            $name .= "<br/>( ". $cfg['deathOccurOption'][ $row['choice'] ]['name'] ." )";
        }
        $price = number_format( $row['summary'] , 2);
?>
    <div class="ea-step">
        <div class="ea-shoping-detail">
            <div class="row">
                <div class="col-xs-1 col-sm-1 col-md-1 ea-shoping-detail-list">
                    <img src="<?php echo assets_path('images/shopping-list-icon.png');?>" />
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 ea-shopping-name">
                    <div class="col-xs-12 col-sm-12 col-md-8 ea-shopping__label">
                        <span class="ea-shoping-text-bold"><?php echo $name;?></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 ea-shopping-price">$<?php echo $price;?></div>
                </div>
            </div>
        </div>
    </div>
<?php
    }
}
?>

<?php
//Handling
if(isset($user_session['packageSelected']['handling'])){
    foreach($user_session['packageSelected']['handling'] as $key => $row){
?>
    <div class="ea-step">
        <div class="ea-shoping-detail">
            <div class="row">
                <div class="col-xs-1 col-sm-1 col-md-1 ea-shoping-detail-list">
                    <img src="<?php echo assets_path('images/shopping-list-icon.png');?>" />
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 ea-shopping-name">
                    <div class="col-xs-12 col-sm-12 col-md-8 ea-shopping__label">
                        <span class="ea-shoping-text-bold"><?php echo $row['name'];?></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 ea-shopping-price">
                        $<?php echo number_format( $row['summary'] , 2);?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }
}
?>

<?php
//Addiontion
if(isset($user_session['packageSelected']['addition'])){
    foreach($user_session['packageSelected']['addition'] as $key => $row){
?>
    <div class="ea-step">
        <div class="ea-shoping-detail">
            <div class="row">
                <div class="col-xs-1 col-sm-1 col-md-1 ea-shoping-detail-list">
                    <img src="<?php echo assets_path('images/shopping-list-icon.png');?>" />
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 ea-shopping-name">
                    <div class="col-xs-12 col-sm-12 col-md-8 ea-shopping__label">
                        <span class="ea-shoping-text-bold"><?php echo $row['name'];?></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 ea-shopping-price">
                        $<?php echo is_numeric( $row['summary'] ) ? number_format( $row['summary'] , 2) : $row['summary'];?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }
}
?>

<?php
if(isset($user_session['cart'])){
    //Get from category
    foreach($user_session['cart'] as $key => $category){
        foreach($category as $id => $row){
            $name = $row['name'];
            if($row['quan'] > 1){
                $name = $row['quan'] ." x ". $name;
            }

            //$name = $row['quan'] ." x ".$row['name'];
            $price = ( ! in_array($row['id'] , $cfg['includedProduct'] )) ? "$".number_format($row['summary'] ,2) : "Included";
?>
    <div class="ea-step">
        <div class="ea-shoping-detail">
            <div class="row">
                <div class="col-xs-1 col-sm-1 col-md-1 ea-shoping-detail-list">
                    <img src="<?php echo assets_path('images/shopping-list-icon.png');?>" />
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 ea-shopping-name">
                    <div class="col-xs-12 col-sm-12 col-md-8 ea-shopping__label">
                        <span class="ea-shoping-text-bold"><?php echo $name;?></span>
                    </div>
                    <div class="col-xs-112 col-sm-12 col-md-4 ea-shopping-price">
                        <?php echo $price;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }
    }
}
?>

<div class="ea-step ea-step-dark ea-step-last">
    <span class="ea-step-total">My Total: $<?php echo number_format($user_session['summary']['total'] , 2);?></span>
</div>


