<div class="ea-box-content creditForm <?php echo Arr::key($paymentprefix , $user_session) ? null : 'hide';?>">
    <div class="show-package shadow-title-plan">SECURE CREDIT CARD PAYMENT</div>
    <div class="ea-box-inner-no-bg ea-secure-box-inner">
        <div class="col-xs-12 col-sm-7 col-md-7 creditFormGroup creditbox">

            <div class="eaFormBox">
                amount
                <div class="eaFormField">
                    <input  id="amount_text" name="amount_text" value="$<?php echo number_format($user_session['summary']['total'] , 2);?>"
                     class="text" readonly type="text">
                </div>
            </div>

            <div class="eaFormBox">
                Credit Card Number
                <div class="eaFormField">
                    <input  id="card_number" name="card_number"  data-value="4111-1111-1111-1111"  value=""
                     class="paymentCondition text validate"
                     placeholder="No dashes or spaces" type="text">
                </div>
            </div>

            <div class="eaFormBox">
                Expiration Date
                <div class="eaFormField">
                    <span class="payment-options col-xs-6">
                        <select  id="expiration_month" name="expiration_month" class="validate col-md-12">
                            <option  value="">Month</option>
                            <?php foreach($cfg['month'] as $monthNumber => $month){ ?>
                                    <option value="<?php echo $monthNumber;?>"><?php echo $month;?></option>
                            <?php } ?>
                        </select>
                    </span>
                    <span class="payment-options col-xs-6">
                        <select  id="expiration_year" name="expiration_year" class="validate col-md-12">
                            <option value="">Year</option>
                            <?php
                                $startYear = date("Y");
                                $endYear   = $startYear + 30;
                                for($year = $startYear; $year <= $endYear; $year++){
                                    $year2digit = substr($year, -2);
                                ?>
                                    <option value="<?php echo $year2digit;?>"><?php echo $year;?></option>
                                <?php
                                }
                            ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="eaFormBox">
                Security Code
                <div class="eaFormField">
                    <input value="" id="security_code" name="security_code" class=" text" placeholder="CVV" type="text">
                    <br>
                    <span class="textSmallDetail">The last 3 digits displayed on the back of the card.</span>
                </div>
            </div>

            <div class="eaFormBox">
                Authorized Signature
                <div class="eaFormField">
                   <input type="text" name="signature" id="signature"  class="input-signature signature-font validate">
                </div>
            </div>

        </div>
        <div class="iconCardbox col-xs-12 col-sm-5 col-md-4">
            <div class="creditcardIcon">
                <li class="visa"></li>
                <li class="mastercard"></li>
                <li class="discover"></li>
                <li class="amex"></li>
            </div>
            <div class="comodoIcon">
                <img border="0" src="<?php echo assets_path('images/comodo-icon.png');?>">
            </div>
        </div>

    </div>
</div>

<!-- <div class="row">
    <div class="eaFormBox">
        <div class="eaFormField">
            <label class="planningRadio ">
               <input data-status="not-checked" type="checkbox"  name="paybycash" id="paybycash" class="checkbox"  value="check/cash">
               <span class="style__2-label">I would like to pay by check/cash</span>
            </label>
        </div>
    </div>
</div> -->