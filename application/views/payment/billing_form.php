
<div class="ea-box-content ea-box-content-no-bg bilingForm
<?php echo (Arr::key($paymentprefix , $user_session) || $user_session['pkgtype'] == "preneed") ? null : 'hide';?>">
    <div class="ea-box-inner-no-bg">
        <div class="formGroupTitle" style="padding-top:10px;">
            <?php $hide = ( $user_session['pkgtype'] == "preneed") ? 'hide' : null;?>
            <span class="tiltePayment <?php echo $hide;?>">Credit Card</span> Billing Information
            <?php if($user_session['pkgtype'] == "preneed"){ ?>
                - OPTIONAL
            <?php } ?>
        </div>
        <div class="eaFormBox">
            <span class="bllingName" data-payment="Name on Card" data-nopayment="Billing Name">
                <?php echo ($user_session['pkgtype'] == "preneed") ?  'Billing Name' : 'Name on Card';?>
            </span>
            <div class="eaFormField">
                <?php $placeholder = ($user_session['pkgtype'] == "preneed") ? '' : 'Exactly as it appears on the card';?>
                <input type="text" name="name_card" id="name_card" class="text validate"
                placeholder="<?php echo $placeholder;?>" data-value="funeralnet">
            </div>
        </div>
        <div class="eaFormBox">
            Billing Address
            <div class="eaFormField">
                <input type="text" name="billing_address" id="billing_address"  class="text validate ">
            </div>
        </div>
        <div class="eaFormBox">
            Address 2
            <div class="eaFormField">
                <input type="text" name="billing_address2" id="billing_address2" class="text" placeholder="For Apt or Suite(optional)">
            </div>
        </div>
        <div class="eaFormBox">
            City
            <div class="eaFormField">
                <input type="text" name="billing_city" id="billing_city" class="validate text">
            </div>
        </div>
         <div class="eaFormBox">
            State/Province
            <div class="eaFormField">
                <span class="payment-options">
                    <select name="billing_state" id="billing_state" class="input-dropdown validate">
                    <option value="" selected="">Select State</option>
                    <?php foreach($states as $state => $arr){ ?>
                        <option  value="<?php echo $state;?>"><?php echo $state;?></option>
                    <?php } ?>
                </select>
                </span>
            </div>
        </div>
        <div class="eaFormBox">
            Zip/Postal Code
            <div class="eaFormField">
                <input type="text" name="billing_zipcode" id="billing_zipcode" class="text validate">
            </div>
        </div>
    </div>
</div>

<?php if($user_session['pkgtype'] == "preneed"){ ?>
<p class="text-plan-merch">We will contact you to set up a time to discuss payment options for your advance planning cremation arrangements</p>
<div class="clearfix"></div>
<?php } ?>