<div class="ea-box-content ea-box-content-no-bg arrangement_form">
    <div class="ea-box-inner-no-bg">
        <div class="formGroupTitle" style="padding-top:10px;">
            Arrangement Information
        </div>
        <div class="eaFormBox">
            Your Name
            <div class="eaFormField">
                <input type="text" name="informant_name" id="informant_name" value="<?php echo Vars::get($vital['informant_name']);?>" class="text validate[required]">
            </div>
        </div>
        <div class="eaFormBox">
            Email Address
            <div class="eaFormField">
                <input type="text" name="informant_email" id="informant_email" value="<?php echo Vars::get($vital['informant_email']);?>" class="text validate[required ,custom[email]]"
                    data-value="sip@funeralnet.com"
                >
            </div>
        </div>
        <div class="eaFormBox">
            Phone Number
            <div class="eaFormField">
                <input type="text" name="informant_phone" id="informant_phone" value="<?php echo Vars::get($vital['informant_phone']);?>" class="text phoneFormat validate[required]">
            </div>
        </div>
        <div class="eaFormBox">
            Person for Whom Arrangements are for
            <div class="eaFormField">
                <?php $fullName = Vars::get($vital['deceased_first']) . " ". Vars::get($vital['deceased_middle']) . " ". Vars::get($vital['deceased_last']); ?>
                <input type="text" name="person" id="person" value="<?php echo $fullName;?>" class="text validate[required]">
            </div>
        </div>
        <?php if($user_session['pkgtype'] == "atneed"){ ?>
         <div class="eaFormBox">
            Date of Death
            <div class="eaFormField">
                <span class="payment-options col-xs-5" >
                    <select  id="month_of_death" name="mod" data-prompt-position="topLeft" readonly class="validate[required] col-md-12">
                        <option value="">Month</option>
                        <?php foreach($cfg['month'] as $monthNumber => $month){
                            $selected = ($month == $auth['mod']) ? 'selected' : null;
                        ?>
                                <option <?php echo $selected;?> value="<?php echo $month;?>"><?php echo $month;?></option>
                        <?php } ?>
                    </select>
                </span>
                <span class="payment-options col-xs-3" >
                    <select  id="day_of_death" name="dod" data-prompt-position="topLeft" readonly class="validate[required] col-md-12">
                        <option selected="" value="">Day</option>
                        <?php for($day= 1; $day<= 31; $day++){
                                $day = substr($day, -2);
                                $selected = ($day == $auth['dod']) ? 'selected' : null;
                        ?>
                            <option <?php echo $selected;?>  value="<?php echo $day;?>"><?php echo $day;?></option>
                        <?php } ?>
                    </select>
                </span>
                <span class="payment-options col-xs-4" >
                    <select  id="year_of_death" name="yod" data-prompt-position="topLeft" class="validate[required] col-md-12">
                        <?php for($year = date("Y"); $year >= 1900; $year--){
                            $selected = ($year == $auth['yod']) ? 'selected' : NULL;
                        ?>
                        <option <?php echo $selected;?> value="<?php echo $year;?>"><?php echo $year;?></option>
                        <?php }?>
                    </select>
                </span>
            </div>
        </div>
        <?php } ?>

    </div>
</div>