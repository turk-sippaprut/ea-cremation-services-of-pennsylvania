<div class="ea-box-content ea-box-content-no-bg">
    <div class="ea-box-inner-no-bg">
        <div class="formGroupTitle" style="padding-top:10px;">
            Select Payment Method
        </div>

        <div class="eaFormBox">
            <div class="eaFormField">
                <label class="planningRadio ">
                   <input data-status="not-checked" type="radio" name="paybycash" id="paybycash" class="radio validate[required]"  value="credit card">
                   <span class="style__2-label">Pay now with credit/debit</span>
                </label>
            </div>
        </div>

        <div class="eaFormBox">
            <div class="eaFormField">
                <label class="planningRadio ">
                   <input data-status="not-checked" type="radio"  name="paybycash" id="paybycash" class="radio validate[required]"  value="check/cash">
                   <span class="style__2-label">Pay later with cash/check or other</span>
                    <p class="style__2-subtitle">We will contact you to set up a time to collect payment. The cremation process cannot begin until arrangements have been paid in full</p>
                </label>
            </div>
        </div>


    </div>
</div>