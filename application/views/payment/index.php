<div class="col-sm-7 col-md-7">
    <h1>Payment Options - Step 5</h1>
    <form action="" id="mainform" name="mainform" method="POST">
        <input type="hidden" name="pkgtype"             id="pkgtype"        value="<?php echo $user_session['pkgtype'];?>" />
        <input type="hidden" name="scriptaction"        id="scriptaction"   value="validate" />
        <input type="hidden" name="notvalidate"         id="notvalidate"    value="" />
        <input type="hidden" name="next_form"           id="next_form"      value="<?php echo $next_form?>" />
        <input type="hidden" name="session_index"       id="session_index"  value="<?php echo $session_index?>" />
        <input type="hidden" name="selected"            id="selected"       value="<?php echo $$session_index;?>" />

    <div class="ea-content">
        <div class="title-checkout">Checkout</div>
        <div class="text-plan-merch">
            <?php if($user_session['pkgtype'] == "atneed"){?>
            Please enter your credit card details below to complete the process.
            <?php } else {?>
            Please enter your arrangement information below to complete the process.
            <?php } ?>
            <br />
            <strong>All fields are required.</strong>
        </div>

        <?php if(isset($user_session['error_message']) and !empty($user_session['error_message'])){ ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error</strong> <?php echo $user_session['error_message'];?>
        </div>
        <?php } ?>

        <?php
        echo $arrangement_form;
        if($user_session['pkgtype'] == "atneed"){
            echo $option_form;
        }
        echo $billing_form;
        if($user_session['pkgtype'] == "atneed"){
            echo $credit_form;
        }
        ?>

        <?php if($user_session['pkgtype'] == "preneed"){ ?>
            <input type="hidden" name="paybycash" value="N/A">
        <?php } ?>

       <!--  <div class="eaFormBox">
            <?php //echo ($user_session['pkgtype'] == "atneed") ? 'Signature of Purchaser' : 'Signature of Arranger';?>:
            <div class="eaFormField">
                <span style="display:inline-block; padding-right:5px;">X</span>
                <input type="text" name="signature" id="signature"  class="input-signature atneed-required">
            </div>
        </div> -->

    </div>

    <div class="ea-btt-prev-next-area">
        <div class="row">
            <div class="col-sm-5 col-md-5">
                <a class="ea-btt-prev-next" href="<?php echo $prev_form;?>">Previous Step</a>
            </div>
            <div class="col-xs-offset-0 col-sm-5 col-sm-offset-2 col-md-5 col-md-offset-2">
                <a class="ea-btt-prev-next btnSubmit" href="<?php echo $next_form;?>">Next Step</a>
            </div>
        </div>
    </div>

    </form>
</div>
<?php echo $sidebar;?>
<script src="<?php echo assets_path('js/template/payment/payment.min.js?_=' . strtotime('now') );?>"></script>

