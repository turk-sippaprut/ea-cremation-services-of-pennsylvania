<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Serives_model extends My_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = "services";
        parent::setkeyCache($this->catchprefix.$this->table."_table");
    }

    /**
     * Get Data Service from type
     * @param  string $type For example [transport , handling ,  addition , merchandise]
     * @return [array]  data of service
     */
    public function getByType($type = 'transport'){
       $request['where']['type'] = $type;
       $request['where']['deleted'] = 0;
       $request['orderby'] = 'sequence asc';
       parent::setKeyCache($this->catchprefix.$this->table."_".$type);
       return parent::selectData($request);
    }

}

/* End of file Serives_model.php */
/* Location: ./application/models/Serives_model.php */