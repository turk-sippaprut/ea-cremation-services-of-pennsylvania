<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Type_model extends My_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = "types";
        $this->setkeyCache($this->catchprefix."types_table");
    }

}

/* End of file Type_model.php */
/* Location: ./application/models/Type_model.php */