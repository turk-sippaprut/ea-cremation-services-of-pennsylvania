<?php
class My_model extends CI_Model{

	private $client_id;

	public $catchprefix = '';
	protected $table = '';
	protected $cacheTarget = '';

    //Manage Cache
	protected $keyCache;
    protected $cacheType = "memcached";

    protected $_primary = "id";


	function __construct(){
		parent::__construct();
		$this->config->load('earrangement');
		$this->catchprefix = $this->config->config['client_id']."-ea-" . ENVIRONMENT . "-";
        //$this->load->driver('cache', array('adapter' => $this->cacheType ));
        $this->load->driver('cache');
	}

	/**
     * [getById one line]
     * @param  [int] $id [description]
     * @return [array]  data   [description]
     */
    public function getById($id){
        if( !$items = $this->get_catch() ){
             $this->db->where( $this->_primary , $id);
             $query = $this->db->get($this->table);
             $items = ($query->num_rows() > 0) ? $query->row_array() : array();

             if(!empty($this->keyCache)){
                $this->set_catch($items);
            }
        }
        return $items;
    }


    /**
     * [selectData get data ]
     * @param  arr = array(
     *      "select"  => "*" ,
     *      "join"    => array("table" => "" , "value" => "") ,
     *      "where"   => array("field" => value) ,
     *      "group_by" => string
     *      "orderby" => string  ,
     *      "limit"   => int,
     *      "start"   => int
     * )
     * @return data array
     */
    public function selectData($arr = array())
    {
        $items = array();
        if( !$items = $this->get_catch() ){
	        if(isset($arr['select'])){
	            $select = $arr['select'];
                if(is_array($select) and sizeof($select) > 0){
                    $select = implode("," , $select);
                }
                $this->db->select($select,false);
	        }
            if(isset($arr['join'])){
                 $this->db->join( $arr['join']['table'] , $arr['join']['value'] );
            }
	        if(isset($arr['where']) and is_array($arr['where'])){
	            foreach($arr['where'] as $field => $value){
	                if(!empty($value)){
	                    $this->db->where($field , $value );
	                }
	                else{
	                    $this->db->where($field , $value , FALSE );
	                }
	            }
	        }
	        if(isset($arr['group_by']) and !empty($arr['group_by'])){
	            $this->db->group_by($arr['group_by']);
	        }

	        if(isset($arr['orderby']) and !empty($arr['orderby']) ){
	            $this->db->order_by($arr['orderby'], '' , false);
	        }
	        if(isset($arr['limit']) or isset($arr['start']))
	        {
	            $this->db->limit( (int)$arr['limit'] , (int)$arr['start'] );
	        }
	        $query = $this->db->get($this->table);

	        $items = decode_rows($query->result_array());
            $dataSet = array();
	        foreach($items as $key => $item){
	        	if(isset($item['id'])){
	        		$dataSet[$item['id']] = $item;
	        	} else{ $dataSet[$key] = $item; }
            }
            $items = $dataSet;
	        if(!empty($this->keyCache)){
	        	$this->set_catch($dataSet);
	        }
	   }
        return $items;
    }



    /**
     * Count Data of table
     * @param  array  $where conditions
     * @return [int] Unit of table
     */
    public function countData($where = array()){
      $count = 0;
      $this->db->select("COUNT(*) as countData");
      foreach($where as $field => $value){
        if(!empty($value)){
            $this->db->where($field , $value );
        }
        else{
            $this->db->where($field , $value , FALSE );
        }
      }
      $query = $this->db->get($this->table);
      if($query->num_rows() > 0){
        $row = $query->row_array();
        return $row['countData'];
      }
      return $count;
    }



    /**
     * [insert description]
     * @param  [array] $data
     * @return [id pk]
     */
    public function insert($data){
        $data = clean_rows($data);
        $insert = $this->db->insert($this->table , $data);
        if(!$insert){
            throw new Exception("Error can not insert database");
        }
        return $this->db->insert_id();
    }


    /**
     * [insert_batch insert multiple  ]
     * @param  [array] $data  [
     *       array() ,
     *       array()
     * ]
     * @return [type] [description]
     */
    public function insert_batch($data = array()){
        $insert = false;
        if(sizeof($data) > 0 ){
            $insert = $this->db->insert_batch($this->table , $data);
        }
        if(!$insert){
            throw new Exception("Error can not insert database");
        }
        return true;
    }



    /**
     * [update ]
     * @param  [array]  $data    [field database => value for update]
     * @param  [array]  $where   [field database => value]
     * @param  boolean $escape [true/false ]
     * @return [boolean] true/false
     */
    public function update($data , $where , $escape = true ){

        //Escapes special characters in a string for use in an SQL statement
        if($escape === true){
            $this->db->where($where);
            $update = $this->db->update($this->table , $data);
        }
        //No escapes
        else{
            foreach($data as $field => $value){
                $this->db->set($field , $value , false);
            }

            $update = $this->db->where($where)
                                  ->update($this->table);
        }


        if(!$update){
            throw new Exception("Error can not update database");
        }
        return true;
    }

    /**
     *  [delete]
     * @param  array  $where [array("field" =>  value)]
     * @return [boolean][true/false]
     */
    public function delete($where = array()){
        return $this->db->where($where)->delete($this->table);
    }


    public function setTypeCache($type = 'memcached'){
        self::$cacheType = $type;
    }


    //Get Memcache
    public function setkeyCache($key = NULL){
        $this->keyCache = $key;
        return $this;
    }


    public function get_catch(){
        $cacheType = $this->cacheType;
        if ($this->cache->$cacheType->is_supported())
        {
            return $this->cache->{$this->cacheType}->get($this->keyCache);
        }

		return false;
	}

	//Save cache apc
	public function set_catch( $data, $expire = 3600){
        $cacheType = $this->cacheType;
        if ($this->cache->$cacheType->is_supported()){
            $this->cache->{$this->cacheType}->save( $this->keyCache , $data , $expire );
        }
	}

    // //Delete Apc cache
     public function clearALL(){
         $this->cache->{$this->cacheType}->clean();

     }

     public function deleteCacheBykey(){
        $this->cache->{$this->cacheType}->delete($this->keyCache);
        return $this;
     }

}
?>