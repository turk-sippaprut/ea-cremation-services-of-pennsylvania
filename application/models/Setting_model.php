<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends My_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = "ea_setting";
        $this->setkeyCache($this->catchprefix."ea_setting_table");
    }

    /**
     * [get_packages ]
     * @param  request = array(
     *      "select"  => "*" ,
     *      "where"   => array("field" => value) ,
     *      "group_by" => string
     *      "orderby" => string  ,
     *      "limit"   => int,
     *      "start"   => int
     * )
     * @return data array
    */
    public function get_settings($request = array()){

        $items = array();
        $result = $this->selectData($request);
        foreach($result as $setting){
            if($setting['var_type'] == 'ARRAY'){
                eval('$items[$setting["var_name"]] = '.$setting["var_value"].';');
            }
            else{
                $items[$setting['var_name']] = $setting['var_value'];
            }
        }
        return $items;
    }

}

/* End of file Setting_model.php */
/* Location: ./application/models/Setting_model.php */