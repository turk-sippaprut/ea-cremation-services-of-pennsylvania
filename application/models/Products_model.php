<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends My_Model {

    private $_cacheName;

    public function __construct()
    {
        parent::__construct();
        $this->table = "products";
        $this->_cacheName = $this->catchprefix."products_table";
        $this->setkeyCache($this->_cacheName);
    }

    public function getAll(){
        $this->setkeyCache($this->catchprefix."-all_items");
        $items = array();
        $result = $this->selectData();
        foreach($result as $item){
             $items[$item['pid']] = $item;
        }
        return $items;
    }

    public function getByCategory($type , $arr = array())
    {
        $items = array();

        //Set cache
        $cacheName = replaceSpace($this->_cacheName."-".$type);
        $this->setkeyCache($cacheName);

        //Set condition
        $request['where']['type'] = $type;
        $request['orderby'] = "price asc";
        $result = $this->selectData($request);
        foreach($result as $item){
             $items[$item['pid']] = $item;
        }
        return $items;
    }


    /**
     * For tabs products
     * @parm package_id int
     */
    public function get_merch_types( ){
        return array(
            'urns' => array(
                'title'    => 'Choose an urn' ,
                'type'     => 'Urns',
                'active'   => true ,
                'data'   => $this->getByCategory('Urns') ,
                'slider' => true ,
                'hasQuan' => false
            ) ,
            'container' => array(
                'title'    => 'CREMATION CONTAINERS' ,
                'type'     => 'Cremation Containers',
                'active'   => true ,
                'data'   => $this->getByCategory('Cremation Containers'),
                'slider' => false ,
                'hasQuan' => false
            ) ,
            'keepsakes' => array(
                'title'    => 'Keepsakes & Jewelery' ,
                'type'     => 'Keepsakes & Jewelery',
                'active'   => false ,
                'data'   => $this->getByCategory('Keepsakes & Jewelery') ,
                'slider' => true ,
                'hasQuan' => true
            ) ,
            'veteran' => array(
                'title'    => 'Veteran Flag Cases' ,
                'type'     => 'Veteran Flag Cases',
                'active'   => false ,
                'data'   => $this->getByCategory('Veteran Flag Cases') ,
                'slider' => false ,
                'hasQuan' => false
            )
        );
    }



}

/* End of file Product_model.php */
/* Location: ./application/models/Product_model.php */