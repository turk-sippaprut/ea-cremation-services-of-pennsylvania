<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends My_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = "order_archive";
        $this->setkeyCache($this->catchprefix."ea_archive_table");
    }

    /**
         * For check status payment
         * @param  [int] $order_id
         * @return [bolean] true/false
         */
        public function checkCompleted($order_id = NULL){
            if(!empty($order_id)){
                $order = $this->setkeyCache($this->catchprefix."order-".$order_id)->getById($order_id);
                if(sizeof($order) > 0 and strtolower($order['status']) == 'complete'){
                    return true;
                }
            }
            return false;
        }


        /**
         * For check status paper
         * @param  [int] $order_id
         * @return [bolean] true/false
         */
        public function checkCompletedPaper($order_id = NULL){
           if(!empty($order_id)){
                $data = $this->setkeyCache($this->catchprefix."order-".$order_id)->getById($order_id);
                if(sizeof($data) > 0 and strtolower($data['paperwork']) == "complete"  ){
                     return true;
                }
           }
           return false;
        }

        /**
         * For check status download paper
         * @param  [int] $order_id
         * @return [bolean] true/false
         */
        public function checkDownloadPaper($order_id = NULL){
            if(!empty($order_id)){
                $data = $this->setkeyCache()->getById($order_id);
                if(sizeof($data) > 0 and strtolower($data['paperwork']) == "download"  ){
                     return true;
                }
           }
           return false;
        }

        /**
         * For check status mail paper
         * @param  [int] $order_id
         * @return [bolean] true/false
         */
        public function checkMailPaper($order_id = NULL){
            if(!empty($order_id)){
                $data = $this->setkeyCache()->getById($order_id);
                if(sizeof($data) > 0 and strtolower($data['paperwork']) == "mail"  ){
                     return true;
                }
           }
           return false;
        }


        /**
         * Get new Uniq
         * @return new uniq
         */
        public function getNewUniq(){

            $request = array(
                "select"  =>  "id, uniq" ,
                "orderby" => "id desc" ,
                "start"   => 0 ,
                "limit"   => 1
            );

            $rows = $this->setkeyCache()->selectData($request);
            $uniq = NULL;
            if(sizeof($rows) > 0){
                list($id) = array_keys($rows);
                $uniq = $rows[$id]['uniq'];
            }
            return generateUniq($uniq);
        }


        public function getByHash($hash){
            $items = array();
            if(!empty($hash)){
                $request = array(
                    "where"   => array("hash" => $hash) ,
                    "orderby" => "id desc" ,
                    "start"   => 0 ,
                    "limit"   => 1
                );
                $rows = $this->setkeyCache($this->catchprefix."order-".$hash)->selectData($request);
                if(sizeof($rows) > 0){
                   foreach($rows as $key => $row){
                        $items = $row;
                   }
                }
            }
            return $items;
        }

        /**
         * [update ]
         * @param  [array]  $data    [field database => value for update]
         * @param  [array]  $where   [field database => value]
         * @param  boolean $escape [true/false ]
         * @return [boolean] true/false
         */
        public function update($data , $where , $escape = true ){
            $return = parent::update($data , $where , $escape);
            if(isset($where['id'])){
                $data = parent::getById($where['id']);
                $this->setkeyCache($this->catchprefix."order-".$data['id'])
                     ->deleteCacheBykey()
                     ->setkeyCache($this->catchprefix."order-".$data['hash'])
                     ->deleteCacheBykey();
            }
            return $return;
        }

}

/* End of file Order_model.php */
/* Location: ./application/models/Order_model.php */