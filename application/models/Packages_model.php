<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packages_model extends My_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = "packages";
        $this->setkeyCache($this->catchprefix."packages_table");
    }

    /**
     * [get_packages ]
     * @param  request = array(
     *      "select"  => "*" ,
     *      "where"   => array("field" => value) ,
     *      "group_by" => string
     *      "orderby" => string  ,
     *      "limit"   => int,
     *      "start"   => int
     * )
     * @return data array
     */
    public function getData($request = array())
    {
        $items = array();
        $request['where']['(deleted IS NULL or deleted = 0)'] = NULL;
        $request['orderby'] = 'price asc';
        $result = $this->selectData($request);
        foreach($result as $item){
            $item['includes'] = explode(',', $item['includes']);
            $items[$item['id']] = $item;
        }
        return $items;
    }

}

/* End of file Packages_model.php */
/* Location: ./application/models/Packages_model.php */