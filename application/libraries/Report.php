<?php

    /**
     * This class for set message and report in admin
     * Setting begin send email
     */

    class Report {


        private $message = "";
        private $adminMessage = array();



         /*--- Usage -------------------
                message('Title');                     # title row
                message('Label','Value');             # normal row
                message('[colspan]','.....');         # merge row (for too long value)
                message('[indent]','.....');          # indent row (description)
                message('[indentitalic]','.....');    # indent row (italic description)
                message();                            # blank row
        */
        public function setMessage($label="",$val="[default]"){

            if($label and $val!="[default]"){
                if(preg_match("/\[colspan\]/i",$label)){
                    $this->message.='
                    <tr align=left valign=top>
                        <td colspan=2 align=right>'.$val.'</td>
                    </tr>
                    ';
                }elseif(preg_match("/\[indent\]/i",$label)){
                    $this->message.='
                    <tr align=left valign=top>
                        <td colspan=2 class="indent">'.$val.'</td>
                    </tr>
                    ';
                }elseif(preg_match("/\[indent2\]/i",$label)){
                    $this->message.='
                    <tr align=left valign=top>
                        <td class=""><li style="list-style-position: outside; margin-left: 30px;">'.$val.'</li></td>
                        <td>&nbsp;</td>
                    </tr>
                    ';
                }elseif(preg_match("/\[indentitalic\]/i",$label)){
                    $this->message.='
                    <tr align=left valign=top>
                        <td colspan=2 class="indent"><i>'.$val.'</i></td>
                    </tr>
                    ';
                }else{
                    $this->message.='
                    <tr align=left valign=top>
                        <td>'.$label.'</td>
                        <td align=right>'.$val.'</td>
                    </tr>
                    ';
                }
            }
            elseif($label){
                $this->message.='
                <tr align=left>
                    <td colspan=2><b>'.$label.'</b></td>
                </tr>
                ';
            }elseif($val!="[default]"){
                $this->message.='
                <tr align=left valign=top>
                    <td>&nbsp;</td>
                    <td align=right>'.$val.'</td>
                </tr>
                ';
            }else{
                $this->message.='<tr><td>&nbsp;</td></tr>';
            }

            return $this;
        }

        public function getMessage(){
            $message = '<style>.indent{text-indent:15px;}</style>
                        <table width=500>'.$this->message.'</table>';
            return $message;
        }


        /*--- Usage -------------------
            setAdminMessage('Title');                       # title row
            setAdminMessage('Label','Value');               # normal row
            setAdminMessage('[colspan]','.....');           # merge row (for too long value)
            setAdminMessage('[indent]','.....');            # indent row (description)
            setAdminMessage('[indentitalic]','.....');      # indent row (italic description)
            setAdminMessage();                              # blank row
         *
        /*--- For sending more parameter -------------------------
            setAdminMessage('Title', 'Value', array(param...));         # send parameter as array(must have 'action' as one of members)

            # For action is "showdetail"
            'action' => 'showdetail',
            'detail' => $detail,

            # For action is "showlargeimage"
            'action' => 'showlargeimage',
            'url' => $large_image_url,
            'thumb' => $thumb_image_url,
        */
        public  function setAdminMessage($label="",$val="[default]", $param = array()){

            if($label and $val!="[default]"){
                if(preg_match("/\[colspan\]/i",$label)){
                    $result = array(
                        'type' => 'row-colspan',
                        'value' => $val,
                    );
                }elseif(preg_match("/\[indent\]/i",$label)){
                    $result = array(
                        'type' => 'row-indent',
                        'value' => $val,
                    );
                }elseif(preg_match("/\[indentitalic\]/i",$label)){
                    $result = array(
                        'type' => 'row-indent-italic',
                        'value' => $val,
                    );
                }else{
                    $result = array(
                        'type' => 'row-normal',
                        'label' => $label,
                        'value' => $val,
                    );
                }
            }elseif($label){
                $result = array(
                    'type' => 'row-title',
                    'value' => $label,
                );
            }elseif($val!="[default]"){
                $result = array(
                    'type' => 'row-value',
                    'value' => $val,
                );
            }else{
                $result = array(
                    'type' => 'row-empty',
                );
            }

            $result['param'] = $param;
            $this->adminMessage[] = $result;
            return $this;
            //$this->session->set_user('admintable=>[]', $row);
        }

        public function getAdminMessage(){
            return $this->adminMessage;
        }

        public function clearEmailMessage(){
            $this->message = "";
        }

        public function clearAdminMessage(){
            $this->adminMessage = array();
        }

        public function clearAllMessage(){
            $this->clearAdminMessage();
            $this->clearEmailMessage();
        }

    }

?>