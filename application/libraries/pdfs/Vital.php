<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once( APPPATH . 'core/PdfCore.php' );

class Vital extends PdfCore
{
	protected $ci;

	protected $file_name = "Vital_Stats.pdf";

    protected $pdfType = 'vital';
    

    public function __construct()
    {
        parent::__construct();

        $this->_ci->config->load('earrangement');
        $user_session = $this->_ci->session->get_user();

        $cfg = $this->_ci->config->config;
        $document = Vars::get( $cfg['documents'][$this->pdfType] );
        $this->resource = base_url($cfg['documents'][$this->pdfType]['source']);
        $this->destination = 'pdf/'. $cfg['client_id'].'/'.date("Y/m/d/". Vars::get($user_session['order_archive_id'])."/");
        $session_index = "formvital";

        $this->data = Vars::get( $user_session[$session_index] );

        $this->data['name_of_deceased'] = getArrayValue($this->data , 'deceased_first') ." ";
        $this->data['name_of_deceased'] .= getArrayValue($this->data , 'deceased_middle');
        $this->data['name_of_deceased'] .= " " . getArrayValue($this->data , 'deceased_last');

        //Date of birth
        $this->data['dob']     = getArrayValue( $user_session[$session_index] , 'mob').", ";
        $this->data['dob']    .= getArrayValue( $user_session[$session_index] , 'dob')." ";
        $this->data['dob']    .= getArrayValue( $user_session[$session_index] , 'yob');

        //Date of Death
        if ($user_session['pkgtype'] == 'atneed') {
            $this->data['dod']     = getArrayValue( $user_session[$session_index] , 'mod').", ";
            $this->data['dod']    .= getArrayValue( $user_session[$session_index] , 'dod')." ";
            $this->data['dod']    .= getArrayValue( $user_session[$session_index] , 'yod');
        }

        //Date of Marriage
        $this->data['dom']     = getArrayValue( $user_session[$session_index] , 'mom').", ";
        $this->data['dom']    .= getArrayValue( $user_session[$session_index] , 'dom')." ";
        $this->data['dom']    .= getArrayValue( $user_session[$session_index] , 'yom');

        $this->data['death_certificates_need'] = Vars::get($user_session['packageSelected']['transport'][2]['quan']);
        $this->data['service_type'] = Arr::firstKey(Vars::get($user_session['packageSelected']['package']) , []);


        //Survivor Information
        $this->data['informant_name'] = getArrayValue($this->data , 'informant_name');
        $this->data['informant_name'] .= getArrayValue($this->data , 'informant_lastname');

        
    }
}

/* End of file vital.php */
/* Location: ./application/libraries/pdfs/vital.php */
