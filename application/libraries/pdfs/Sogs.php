<?php
include_once( APPPATH . 'core/PdfCore.php' );

class Sogs extends PdfCore
{
	protected $_ci;

	protected $file_name = "sogs.pdf";

    protected $pdfType = 'sogs';

	public function __construct()
	{
		parent::__construct();
        $this->_ci =& get_instance();

        $this->_ci->config->load('earrangement');
        $user_session = $this->_ci->session->get_user();

        $cfg = $this->_ci->config->config;
        $document = Vars::get( $cfg['documents'][$this->pdfType] );
        $this->resource = base_url($cfg['documents'][$this->pdfType]['source']);
        $this->destination = 'pdf/'. $cfg['client_id'].'/'.date("Y/m/d/". Vars::get($user_session['order_archive_id'])."/");

        $cart = $user_session['cart'];
        $packageSelected = $user_session['packageSelected'];
        list($package_id) = array_keys( $packageSelected['package'] );

        $totalA = 0;
        $this->data['cremation_package_price'] = number_format( $user_session['summary']['each']['packages'] , 2);
        $this->data['refEmbalming'] = 'Included';
        $totalA += $user_session['summary']['each']['packages'];

        /*====================================
        =            2 Facilities            =
        ====================================*/
        if($package_id == 2){
            $this->data['our_location'] = 'Included';
        }
        if( isset($packageSelected['addition'][6]['summary']) ){
            $this->data['visitation_location'] = number_format( $packageSelected['addition'][6]['summary'] ,2);
            $totalA += $packageSelected['addition'][6]['summary'];
        }
        if( isset($packageSelected['addition'][10]['summary']) ){
            $this->data['graveside_service'] = number_format( $packageSelected['addition'][10]['summary'] ,2);
            $totalA += $packageSelected['addition'][10]['summary'];
        }
        /*=====  End of 2 Facilities  ======*/

        /*======================================
        =            Transportation            =
        ======================================*/
        $this->data['utilityFlower'] = 'Included';
        if( isset( $packageSelected['transport'][1]['summary'] ) ){
            $this->data['transfer_center'] = number_format( $packageSelected['transport'][1]['summary'] , 2);
            $totalA += $packageSelected['transport'][1]['summary'];
        }
        /*=====  End of Transportation  ======*/

        /*======================================
        =            Other Services            =
        ======================================*/
        if( isset( $packageSelected['addition'][8]['summary'] ) ){
            $this->data['weekend_fee'] = number_format( $packageSelected['addition'][8]['summary'] , 2);
            $totalA += $packageSelected['addition'][8]['summary'];
        }

        if( isset( $packageSelected['addition'][7] ) ){
            $this->data['witnessed'] = $packageSelected['addition'][7]['name'];
            $this->data['witnessed_price'] = number_format( $packageSelected['addition'][7]['summary'] , 2);
            $totalA += $packageSelected['addition'][7]['summary'];
        }

        /*=====  End of Other Services   ======*/
        $this->data['total_services'] = number_format( $totalA , 2);

        /*===================================
        =            Merchandise            =
        ===================================*/
        $totalB = 0;
        $productAlready = array();
        $containerKey = getFirstKey($cart['container']);
        $price = $cart['container'][ $containerKey ]['summary'];
        $totalB += $price;
        $this->data['casket_price'] = ($price > 0) ? number_format( $cart['container'][ $containerKey ]['summary'] , 2 ) : 'Included';
        $this->data['casket_name'] = $cart['container'][ $containerKey ]['name'];
        $productAlready[] = $containerKey;

        $index = 0;
        foreach($cart['urns'] as $key => $row){
            $this->data['urn_selection' . $index ] = $row['name'];
            $this->data['urn_selection_price' . $index] = ($row['summary'] > 0) ? number_format($row['summary'],2) : 'Included';
            $totalB += $row['summary'];
            $index++;
            $productAlready[] = $row['id'];
        }

        $index = 0;
        foreach($cart as $key => $category){
            foreach($category as $id => $row){
                if(in_array($id , $productAlready) and $key != 'merchandise') { continue; }
                if($id == 11 and $key == 'merchandise' ){ continue; } //Memorial Stationary Package
                $this->data['temporary_marker_'.$index] = $row['name'];
                // if($id == 13 and $key == 'merchandise' ){

                // }
                $this->data['temporary_marker_'.$index . '_price'] = number_format($row['summary'],2);
                $totalB += $row['summary'];
                $index++;
            }
        }

        $registerBook = 0;
        if(isset($cart['merchandise'][11])){
            $registerBook = $cart['merchandise'][11]['summary'];
            $this->data['register_book'] = $cart['merchandise'][11]['name'];
            $this->data['register_book_price'] = number_format( $registerBook , 2);
        } else{
            if($package_id == 2){
                $registerBook = 0;
                $this->data['register_book_price'] = 'Included';
            }
        }
        $totalB += $registerBook;

        //Memorial Folder
        $this->data['memFolder_price'] = ($package_id == 2) ? 'Included' : null;

        $this->data['total_merchandise'] = number_format( $totalB , 2);
        /*=====  End of Merchandise  ======*/

        /*==================================
        =            C. SPECIAL            =
        ==================================*/
        $totalC = 0;
        if(isset($packageSelected['handling'])){
            $remainKey = getFirstKey( $packageSelected['handling'] );
            if($remainKey != 15 and $remainKey != 16){
                $this->data['remains_to'] = $packageSelected['handling'][ $remainKey ]['name'];
                $this->data['remains_to_price'] = number_format( $packageSelected['handling'][ $remainKey ]['summary'] , 2);
                $totalC += $packageSelected['handling'][ $remainKey ]['summary'];
            }
        }

        if ( isset( $packageSelected['transport'][14] )){
            $this->data['remains_from'] = $cfg['deathOccurOption'][ $packageSelected['transport'][14]['choice'] ]['name'] ;
            $this->data['remains_from_price'] = number_format( $packageSelected['transport'][14]['summary'] , 2);
            $totalC += $packageSelected['transport'][ 14 ]['summary'];
        }
        $this->data['total_of_special_charges'] = number_format( $totalC , 2 );
        /*=====  End of C. SPECIAL  ======*/


        /*==============================
        =            D.Cash            =
        ==============================*/
        $totalD = 0;
        if( isset( $packageSelected['transport'][2] )){
            $this->data['quan_certificates'] = $packageSelected['transport'][2]['quan'];
            $this->data['certified_each']    = $packageSelected['transport'][2]['price'];
            $this->data['certified_price']   = number_format( $packageSelected['transport'][2]['summary'] , 2);
            $totalD += $packageSelected['transport'][2]['summary'];
        }
        if(isset($packageSelected['handling'])){
            $remainKey = getFirstKey( $packageSelected['handling'] );
            if($remainKey == 15 || $remainKey == 16){
                $this->data['scatteringLabel'] = $packageSelected['handling'][ $remainKey ]['name'];
                $this->data['scatteringPrice'] = number_format( $packageSelected['handling'][ $remainKey ]['summary'] , 2);
                $totalD += $packageSelected['handling'][ $remainKey ]['summary'];
            }
        }

        if ( isset( $packageSelected['transport'][17] )){
            $this->data['coroner_authorization_2'] = $packageSelected['transport'][17]['name'] ;
            $this->data['coroner_authorization_2_price'] = number_format( $packageSelected['transport'][17]['summary'] , 2);
            $totalD += $packageSelected['transport'][ 17 ]['summary'];
        }

        $this->data['total_disbursements'] = number_format( $totalD , 2);
        /*=====  End of D.Cash  ======*/

        /*===============================
        =            Summary            =
        ===============================*/
        $this->data['total_cremation_services'] = number_format( $totalA + $totalB + $totalC , 2 );
        $this->data['total_cash_advances'] = number_format( $totalD , 2);
        $this->data['complete_to_total'] = number_format( $user_session['summary']['total'] , 2);

        $this->data['balance_du'] = "0.00";
        if( Vars::get($user_session[ $session_index ]['paybycash']) == "check/cash"){
            $this->data['balance_du'] = number_format( $user_session['summary']['total'] , 2);
        }
        /*=====  End of Summary  ======*/

        /*======================================
        =            Signature Form            =
        ======================================*/
        $this->data['signed']    = $user_session['payment']['signature'];
        $this->data['address']   = $user_session['payment']['billing_address'];
        $this->data['city']      = $user_session['payment']['billing_city'];
        $this->data['state']     = $user_session['payment']['billing_state'];
        $this->data['zip']       = $user_session['payment']['billing_zipcode'];
        $this->data['telephone'] = $user_session['payment']['informant_phone'];
        /*=====  End of Signature Form  ======*/
	}

	

}

/* End of file Sogs.php */
/* Location: ./application/libraries/pdfs/Sogs.php */
