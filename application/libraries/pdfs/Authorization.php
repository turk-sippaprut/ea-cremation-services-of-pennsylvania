<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once( APPPATH . 'core/PdfCore.php' );

class Authorization extends PdfCore
{
	protected $_ci;

	protected $file_name = "Cremation_Authorization.pdf";

    protected $pdfType = 'auth';

	public function __construct()
	{
		parent::__construct();
        $this->_ci =& get_instance();

        $this->_ci->config->load('earrangement');
        $user_session = $this->_ci->session->get_user();

        $cfg = $this->_ci->config->config;
        $document = Vars::get( $cfg['documents'][$this->pdfType] );
        $this->resource = base_url($cfg['documents'][$this->pdfType]['source']);
        $this->destination = 'pdf/'. $cfg['client_id'].'/'.date("Y/m/d/". Vars::get($user_session['order_archive_id'])."/");
        $session_index = "formauth";

        $this->data = Vars::get( $user_session[$session_index] );
        $this->data['dod']     = getArrayValue($user_session[$session_index] , 'mod').", ";
        $this->data['dod']    .= getArrayValue($user_session[$session_index] , 'dod')." ";
        $this->data['dod']    .= getArrayValue($user_session[$session_index] , 'yod');

        if ( ! isset($this->data['deliver_address'])) {
        	$this->data['deliver_address'] = '';
        }
        $this->data['deliver_address'] .= " " .getArrayValue( $this->data , 'deliver_city');
        $this->data['deliver_address'] .= " " .getArrayValue( $this->data , 'deliver_phone');

        if ( ! isset($this->data['ship_address'])) {
        	$this->data['ship_address'] = '';
        }
        $this->data['ship_address']    .= " " .getArrayValue( $this->data , 'ship_city');
        $this->data['date_sign'] .= " " .getArrayValue( $this->data , 'time_sign');
	}

}

/* End of file Authorization.php */
/* Location: ./application/libraries/pdfs/Authorization.php */
