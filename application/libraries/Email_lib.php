<?php
    require_once('/home/funeralnet/public_html/mfscommon/php/mail.php');

    class Email_lib {

        private $subject = NULL;
        private $message = NULL;
        private $emailTo = array();
        private $emailFrom = NULL;


        public function setSubject($subject = NULL){
            $this->subject = $subject;
            return $this;
        }
        public function setMessage($message = NULL){
            $this->message = $message;
            return $this;
        }
        public function setemailTo($email = NULL){
            if(!empty($email)){
                $this->emailTo = $email;
            }
            return $this;
        }

        public function setemailFrom($email = NULL){
            if(!empty($email)){
                $this->emailFrom = $email;
            }
            return $this;
        }

        public function setTemplate($file = NULL , $data = array()){
            if(!empty($file)){
                $template = fopen($file, 'r') or die("Template : Couldn't open for reading \"$file\"\n");
                while (! feof($template)){
                    $this->message .= fgets($template, 1024);
                }
                fclose($template);
                $this->message = @preg_replace('/\$(\w+)\$/e', '$data["$1"]', $this->message );
            }
            return $this;
        }

        public function send(){
            foreach($this->emailTo as $key => $to){
                send_mail($this->emailFrom , $to, $this->subject, $this->message);
            }
        }

        public function debug(){

            return $this->message;

        }


    }

?>