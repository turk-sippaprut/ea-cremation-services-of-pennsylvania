<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once('/home/funeralnet/public_html/mfscommon/php/sms.php');

class Sms_lib
{
    protected $ci;
    private $_message = NULL;
    private $_sendTo = array();
    private $_sendFrom = NULL;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function setSendTo($number = array()){
        if(!empty($email)){
            $this->_sendTo = $number;
        }
        return $this;
    }

    public function setSendFrom($email = null){
        if(!empty($email)){
            $this->_sendFrom = $email;
        }
        return $this;
    }

    public function setMessage($message = null){
        $this->_message = $message;
        return $this;
    }

    public function send(){
        $config = $this->ci->config->config;
        $reponse = array();
        foreach($this->_sendTo as $to){
            if(preg_match('/2072006685/', $to)){
                $this->_message = $this->_message . ' - ['.$config['client_id'].']';
            }
            if(is_dev()){
                $to = '+12072006685';
            }
            $reponse[] = send_sms($this->_sendFrom , $to, $this->_message, 'eArrangement', $config['client_id']);
        }
        return $reponse;
    }


}

/* End of file Sms_lib.php */
/* Location: ./application/libraries/Sms_lib.php */
