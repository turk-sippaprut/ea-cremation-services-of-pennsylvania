<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CustomUpload
{

    public $width   = 400;
    public $height  = 400;

    private $_uploadPath;
    private $_filename;
    private $_allowType = 'gif|jpg|png|jpeg';
    private $_allowSize = '2000048';

    private $_CI;

    public function __construct(){
        $this->_CI =  & get_instance();
    }

    public function setPathUpload($path){
        $this->_uploadPath = $path;
        return $this;
    }

    public function setAllowType($allow){
        $this->_allowType = $allow;
        return $this;
    }

    public function setAllowSize($size){
        $this->_allowSize = $size;
        return $this;
    }

    public function setFileName($filename){
        $this->_filename = $filename;
        return $this;
    }

    public function setSize( $w = 150 , $h = 150 )
    {
        $this->width = $w;
        $this->height = $h;
        return $this;
    }

    public function run(){
        $config['upload_path']   = $this->_uploadPath;
        $config['allowed_types'] = $this->_allowType;
        $config['max_size']      = $this->_allowSize;

        $this->_CI->load->library('upload', $config);
        if(!$this->_CI->upload->do_upload($this->_filename)){
            throw new Exception($this->_CI->upload->display_errors() . $this->_uploadPath);
        }
        return $this->_CI->upload->data();
    }

    public function getExtFile($filename)
    {
        return strtolower(end(explode('.',$filename)));
    }

    /**
     * [resize resize image, sets the quality of the image is 60% ]
     * @param  $data = array( 'source_image' => Path image , 'filename' => New name file  )
     * @return [array]  => fileName , fullPath
     */
    public function resize($data){

        $this->_CI->load->library('image_lib');

        $config['image_library'] = 'gd2';
        $config['source_image']  = $data['source_image'];
        $config['width']         = $this->width;
        $config['height']        = $this->height;
        $config['quality']       = 50; //60%  Sets the quality of the image. The higher the quality the larger the file size.

        $newFile = $data['filename'];
        $config['new_image'] = $this->_uploadPath.$newFile;

        $this->_CI->image_lib->clear();
        $this->_CI->image_lib->initialize($config);
        if ( ! $this->_CI->image_lib->resize() )
        {
            throw new Exception($this->_CI->image_lib->display_errors());
        }
        return $newFile;
    }

}

/* End of file CustomUpload.php */
/* Location: ./application/libraries/CustomUpload.php */
