<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogsFile
{
    protected $ci;
    /**
     * [$_data description]
     * @var array
     * session => array() ,
     * path => string ,
     * filename => string
     */
    private $_data = array();

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function setData($key , $value){
        $this->_data[ $key ] = $value;
        return $this;
    }

    public function getdata( $key ){
        return $this->_data[ $key ];
    }

    public function __isset($name) {
        return isset($this->_data[$name]);
    }

    public function saveTransaction(  ){

        if(!$this->_hasPath() or !$this->_hasFileName()){
            return false;
        }

        createPath($this->_data['path']);
        if(!file_exists($this->_data['path'].$this->_data['fileName'])){
            $fp = fopen($this->_data['path'].$this->_data['fileName'] , 'wb');
            fwrite($fp,'');
            fclose($fp);
        }

        if($logf = fopen($this->_data['path'].$this->_data['fileName'] , 'a')){ # a/w
            $orderArchive = isset($this->_data['session']['order_archive_id']) ? $this->_data['session']['order_archive_id'] : null;
            $uniqid = isset($this->_data['session']['uniqid']) ? $this->_data['session']['uniqid'] : null;
            $text  = "=== ";
            $text .= date('Y-m-d H:i:s');
            $text .= " | ";
            $text .= "Order Archive ID : ". $orderArchive;
            $text .= " | ";
            $text .= "Order ID : ". $uniqid;
            $text .= " =====X=X=X=X=X=X=X=X=============================\n";
            $text .= print_r( $this->_data['session'] ,true);
            $text .= "\n\n\n";

            flock($logf, LOCK_EX);
            fwrite($logf, $text);
            flock($logf, LOCK_UN);  # Release the write lock.
            fclose($logf);
        } else{
            die("Cannot open log file.");
        }
    }

    public function saveStep(){

        if(!$this->_hasPath() or !$this->_hasFileName()){
            return false;
        }

        createPath($this->_data['path']);
        $session = $this->_data['session'];

        $unsetSession = array(
            'admintable' ,
            'arrangementtable' ,
            'decedenttable' ,
            'contacttable'
        );
        foreach($unsetSession as $key => $value){
            if(isset($session[ $value ])){
                unset($session[$value]);
            }
        }

        $arr = array(
            'get' => $this->ci->input->get() ,
            'post' => $this->ci->input->post() ,
            'session' => $session
        );
        if(!file_exists($this->_data['path'].$this->_data['fileName'])){
            $fp = fopen($this->_data['path'].$this->_data['fileName'] , 'wb');
            fwrite($fp,'');
            fclose($fp);
        }
        if($logf = fopen($this->_data['path'].$this->_data['fileName'], 'a')){
            $text  = "=== ";
            $text .= date('r');
            $text .= " =====X=X=X=X=X=X=X=X=============================\n";
            $text .= print_r($arr,true);
            $text .= "\n\n\n";
            flock($logf, LOCK_EX);
            fwrite($logf, $text);
            flock($logf, LOCK_UN);  # Release the write lock.
            fclose($logf);
        }
    }

    private function _hasPath(){
        if(empty($this->_data['path'])){
            return false;
        }
        return true;
    }

    private function _hasFileName(){
        if(empty($this->_data['fileName'])){
            return false;
        }
        return true;
    }



}

/* End of file LogsFile.php */
/* Location: ./application/libraries/LogsFile.php */
