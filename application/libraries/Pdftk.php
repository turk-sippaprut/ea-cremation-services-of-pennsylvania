<?php
class Pdftk {

    const APIURL = 'http://pdf.funeralnet.com/pdftk/?';
    private $_settings = [];
    private $_data = [];


    /**
     * [__construct description]
     * @param array $config [ client_id , file , destination , file_name ]
    */
    public function __construct($config = array()){
        $this->initialize($config);
    }

    public function initialize($config = array()){
        $this->_settings = $config;
      //  $this->_settings['token'] = isset($this->_settings['client_id']) ? md5($this->_settings['client_id']."fill_pdf") : md5('fill_pdf');
        return $this;
    }

    public function setFile($file = null){
        $this->_settings['file'] = $file;
        return $this;
    }

    public function setDestination($des = null){
        $this->_settings['destination'] = $des;
        return $this;
    }

    public function setName($name = null){
        $this->settings['file_name'] = $name;
        return $this;
    }

    public function setData($data = array()){
        $this->_data = $data;
        return $this;
    }

    public function fill(){
        $this->setAction('fill_pdf');
        $this->generateToken();
        return $this->_process();
    }

    public function setAction($action = 'fill_pdf')
    {
        $this->_settings['action'] = $action;
        return $this;
    }

    public function getAction()
    {
        return isset($this->_settings['action']) ? $this->_settings['action'] : 'fill_pdf';
    }

    public function generateToken()
    {
        $action = $this->getAction();
        $this->_settings['token'] = isset($this->_settings['client_id']) ? md5($this->_settings['client_id'].$action) : md5($action);
        return $this;
    }

    /**
     * Get Index of PDF
     * @param  boolean $pattern [if false = show array index of PDF then true show string pattern array]
     * @return [type]           [description]
     */
    public function getIndex($pattern = false)
    {
        $this->setAction('get_index');
        $this->generateToken();

        $reponse = $this->_process();
        if(!$pattern){
            return $reponse;
        } else{
            $report = '// Array Variable ---------------------------------------------------------------------------<br><br>';
            $report .= '$data = array();<br>';
            foreach($reponse['index'] as $field_index => $field){
                $key = $field[2];
                $key = str_replace('FieldNameAlt','',$key);
                $key = str_replace('FieldFlags','',$key);
                $key = str_replace(' ','',trim($key));
                $report .= '$data["'.$key.'"] = "'.$field_index.'"; //'.$field_index.'<br>';
            }
            return $report;
        }
    }

    private function _process(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::APIURL . http_build_query($this->_settings));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->_data)  );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close ($ch);
        return json_decode($output, true);
    }


}