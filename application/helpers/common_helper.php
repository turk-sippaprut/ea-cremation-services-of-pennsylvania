<?php

/**
 * Stripslashes and decode html
 * @param  string  $string     [description]
 * @param  boolean $decodeHtml [true = html decode / false = stripslashes only]
 * @return [string]
 */
function decode_data($string = "" , $decodeHtml = false)
{
    if(!is_numeric($string) and !is_array($string)){
        $string = stripslashes($string);
        //$string = utf8_decode($string);
        if($decodeHtml == true){
            $string = html_entity_decode($string);
        }
    }
    return $string;
}

function decode_rows($rows = array())
{
    foreach($rows as $key => $data){
        foreach ($data as $field => $value) {
            $rows[$key][$field] = decode_data($value);
        }
    }
    return $rows;
}

function getArrayValue($data , $index){
    return $result = (isset($data[$index])) ? $data[$index] : NULL;
}



/**
 * [clean_input block html tag]
 * @param  [string] $string
 * @return [string]
 */
function clean_input($string = "")
{
    if(!is_numeric($string)){
        $string = trim($string);
        $string = htmlentities($string);
        $string = addslashes($string);
    }
    return $string;
}
/**
 * [clean_rows encode htmlentities before add to database]
 * @param  array  $rows [POST,GET]
 * @return [array] encode htmlentities
 */
function clean_rows($rows = array())
{
    foreach($rows as $key => $value){
        $rows[$key] = clean_input($value);
    }
    return $rows;
}

function clean_card($cardnumber) {
    $cardnumber = str_replace(" " , "" , $cardnumber);
    return str_replace("-" , "" , $cardnumber);
}

/**
 * For check value exits if do not exits will return null
 * @param  [string]  $string
 * @return string data/null
 */
function isNullValue($string = NULL){
    return filter_var($string , FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
}



/**
 * [substr_utf8 sub String and replace]
 * @param  [string] $str    text
 * @param  [int] $start_p   Start substr
 * @param  [int] $len_p     End substr
 * @param  string $show     Display for replace default = "..."
 * @return [string]
 */
function substr_utf8( $str, $start_p , $len_p , $show = "...") {
   $str_post = "";
   if(strlen(($str)) > $len_p)
   {
     $str_post = $show;
   }
   return preg_replace( '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$start_p.'}'.
    '((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len_p.'}).*#s',
    '$1' , $str ) . $str_post;
}

/**
 * [ReplaceSpace ]
 * @param  [string] $string
 */
function replaceSpace($string)
{
    $string = clean_input($string);
    $string = strtolower(trim($string));
    $string = preg_replace('/\s+/', ' ',$string);
    $string = preg_replace('/([^a-zA-Z0-9[:space:]-_])/u', '', $string);
    $string = str_replace(" ", "-", $string);
    return $string = preg_replace('/-+/', '-', $string); //replace multi text
}

/**
 * used this function for making a variable javascript compatible
 */
function jsSpecialchars( $string = '') {
  //  $string = decode_data($string);
    $string = htmlentities($string);
    //$string = strtr($string, "\"", "'");
    return $string;
}

/**
 * Get first key from list array
 */
function getFirstKey( $arr = array()){
    if( !is_array( $arr ) or sizeof($arr) == 0  ){
        return null;
    }
    list($index) = array_keys( $arr );
    return $index;
}

/**
 * Check have Decimal
 */
function have_decimal ($value){
   return ((float) $value !== floor($value));
}

function generateUniq($lastUniq = NULL){
    $digit = 6; # Number of digit for order id (Include year number for 2 first digit)
    $space="";
    for($i=1;$i<=$digit;$i++){
        $space.="0";
    }

    $uniqid = date("y").substr($space."1",(($digit-2)*-1));
    if($lastUniq){
        $uniqid = substr($lastUniq,(($digit-2)*-1))+1;
        $uniqid = date("y").substr($space.$uniqid,(($digit-2)*-1));
    }
    return $uniqid;
}

function get_hash_code(){
    $data = crypt(rand(100000,999999)).
            crypt(rand(100000,999999)).
            crypt(rand(100000,999999)).
            crypt(rand(100000,999999));
    return base64_encode(crypt($data));
}

function senser( $string  )
{
    return substr_replace( $string ,"****", -4 );
}

function createPath($path){
    if (is_dir($path)) return true;
    $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
    $return = createPath($prev_path);
    return ($return && is_writable($prev_path)) ? mkdir($path , 0775) : false;
}

function rc4($key, $str) {
    $s = array();
    for ($i = 0; $i < 256; $i++) {
        $s[$i] = $i;
    }
    $j = 0;
    for ($i = 0; $i < 256; $i++) {
        $j = ($j + $s[$i] + ord($key[$i % strlen($key)])) % 256;
        $x = $s[$i];
        $s[$i] = $s[$j];
        $s[$j] = $x;
    }
    $i = 0;
    $j = 0;
    $res = '';
    for ($y = 0; $y < strlen($str); $y++) {
        $i = ($i + 1) % 256;
        $j = ($j + $s[$i]) % 256;
        $x = $s[$i];
        $s[$i] = $s[$j];
        $s[$j] = $x;
        $res .= $str[$y] ^ chr($s[($s[$i] + $s[$j]) % 256]);
    }
    return $res;
}


?>