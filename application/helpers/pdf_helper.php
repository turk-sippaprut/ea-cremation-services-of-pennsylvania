<?php
function setMultiLine($data , $keyname){
    $arr = explode("<br />" , nl2br($data[$keyname]));
    foreach($arr as $key => $value){
        $data[$keyname.$key] = trim($value);
    }
    return $data;
}

function spiltPhone($data , $keyname){
    $phoneArr = explode(' ' , $data[$keyname] );
    foreach($phoneArr as $key => $row){
        $phoneArr[$key] = str_replace("(" , '' , $phoneArr[$key]);
        $phoneArr[$key] = str_replace(")" , '' , $phoneArr[$key]);
        $data[$keyname.$key] = $phoneArr[$key];
    }
    return $data;
}

function splitName($name) {
    $parts = array();
    while ( strlen( trim($name)) > 0 ) {
        $name = trim($name);
        $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $parts[] = $string;
        $name = trim( preg_replace('#'.$string.'#', '', $name ) );
    }
    if (empty($parts)) {
        return false;
    }
    $parts = array_reverse($parts);
    $name = array();
    $name['first_name'] = $parts[0];
    $name['middle_name'] = (isset($parts[2])) ? $parts[1] : '';
    $name['last_name'] = (isset($parts[2])) ? $parts[2] : ( isset($parts[1]) ? $parts[1] : '');
    return $name;
}