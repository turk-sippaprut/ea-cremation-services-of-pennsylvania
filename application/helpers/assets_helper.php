<?php

function assets_path($string){
    $ci = get_instance();
    $config = $ci->config->config;
    return $config['root'] ."assets/" . $string;
}

function pathImage($string){
    $ci = get_instance();
    $config = $ci->config->config;
    return "https://fnetclients.s3.amazonaws.com/mastercart/clients/".$config['client_id']."/pictures/" . $string;
}


?>