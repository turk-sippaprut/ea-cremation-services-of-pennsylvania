<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchandise extends MY_Controller {

    private $_targetMethod;
    private $_vars = array();

    public function __construct()
    {
        parent::__construct();
        $method = parent::uriDirection();
        $this->_targetMethod = ( $method == strtolower(__CLASS__) ) ? null : $method;
        $this->data['sidebarActive'] = 'merchandise';
    }

    public function index(){
        self::_init();
        if(empty($this->_targetMethod)){
            self::_render();
        }
        else{
            $method = strtolower($this->_targetMethod);
            if(method_exists( $this , $method )){
                $reflection = new ReflectionMethod(__CLASS__, $method);
                if ($reflection->isPublic()) {
                    $this->$method();
                }
            }
            else{
               show_404();
            }
        }
    }

    private function _init(){
        self::_validate();
        $user_session = $this->session->get_user();
        $this->_vars = array(
            'stepkey'       => 'merch',
            'prev_form'     =>  $this->session->get_user("pkgtype") ,
            'next_form'     => 'vital',
            'page_title'    =>  $this->cfg['steps'][ $this->data['sidebarActive'] ]['name'] ,
            'session_index' => "cart"
        );
        $this->load->vars($this->_vars);
    }

    private function _render(){
        $this->data['merchant'] = $this->mproduct->get_merch_types();
        if($this->input->post('scriptaction') == "validate"){
            self::_action();
        }

         # Load the step view.
        extract($this->_vars);
        parent::load_selected($this->_vars);
        $user_session = $this->session->get_user();

        list( $package_id ) = array_keys( $user_session['packageSelected']['package'] );
        $this->data['package_id'] = $package_id;
        $this->data['additionalMerch'] = $this->mserives->getByType('merchandise');
        foreach($this->data['additionalMerch'] as $key => $row){
            //Memorial Stationery Package is not appear if user select Memorial Plan on Step1
            if($package_id == 2 and $row['id'] == 11){
                unset( $this->data['additionalMerch'][$key]);
            }
        }

        //Get Session Data
        if(isset($user_session[$session_index])){
            foreach($user_session[$session_index] as $key => $row){
                $index = $key;
                $this->data[$index] = isset($user_session[$session_index][$key]) ? $user_session[$session_index][$key] : array();
            }
        }
        # Load the step view.
        $this->data[$session_index] = isset($user_session[$session_index]) ? jsSpecialchars(json_encode($user_session[$session_index])) : NULL;
        //Set info merchant
        foreach($this->data['merchant'] as $key => $row){
            $this->data['merchant'][ $key ]["pageinfo"] = $this->load->view(  strtolower(__CLASS__) . "/" .$key . "_info" , $this->data, true);
        }


        parent::load_step( strtolower(__CLASS__) .'/index', $page_title);
    }

    private function _action(){
        //Save information
        parent::save_selected($this->_vars);

        //Calculate price of products
        parent::_calculate_amount_due();
        parent::_notify_message_preparation();
        parent::go_to($this->_vars['next_form']);
    }


    private function _validate(){
        //if pay online done will can not change options
        if(parent::checkPaymentAccept()){
            show_404();
        }
        parent::check_paperwork();
        parent::checkDirectForm();

        $user_session = $this->session->get_user();
        if(!isset($user_session['packageSelected'])){
            show_404();
        }
    }

}

/* End of file Merchandise.php */
/* Location: ./application/controllers/Merchandise.php */