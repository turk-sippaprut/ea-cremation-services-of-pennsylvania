<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authorization extends MY_Controller {

    private $_targetMethod;
    private $_vars = array();
    private $_hash = NULL;

    public function __construct()
    {
        parent::__construct();
        $method = parent::uriDirection();
        $this->data['sidebarActive'] = 'paperwork';
        $this->_hash = ($this->input->get('hash')) ? $this->input->get('hash') : NULL;
    }

    public function index()
    {
        self::_init();
        self::_render();
    }

     private function _init(){
        //if hash not empty Session will continue
        parent::getDataByHash($this->_hash);
        self::_validate();

        $this->_vars = array(
            'stepkey'         => 'formAuth',
            'hash'            => $this->_hash ,
            'prev_form'       => 'vital',
            'next_form'       => 'overview',
            'page_title'      => 'Cremation Authorization',
            'session_index'   => parent::authprefix ,
            'is_paperwork'    => true ,
            'is_print'        => false
        );
        $this->load->vars($this->_vars);
    }

    private function _validate(){
        parent::checkDirectForm();
        parent::check_paperwork();
        $user_session = $this->session->get_user();
        if(!isset($user_session[parent::vitalprefix])){
            show_404();
        }
    }

    private function _render(){
        extract($this->_vars);
        $user_session = $this->session->get_user();
        if($this->input->post('scriptaction') == "validate"){
            self::_action();
        }
        $this->data[$session_index] = isset($user_session[$session_index]) ?  jsSpecialchars(json_encode($user_session[$session_index])) : NULL;
        $this->data['user_session'] = $user_session;
        $this->data['auth'] = isset($user_session[$session_index]) ? $user_session[$session_index] : array();
        $this->data['vital'] = $user_session[parent::vitalprefix];

        $casketId = getFirstKey($user_session['cart']['container']);
        $this->data['container'] = decode_data( $user_session['cart']['container'][ $casketId ]['name'] );
        $this->data['viewAuthagent'] = $this->load->view( strtolower(__CLASS__) . "/auth_agent", $this->data , true);

        $this->data['urns'] = null;
        $urnsArr = array();
        foreach($user_session['cart']['urns'] as  $key => $row){
            $urnsArr[] = $row['name'];
        }
        $this->data['urns'] = implode( "," , $urnsArr );

        parent::load_step( strtolower(__CLASS__) .'/index', $page_title);
    }

    private function _action(){
        extract($this->_vars);
        $data = $this->input->post();
        $user_session = $this->session->get_user();
        $user_session = $this->session->set_user($session_index , $data);
        parent::go_to($next_form);
    }
}

/* End of file Authorization.php */
/* Location: ./application/controllers/Authorization.php */