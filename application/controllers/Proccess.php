<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proccess extends MY_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $pageUrl =  strtolower(basename($this->uri->uri_string()));
        if(empty($pageUrl)){
            show_404();
        }
        else{
            $method = $pageUrl;
            if(method_exists( $this , $method )){
                $reflection = new ReflectionMethod(__CLASS__, $method);
                if ($reflection->isPublic()) {
                    $this->$method();
                }
            }
            else{
               show_404();
            }
        }
    }

    public function sendtoFamily(){
        try {

            $this->form_validation->set_rules('family_email', 'Family Email', 'required|valid_email');

            if($this->form_validation->run() == FALSE){
                throw new Exception(validation_errors());
            }

            $user_session = $this->session->get_user();

            $subjectText = "Advanced Planning Service Agreement";
            if($user_session['pkgtype'] == "atneed"){
                $subjectText  = "At-need Service Agreement";
            }

            $user_session['informant_name'] = $user_session[parent::paymentprefix]['informant_name'];
            $user_session['person'] = $user_session[parent::paymentprefix]['person'];

            //Send Email
            $this->email_lib
                    ->setemailTo( array( $this->input->post('family_email') ) )
                    ->setemailFrom( $user_session['email_form'] )
                    ->setSubject( $subjectText )
                    ->setTemplate( SEVERPATH . assets_path('templates/family.tmpl.html') , $user_session)
                    ->send();

            echo json_encode(array(
                'result' => 'success',
                'pnotify' => array(
                    'title' => 'Sending Success',
                    'type' => 'success',
                    'text' => 'The arrangement selections have been sent to "' . $this->input->post('family_email') . '".',
                    'width' => '600px',
                ),
            ));
            exit();

        }
        catch ( Exception $e) {
            echo json_encode(array(
                'result' => 'error',
                'pnotify' => array(
                    'title' => 'Sending Fail',
                    'type' => 'error',
                    'text' => $e->getMessage(),
                    'width' => '600px',
                ),
            ));
            exit();
        }
    }

}

/* End of file Proccess.php */
/* Location: ./application/controllers/Proccess.php */