<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Overview extends MY_Controller {

    private $_targetMethod;
    private $_vars = array();

    public function __construct()
    {
        parent::__construct();
        $method = parent::uriDirection();
        $this->_targetMethod = ( $method == strtolower(__CLASS__) ) ? null : $method;
        $this->data['sidebarActive'] = 'overview';
    }

    public function index(){
        self::_init();
        if(empty($this->_targetMethod)){
            self::_render();
        }
        else{
            $method = strtolower($this->_targetMethod);
            if(method_exists( $this , $method )){
                $reflection = new ReflectionMethod(__CLASS__, $method);
                if ($reflection->isPublic()) {
                    $this->$method();
                }
            }
            else{
               show_404();
            }
        }
    }

    private function _init(){
        self::_validate();
        $user_session = $this->session->get_user();
        $this->_vars = array(
            'stepkey'       => 'overview',
            'prev_form'     => 'authorization' ,
            'next_form'     => 'payment',
            'page_title'    =>  $this->cfg['steps'][ $this->data['sidebarActive'] ]['name'] ,
        );
        $this->load->vars($this->_vars);
    }

    private function _render(){
        if($this->input->post('scriptaction') == "validate"){
            self::_action();
        }
         # Load the step view.
        extract($this->_vars);
        $this->data['packages']  = $this->mpackage->getData();
        $this->data['products']  = $this->mproduct->getAll();
        $this->data['transport'] = $this->mserives->getByType('transport');
        $this->data['handling']  = $this->mserives->getByType('handling');
        $this->data['addition']  = $this->mserives->getByType('addition');
        $user_session = $this->session->get_user();
        parent::load_step( strtolower(__CLASS__) .'/index', $page_title);
    }


    private function _action(){
        parent::go_to($this->_vars['next_form']);
    }


    private function _validate(){
        //if pay online done will can not change options
        if(parent::checkPaymentAccept()){
            show_404();
        }
        parent::check_paperwork();
        parent::checkDirectForm();

        $user_session = $this->session->get_user();
        if(!isset($user_session['packageSelected']) and !isset($user_session['cart']) ){
            show_404();
        }
    }


}

/* End of file Overview.php */
/* Location: ./application/controllers/Overview.php */