<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {

    private $_targetMethod;
    private $_vars = array();
    private $_logPayment = array();

    public function __construct()
    {

        parent::__construct();
        $method = parent::uriDirection();
        $this->_targetMethod = ( $method == strtolower(__CLASS__) ) ? null : $method;
        $this->data['sidebarActive'] = 'payment';
    }

    public function index(){
        self::_init();
        if(empty($this->_targetMethod)){
            self::_render();
        }
        else{
            $method = strtolower($this->_targetMethod);
            if(method_exists( $this , $method )){
                $reflection = new ReflectionMethod(__CLASS__, $method);
                if ($reflection->isPublic()) {
                    $this->$method();
                }
            }
            else{
               show_404();
            }
        }
    }

    public function error() {

        $user_session = $this->session->get_user();

        if ( ! isset($user_session['ccresult']['result']) ) {
            show_404();
        }

        if ( strtolower(Vars::get($user_session['ccresult']['result'])) !== 'accepted' ) {
            $this->session->set_user('error_message' , $user_session['ccresult']['result'] );
        }

        self::_render();
    }

    private function _init(){
        self::_validate();
        $user_session = $this->session->get_user();
        $this->_vars = array(
            'stepkey'       => 'payment',
            'prev_form'     => 'overview' ,
            'next_form'     => 'thankyou',
            'page_title'    =>  $this->cfg['steps'][ $this->data['sidebarActive'] ]['name'] ,
            'session_index' => "payment"
        );

        $this->data['vitalprefix'] = parent::vitalprefix;
        $this->data['authprefix'] = parent::authprefix;
        $this->data['paymentprefix'] = parent::paymentprefix;
        $this->data['user_session'] = $this->session->get_user();
        $this->data['cfg'] = $this->cfg;
        $this->data['settings'] = $this->settings;


        $this->load->vars($this->_vars);
    }

    private function _validate(){
        //if pay online done will can not change options
        if(parent::checkPaymentAccept()){
            show_404();
        }
        parent::check_paperwork();
        parent::checkDirectForm();

        $user_session = $this->session->get_user();

        if( !isset($user_session['packageSelected']) and !isset($user_session['cart']) ){
            show_404();
        }

        if ( ! isset($user_session[parent::vitalprefix]) || ! isset($user_session[parent::authprefix]) ) {
            show_error('Please fill paperwork before submiting payment!');
        }

    }

    private function _render(){

        extract($this->_vars);
        if($this->input->post('scriptaction') == "validate"){
            self::_action();
        }
        parent::load_selected($this->_vars);
        $user_session = $this->session->get_user();

        $this->data[$session_index] = isset($user_session[$session_index]) ?  jsSpecialchars(json_encode($user_session[$session_index])) : NULL;
        $this->data['vital'] = Vars::get($user_session[parent::vitalprefix] , []);
        $this->data['auth'] = Vars::get($user_session[parent::authprefix] , []);

        $this->data['arrangement_form'] = $this->load->view(  strtolower(__CLASS__) .'/arrangement_form' , $this->data , true);
        $this->data['billing_form'] = $this->load->view(  strtolower(__CLASS__) .'/billing_form' , $this->data , true);

        $this->data['option_form'] = $this->load->view(  strtolower(__CLASS__) .'/option_form' , $this->data , true);
        $this->data['credit_form'] = $this->load->view(  strtolower(__CLASS__) .'/credit_form' , $this->data , true);
        parent::load_step( strtolower(__CLASS__) .'/index', $page_title);
    }

    private function _action(){
        $data = $this->input->post();
        if(isset($data['card_number'])){
            unset($data['card_number']);
        }
        
        extract($this->_vars);
        $user_session = $this->session->set_user($session_index , $data);

        parent::createNewArchive();
        $user_session = $this->session->get_user();

        //Get package
        $package_id = getFirstKey($user_session['packageSelected']['package']);
        $package_name = $user_session['packageSelected']['package'][$package_id]['name'];

         //Save Log file
        $this->_logPayment = array(
            'order_archive_id'  =>  $this->archive_id ,
            'uniqid'            =>  $user_session['uniqid'] ,
            'pkgtype'           =>  $user_session['pkgtype'] ,
            'package_name'      =>  $package_name,
            'informant_name'    =>  $user_session[parent::paymentprefix]['informant_name'],
            'informant_phone'   =>  $user_session[parent::paymentprefix]['informant_phone'],
            'informant_email'   =>  $user_session[parent::paymentprefix]['informant_email'],
            'payment_method'    =>  "",
            'summary'           =>  $user_session['summary'],
            'browser'           =>  array(
                'HTTP_USER_AGENT' => $_SERVER['HTTP_USER_AGENT'],
                'REMOTE_ADDR'     => $_SERVER['REMOTE_ADDR'],
                'SERVER_PORT'     => $_SERVER['SERVER_PORT'],
                'REQUEST_TIME'    => $_SERVER['REQUEST_TIME']
            )
        );
        $amount = $user_session['summary']['total'];
        if(is_debugger($user_session[$session_index]['informant_email'])){
            $amount = rand(100,199)/100;
        }
        $this->_logPayment['amount'] = $amount;
        $this->_logPayment['fullAmount'] = $user_session['summary']['total'];

        if($user_session['pkgtype'] == 'atneed'){
            if($data['paybycash'] != "credit card"){
                $ccapproved = self::_cash($amount);
            } else{
                $ccapproved = self::_payByCreditCard($amount);
            }

            $this->mylogs
                ->setData('path' , SEVERPATH . assets_path('logs/transaction/' .date("Y/m/d/")) )
                ->setData('fileName' , 'transaction.log')
                ->setData('session' , $this->_logPayment)
                ->saveTransaction();

            if(!$ccapproved){
                # for CC duplicate, declined, error, etc.
                $this->session->set_user('error_message' , 'payment is declined');
                $this->go_to(strtolower(__CLASS__) . "/error");
            }
        }

        parent::order_archive("complete", "complete");
        // Create PDF
        $this->load->library('pdfs/sogs');
        $this->sogs->process();
        //$this->_buildSogs();

        $this->load->library('pdfs/vital');
        $this->vital->process();

        $this->load->library('pdfs/authorization');
        $this->authorization->process();

        parent::_notify_message_preparation();
        parent::updateSessionOrder();

        //Send Email
        parent::sendCompleteEmail();
        parent::go_to($this->_vars['next_form']);
    }

    /**
     * PayBy Creditcard
     * @param  [int] $amount total price
     * @return boolean true/false
     */
    private function _payByCreditCard($amount){

        $user_session = $this->session->get_user();
        extract($this->_vars);
        extract($user_session[$session_index]);

        if ( strtolower(Vars::get($user_session['ccresult']['result'])) == 'accepted')
        {
            return true;
        }

        if( is_debugger($informant_email) and (strtolower($name_card) == 'funeralnet')){
            $user_session = $this->session->set_user('ccresult', array(
                'result' => 'Accepted',
                'reason' => 'Test Transaction'
            ));
            return true;
        } 
        else {

            $request = [
                'cardNumber'  => clean_card($this->input->post('card_number')) ,
                'cardName'    => Vars::get($name_card),
                'gatewayID'   => $this->settings['gateway_id'] ,
                'clientID'    => $this->cfg['client_id'] ,
                'clientIP'    => $_SERVER['SERVER_ADDR'],
                'userIP'      => $_SERVER['REMOTE_ADDR'] ,
                'checksum'    => '',
                'description' => $user_session['uniqid'].' - '.ucfirst($user_session['pkgtype']).' - $'.$amount ,
                'amount'      => str_replace("," , "" , $amount) ,
                'exp_year'    => Vars::get($expiration_year) ,
                'exp_month'   => Vars::get($expiration_month),
                'cvv'         => Vars::get($security_code),
                'email'       => Vars::get($informant_email) 
            ];

            extract($request);
            $code = $userIP.$clientIP.$amount.$cvv.$cardNumber.$cardName.$this->settings['gateway_publickey'];
            $code .= $clientID.$gatewayID.$this->settings['gateway_privatekey'];
            $request['checksum'] = md5($code);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,  'https://gateway.funeralnet.com/pay' );
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request)  );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $reponse = json_decode(curl_exec ($ch) , true);
            curl_close ($ch);

            $user_session = $this->session->set_user('ccresult', array(
                'result' => Vars::get($reponse['message']),
                'reason' => Vars::get($reponse['moremessage']) ,
                'object' => $reponse
            ));

            $this->_logPayment['payment_reponse'] = $reponse;
            $this->_logPayment['request_payment'] = $request;
            $this->_logPayment['request_payment']['cardNumber'] = senser($this->input->post('card_number'));


            if ($reponse['isApproved'] != 1){
                return false;
            }

            $this->session->set_user('error_message' , '');

            return true;
        }

    }

    /**
     * No pay
     * @param  [int] $amount total price
     * @return boolean true/false
     */
    private function _cash($amount){
        $user_session = $this->session->get_user();
        $user_session = $this->session->set_user('ccresult', array(
            'result' => 'Accepted',
            'reason' => 'check/cash'
        ));
        return true;
    }
}

/* End of file Payment.php */
/* Location: ./application/controllers/Payment.php */