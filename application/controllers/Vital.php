<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vital extends MY_Controller {

    private $_vars = array();
    private $_hash = NULL;

    public function __construct()
    {
        parent::__construct();

        $method = parent::uriDirection();
        $this->data['sidebarActive'] = 'paperwork';
        $this->_hash = ($this->input->get('hash')) ? $this->input->get('hash') : NULL;
    }

    public function index(){
        self::_init();
        self::_render();
    }

    private function _init(){
        parent::getDataByHash($this->_hash);
        self::_validate();

        $this->_vars = array(
            'stepkey'         => 'formvital',
            'hash'            => $this->_hash ,
            'prev_form'       => 'merchandise',
            'next_form'       => 'authorization',
            'page_title'      => 'Vital Information',
            'session_index'   => parent::vitalprefix ,
            'is_paperwork'    => true ,
            'is_print'        => false
        );
        $this->load->vars($this->_vars);
    }

    private function _validate(){
        parent::checkDirectForm();
        parent::check_paperwork();
        $user_session = $this->session->get_user();
        // if(!isset($user_session[parent::paymentprefix])){
        //     show_404();
        // }
    }

    private function _render(){
        extract($this->_vars);
        $user_session = $this->session->get_user();
        if($this->input->post('scriptaction') == "validate"){
            self::_action();
        }
        $this->data[$session_index] = isset($user_session[$session_index]) ?  jsSpecialchars(json_encode($user_session[$session_index])) : NULL;
        $this->data['vital'] = isset($user_session[$session_index]) ? $user_session[$session_index] : array();
        $this->data['documents'] = $user_session['documents'];
        //Status = complete forms
        $this->data['maskStatus'] = null;
        if( isset($this->data[ 'vital' ]['choiceForm']) and $this->data[ 'vital' ]['choiceForm'] == "Complete" ){
            $this->data['maskStatus'] = 'hide';
        }

        //Military Service
        $this->data['militaryStatus'] = 'hide';
        if( isset($this->data[ 'vital' ]['military']) and $this->data[ 'vital' ]['military'] == "Yes" ){
            $this->data['militaryStatus'] = null;
        }

        //Status obit Upload
        $this->data['obitStatus'] = 'hide';
        if(isset($this->data[ 'vital' ]['obitChoice']) and $this->data[ 'vital' ]['obitChoice'] == "Yes"){
            $this->data['obitStatus'] = null;
        }

        parent::load_step( strtolower(__CLASS__) .'/index', $page_title);
    }

    private function _action(){
        $data = $this->input->post();

        unset($data['pkgtype']);
        unset($data['selected']);
        unset($data['next_form']);
        unset($data['session_index']);

        extract($this->_vars);
        $user_session = $this->session->set_user($session_index , $data);
        parent::go_to($next_form);
    }



}

/* End of file Vital.php */
/* Location: ./application/controllers/Vital.php */