<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends MY_Controller {

    private $_targetMethod;
    private $_vars = array();

    public function __construct()
    {
        parent::__construct();
        $this->_targetMethod = strtolower(basename( $this->uri->uri_string() ));;
        $this->data['sidebarActive'] = 'pkg';
    }

    public function index(){
        self::_init();

        if(empty($this->_targetMethod)){
            self::atneed();
        }
        else{
            $method = strtolower($this->_targetMethod);
            if(method_exists( $this , $method )){
                $reflection = new ReflectionMethod(__CLASS__, $method);
                if ($reflection->isPublic()) {
                    $this->$method();
                }
            }
            else{
               show_404();
            }
        }
    }

    private function _init(){
        $user_session = $this->session->get_user();
        $this->_vars = array(
            'next_form' => 'merchandise',
            'page_title' => $this->cfg['steps']['pkg']['name'] ,
            'session_index' => "packageSelected"
        );
        $this->load->vars($this->_vars);
    }

    public function atneed(){
        $user_session = $this->session->get_user();
        if( $this->morder->checkCompleted($this->archive_id) or (isset($user_session['pkgtype']) and  $user_session['pkgtype'] != "atneed") ){
            $this->session->clear();
        }
        $this->session->set_user('pkgtype','atneed');
        self::_render();
    }

    public function preneed(){
        $user_session = $this->session->get_user();
        if( $this->morder->checkCompleted($this->archive_id) or (isset($user_session['pkgtype']) and  $user_session['pkgtype'] != "preneed") ){
            $this->session->clear();
        }
        $this->session->set_user('pkgtype','preneed');
        self::_render();
    }

    private function _render(){

        $user_session = $this->session->get_user();
        //if pay online done will can not change options
        parent::checkPaymentAccept();

        //Load packages
        $this->data['packages'] = $this->mpackage->getData();
        $this->data['transport'] = $this->mserives->getByType('transport');

        $this->data['handling'] = $this->mserives->getByType('handling');
        $this->data['addition'] = $this->mserives->getByType('addition');
        extract($this->_vars);
        if($this->input->post('scriptaction') == "validate"){
           self::_action();
        }

        //Get Delivery Counties
        $this->data['zoneCounties'] = array();
        foreach ( $this->cfg['counties'] as $key => $row ){
            if ( $row['delivery'] ){
                $this->data['zoneCounties'][] = $row['name'];
            }
        }
        $this->data['zoneCounties'] = implode("," , $this->data['zoneCounties'] );
        $this->data[$session_index] = isset($user_session[$session_index]) ?  jsSpecialchars(json_encode($user_session[$session_index])) : NULL;

        //Get Session Data
        if(isset($user_session[$session_index])){
            foreach($user_session[$session_index] as $key => $row){
                $index = $key."_selected";
                $this->data[$index] = isset($user_session[$session_index][$key]) ? $user_session[$session_index][$key] : array();
            }
        }
        parent::load_step( strtolower(__CLASS__) .'/index', $page_title);
    }

    private function _action(){
        parent::save_selected($this->_vars);
        parent::_calculate_amount_due();
        parent::_notify_message_preparation();

        $user_session = $this->session->get_user();
        parent::go_to($this->_vars['next_form']);
    }

}

/* End of file Packages.php */
/* Location: ./application/controllers/Packages.php */