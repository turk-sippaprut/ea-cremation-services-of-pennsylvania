<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debuger extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        $pageUrl =  strtolower(basename($this->uri->uri_string()));
        if($pageUrl == "debuger"){
             $this->print_session();
        }
        else{
            $method = $pageUrl;
            if(method_exists( $this , $method )){
                $reflection = new ReflectionMethod(__CLASS__, $method);
                if ($reflection->isPublic()) {
                    $this->$method();
                }
            }
            else{
               show_404();
            }
        }
    }

    public function print_session(){
            $data['session'] = $this->session->get();
            $data['settings'] = $this->settings;
            $data['cfg'] = $this->cfg;
            $data['Server'] = $_SERVER;

            echo '<style>
            pre {
                background-color: #F0F0F0;
                border: 1px solid #CCCCCC;
                margin-bottom: 30px;
                padding: 10px;
            }
            pre > b {
                font-weight: normal;
            }
            </style>';

            echo "<pre>";
            print_r($data);
            echo "</pre>";

          // +Kint::dump($data);
    }

    public function showSessionForm(){
        $user_session = $this->session->get_user();
        $data[parent::paymentprefix] = $user_session[parent::paymentprefix];
        $data[parent::vitalprefix] = $user_session[parent::vitalprefix];
        $data[parent::authprefix] = $user_session[parent::authprefix];
        dd($data);
    }

    public function debug(){
        $data['session'] = $this->session->get();
        $data['settings'] = $this->settings;
        $data['cfg'] = $this->cfg;
        $data['Server'] = $_SERVER;
        +Kint::dump($data);
    }

    public function reportAdmin(){
        $user_session = $this->session->get_user();
        dd($user_session['admintable']);
    }


    public function destory_session(){
        $this->session->clear();
        echo "Session Destory";
        $this->go_to("atneed");

    }

    public function clear_cache(){
        if(file_exists('application/cache/ea-template.html')){
            if(unlink('application/cache/ea-template.html')){
                echo "Wrapper cache was cleared successful.";
            }else{
                echo "Process fail!<br>Wrapper cache was not cleared out.";
            }
        }else{
            echo "Wrapper cache is now empty.";
        }

        if(file_exists('application/cache/ea-template-print.html')){
            if(unlink('application/cache/ea-template-print.html')){
                echo "Wrapper Print cache was cleared successful.";
            }else{
                echo "Process fail!<br>Wrapper cache was not cleared out.";
            }
        }else{
            echo "Wrapper cache is now empty.";
        }

        $data['type'] = "memcached";
        $data['key'] = $this->cfg['client_id'] . "-*";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://mfs5.funeralnet.com/cachedriver/deleteBykey.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)  );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        devprint($output);

    }

    public function fillvital(){
        error_reporting(E_ALL);
        $this->load->library('pdfs/vital');
        dd($this->vital->process());
        // $user_session = $this->session->get_user();
        // $pdftk = new CustomPdftk(array(
        //     'client_id'   => $this->cfg['client_id'] ,
        //     'file'        => base_url($this->cfg['documents']['vital']['source']) ,
        //     "destination" => 'pdf/'.$this->cfg['client_id'].'/'.date("Y/m/d/").$user_session['order_archive_id']."/" ,
        //     'file_name'   => basename($this->cfg['documents']['vital']['source'])
        // ));
        // $data = $user_session[parent::vitalprefix];

        // $data = spiltPhone($data , 'informant_tel');
        // $data = setMultiLine($data , 'insurance_information' );
        // $data = setMultiLine($data , 'music_selection' );
        // $data = setMultiLine($data , 'notify_message' );

        // //#d
        // $data = spiltPhone($data , 'arrangement1_tel');
        // $data = spiltPhone($data , 'arrangement2_tel');

        // #E
        // $data = spiltPhone($data , 'emegency1_tel');
        // $data = spiltPhone($data , 'emegency2_tel');

        // $data = setMultiLine($data , 'signed_notes');

        // //Part B
        // $data = spiltPhone($data , 'person_phone');

        // $reponse = $pdftk->setData($data)->fill();
        // dd($reponse);
    }

    public function fillauth(){
        error_reporting(E_ALL);
        $this->load->library('pdfs/authorization');
        dd($this->authorization->process());
    }

    public function fillsogs(){
       $this->load->library('pdfs/sogs');
        dd($this->sogs->process());
    }

    public function ftni(){

        $ftniUrl = 'https://test.ftnirdc.com/httpext.dll?set_session';
        $data = array(
            'username' => 'markmyers',
            'password' => 'test1234' ,
            'customer_id' => '10015'
        );
        $get = http_build_query($data);
        $ftniUrl  = urldecode($ftniUrl."&".$get);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ftniUrl );
        curl_setopt($ch, CURLOPT_POST, 0);
//         curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//     'Expect:',
//     'Content-Type: application/x-www-form-urlencoded',
//     'Content-Length: 0'
// ));
       // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->_data)  );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close ($ch);

        echo '<pre>';
        print_r($output);
        echo '</pre>';
    }

    public function insertCountry(){
        $data = array(
            'name' => 'Sullivan' ,
            'missouri' => '4' ,
            'kansas' => 'other'
        );
        if($this->mcounty->countData(array('name LIKE "%'.$data['name'].'%"' => NULL)) == 0){
            $this->mcounty->insert($data);
            $this->mcounty->deleteCacheBykey($this->cfg['client_id'].'-ea-county_table');
            echo "insert Complete ".$data['name'];
            exit();
        }
        echo "Already Data Name is ".$data['name'];

        dd($data);
    }

    public function stringCountry(){
        $string = '<option selected="" value="Adams">Lancaster</option>
                        <option value="Allegheny">Adams</option>
                        <option value="Berks">Berks</option>
                        <option value="Bucks">Bucks</option>
                        <option value="Chester">Chester</option>
                        <option value="Cumberland">Cumberland</option>
                        <option value="Dauphin">Dauphin</option>
                        <option value="Juniata">Juniata</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lehigh">Lehigh</option>
                        <option value="Montgomery">Montgomery</option>
                        <option value="Northumberland">Northumberland</option>
                        <option value="Perry">Perry</option>
                        <option value="Philadelphia">Philadelphia</option>
                        <option value="Schuylkill">Schuylkill</option>
                        <option value="Snyder">Snyder</option>
                        <option value="York">York</option>';
        $data = strip_tags($string);
        $data = nl2br($data);
        $data = explode('<br>' , trim($data));
        print_r($data);
    }

    public function testlogs(){
        $user_session = $this->session->get_user();
        $package_id = getFirstKey($user_session['packageSelected']['package']);
        $package_name = $user_session['packageSelected']['package'][$package_id]['name'];
        $logs = array(
            'order_archive_id'  =>  $this->archive_id ,
            'uniqid'            =>  $user_session['uniqid'] ,
            'pkgtype'           =>  $user_session['pkgtype'] ,
            'package_name'      =>  $package_name,
            'informant_name'    =>  $user_session[parent::paymentprefix]['informant_name'],
            'informant_phone'   =>  $user_session[parent::paymentprefix]['informant_phone'],
            'informant_email'   =>  $user_session[parent::paymentprefix]['informant_email'],
            'payment_method'    =>  "",
            'summary'           =>  $user_session['summary'],
            'browser'           =>  array(
                'HTTP_USER_AGENT' => $_SERVER['HTTP_USER_AGENT'],
                'REMOTE_ADDR'     => $_SERVER['REMOTE_ADDR'],
                'SERVER_PORT'     => $_SERVER['SERVER_PORT'],
                'REQUEST_TIME'    => $_SERVER['REQUEST_TIME']
            )
        );

        $this->mylogs
            ->setData('path' , SEVERPATH . assets_path('logs/transaction/' .date("Y/m/d/")) )
            ->setData('fileName' , 'transaction.log')
            ->setData('session' , $logs)
            ->saveTransaction();
    }

    public function testForm(){
        echo "test";
        echo '<script src="'.assets_path('js/template/payment.js?_='.strtotime('now')).'"></script>';
        echo '<script src="https://secure.funeralnet.com/js/reconfigformat_dev.js"></script>';
        echo '<form action="" id="mainform" name="mainform" method="POST">';
        echo '<input type="text" class="validate[funcCall[reconfigure]]" reconfigure="phone" name="phone" value="" />';
        echo '</form>';
    }

    public function timezone() {
        myprint($_SESSION);
    }


    public function testFunction(){
        //echo Http::IP($trustProxy = false);
      //  error_reporting(E_ALL);
       // echo $data = Vars::get(&$user_session['uniqid'] , "no");
       //  error_reporting(E_ALL);
       //  Http::nocache();
       // //$data = Dates::human( Dates::is('-3 day') , $format = 'd M Y H:i');
       //  //use JBZoo\Utils\Arr;
       //  // $user_session = $this->session->get_user();
       //  //  $data = Arr::key( 'pkgtype' , $user_session , true );
       //  // echo "test";
       //   //$data = Str::splitCamelCase('You are Welcome !!');
       //  // $data = Url::current();
       //  $data = Vars::get(&$e , "no");
       //  echo "tasdasd";
       //   echo '<pre>';
       //   print_r($data);
       //   echo '</pre>';

        //dd($data);
    }


}

/* End of file debug.php */
/* Location: ./application/controllers/debug.php */