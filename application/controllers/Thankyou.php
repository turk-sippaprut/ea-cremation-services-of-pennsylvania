<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thankyou extends MY_Controller {

    private $_targetMethod;
    private $_vars = array();

    public function __construct()
    {
        parent::__construct();
        $method = parent::uriDirection();
        $this->data['sidebarActive'] = 'paperwork';
    }

    public function index()
    {
        self::_init();
        if(empty($this->_targetMethod)){
            self::_render();
        }
        else{
            show_404();
        }
    }

     private function _render(){
        extract($this->_vars);
        $user_session = $this->session->get_user();
        $this->data['user_session'] = $user_session;
        parent::load_step( strtolower(__CLASS__).'/index' , $page_title);
    }

    private function _init(){
        self::_validate();
        // # Assign page variable
        $user_session = $this->session->get_user();
        $this->_vars = array(
            'stepkey'       => 'Form Complete',
            'prev_form'     => '',
            'next_form'     => '',
            'page_title'    => 'Forms Complete',
            'pkgtype'       => $user_session['pkgtype']
        );
        $this->load->vars($this->_vars);
    }

    private function _validate(){
        $validate = array(
            'order'    => $this->morder->checkCompleted($this->archive_id) ,
            'paper'    => $this->morder->checkCompletedPaper($this->archive_id) ,
            'download' => $this->morder->checkDownloadPaper($this->archive_id) ,
        );
        if($validate['order'] == false or ($validate['paper'] == false and $validate['download'] == false  )){
            show_404();
        }
    }

}

/* End of file Thankyou.php */
/* Location: ./application/controllers/Thankyou.php */