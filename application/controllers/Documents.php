<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends MY_Controller {

    //Path upload to s3
    private $_folderTarget = NULL;

    //Path upload to server
    private $_path = NULL;

    public function __construct(){
        parent::__construct();

        $user_session = $this->session->get_user();
        $this->_folderTarget = 'documents/'.$this->cfg['client_id'].'/'.date("Y/m/d/".$user_session['order_archive_id']."/");
        $this->_path =  $_SERVER['DOCUMENT_ROOT']  .assets_path('uploads/');

    }

    public function index(){
        $pageUrl =  strtolower(basename($this->uri->uri_string()));
        if(empty($pageUrl)){
            show_404();
        }
        else{
            $method = $pageUrl;
            if(method_exists( $this , $method )){
                $reflection = new ReflectionMethod(__CLASS__, $method);
                if ($reflection->isPublic()) {
                    $this->$method();
                }
            }
            else{
               show_404();
            }
        }
    }

    public function uploadPhoto(){
        try {

            if( !isset($_FILES['qqfile']) or empty($_FILES['qqfile']['name'])){
                throw new Exception("There is no Image");
            }

            $user_session = $this->session->get_user();
            $photoName = 'Photo_Identification';

            //Upload in Server
            $data = $this->_upload
                    ->setPathUpload($this->_path)
                    ->setFileName('qqfile')
                    ->run();
            $ext = $this->_upload->getExtFile($data['file_name']);
            $photo = array(
                "source_image" => $data['full_path'] ,
                "filename" => $user_session['uniqid'].'_'.$photoName.'.'.$ext
            );
            $newFile = $this->_upload
                        ->setSize(300 , 300)
                        ->resize($photo);
            unlink($data['full_path']);

            //Upload to the S3
            $s3Filename = $photoName.'.'.$ext;
            $this->_s3->initialize(
                array(
                    'folder'     => $this->_folderTarget ,
                    'tmpfile'    => $this->_path . $newFile,
                    'fileUpload' => $s3Filename
                )
            );
            $this->_s3->send();


            // Save new Session
            $user_session['documents']['photo']['url'] = s3lib::S3URL . $this->_folderTarget . $s3Filename;
            $user_session['documents']['photo']['name'] = "Photo Identification";
            $user_session['documents']['photo']['time'] = date("Y-m-d H:i:s");
            $user_session = $this->session->set_user("documents" , $user_session['documents'] );

            parent::updateSessionOrder();
            $arr = array('type' => 'success' ,
                'title' => 'Success' ,
                'text' => "Generate is done!" ,
                'path' => $user_session['documents']['photo']['url'].'?_=' . strtotime('now') ,
                'fileName' => $user_session['documents']['photo']['name'] ,
            );
            echo json_encode($arr);
            exit();

        } catch (Exception $e) {
            $arr = array('type' => "error" , 'title' => 'Error' , 'text' => $e->getMessage() , "upload" => $this->_path );
            echo json_encode($arr);
            exit();
        }
    }

    public function remove(){
        try {

            $this->form_validation->set_rules('key', 'Key', 'required');
            if($this->form_validation->run() == false){
                throw new Exception(validation_errors());
            }

            $user_session = $this->session->get_user();
            $key = $this->input->post('key');

            $fileList[] = basename($user_session['documents'][$key]['url']);
            $folder = str_replace(s3lib::S3URL , "" , $user_session['documents'][$key]['url']);
            $folder = str_replace($fileList[0] , "" , $folder);
            $this->_s3->initialize(
                array(
                    'folder'     => $folder
                )
            )->delete($fileList);
            unset($user_session['documents'][$key]);
            $this->session->set_user('documents' , $user_session['documents']);
            parent::updateSessionOrder();


            $arr = array('type' => 'success' , 'title' => 'Success' , 'text' => "Remove Success" );
            echo json_encode($arr);
            exit();

        }
        catch (Exception $e) {
                $arr = array('type' => "error" , 'title' => 'Error' , 'text' => $e->getMessage() );
                echo json_encode($arr);
                exit();
            }
        }



}

/* End of file Documents.php */
/* Location: ./application/controllers/Documents.php */