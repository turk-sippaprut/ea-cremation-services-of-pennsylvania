<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends MY_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index()
    {
        $pageUrl =  strtolower(basename($this->uri->uri_string()));
        $this->data['cfg'] = $this->cfg;
        if(empty($pageUrl)){
        }
        else{
            $method = $pageUrl;
            if(method_exists( $this , $method )){
                $reflection = new ReflectionMethod(__CLASS__, $method);
                if ($reflection->isPublic()) {
                    $this->$method();
                }
            }
            else{
               show_404();
            }
        }
    }

    public function view(){
        $this->data['user_session'] = $this->session->get_user();
        $view = $this->load->view('cart', $this->data, true);

        echo json_encode(array(
            'result' => 'success',
            'view'  => $view
        ));
    }

    public function save(){

        try {
            $this->form_validation->set_rules('session_index', 'Session Index', 'required');
            if($this->form_validation->run() == false){
                throw new Exception(validation_errors());
            }
            $vars = array(
                'session_index' => $this->input->post('session_index')
            );

            $this->load->vars($vars);
            if($this->input->post('selected')){
                parent::save_selected($vars);
            }

            parent::_calculate_amount_due();

            $this->data['user_session'] = $this->session->get_user();
            $view = $this->load->view('cart', $this->data , true);

            echo json_encode(array(
                'result' => 'success',
                'view'  => $view
            ));
        }
        catch (Exception $e) {
            echo json_encode(array(
                'result' => 'error',
                'pnotify' => array(
                    'title' => 'Save Error',
                    'type' => 'error',
                    'text' => $e->getMessage(),
                    'width' => '600px',
                ),
            ));
            exit();
        }
    }

}

/* End of file Cart.php */
/* Location: ./application/controllers/Cart.php */