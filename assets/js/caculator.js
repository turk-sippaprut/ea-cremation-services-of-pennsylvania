
 (function($){

    var methods = {

        init : function( options  ){
            return this.each(function(){
                var $this = $(this);
                $.ajax({
                      url : root + "cart/save"   ,
                      data : options ,
                      type : "POST" ,
                      cache: false,
                      contentType:"application/x-www-form-urlencoded; charset=utf-8",
                      dataType:"json" ,
                      success : function(data){
                          if(data.result == 'success'){
                               $this.html(data.view);
                          } else{
                              console.log(data.message)
                          }
                      },
                      error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.responseText);
                      }
                });
            });
        } ,
        view : function(options){
          return this.each(function(){
              var $this = $(this);
              $.ajax({
                      url : root +"cart/view"   ,
                      data : options ,
                      type : "POST" ,
                      cache: false,
                      contentType:"application/x-www-form-urlencoded; charset=utf-8",
                      dataType:"json" ,
                      success : function(data){
                          if(data.result == 'success'){
                               $this.html(data.view);
                          } else{
                              console.log(data.message)
                          }
                      },
                      error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.responseText);
                      }
              });
          });
        }
  };


  $.fn.caculator = function ( options ){

    var defaults = {
        session_index : $("#session_index").val(),
        selected : $("#selected").val()
    }

    if(typeof options === 'object' || ! options)
       {
         if(options){
          $.extend( defaults, options );
          }
       return methods.init.apply( this, [defaults] );
     }
     else
     {
       if(typeof methods[options] !== "undefined"){
           return methods[options].apply( this , [defaults]);
       }
       else{
          console.log("Error not method")
       }
    }

  };

}(jQuery));


