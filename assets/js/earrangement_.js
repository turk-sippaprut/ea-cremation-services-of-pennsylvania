$(function(){
	$.fn.eaFn = function ( options ){
		var opts = $.extend({
			actions : "click",
			parentTarget : true,
			addWithClass :'className',
			removeWithClass :'className',
			hide : '#target',
			show : '#target',
			callback : function(){},
			callbefore : function (){}
		},options);

		return this.each(function(){

			opts.callbefore.call(this);

			$(this).bind( opts.actions , function (event){

				if (opts.parentTarget) { // add class to parent ?
					$(this).parent().removeClass(opts.removeWithClass);
					$(this).parent().addClass(opts.addWithClass);

				} else {
					$(this).removeClass(opts.removeWithClass);
					$(this).addClass(opts.addWithClass);
				}

				$(opts.hide).hide();
				$(opts.show).show();
				opts.callback.call(this);
			});

		});
	};


	$.fn.clearNames = function ( options ) {
		var opts = $.extend({
			prefix:$(this).attr('id'),
			callback : function(){}
		},options);

		return this.each(function(){
			var indexIncred = 1 ;
			$(this).find('input').each(function(){
				var o = $(this);
				var t = o.attr('type');
				if ( t=='text' || t=='password' || t=='radio' || t=='checkbox'){
					if (typeof(o.data('name'))!='undefined'){
						var tempAttr = o.attr('name');
						o.removeAttr('name');
						o.attr('name',opts.prefix +"_"+ o.data('name'));
						o.data('tempAttr',tempAttr);
					} else {
						if (typeof(o.attr('name'))=='undefined'){
							o.attr('name',opts.prefix +"_"+ indexIncred);
							indexIncred +=1;
						} else {
							o.attr('name',opts.prefix +"_"+ o.attr('name'));
						}
					}
					o.attr('name',o.attr('name').toLowerCase().replace(" ","_"));
				}

			});

			$(this).find('select').each(function(){
				var o = $(this);
				if (typeof(o.data('name'))!='undefined'){
					var tempAttr = o.attr('name');
					o.removeAttr('name');
					o.attr('name',opts.prefix +"_"+ o.data('name'));
					o.data('tempAttr',tempAttr);
				} else {
					if (typeof(o.attr('name'))=='undefined'){
						o.attr('name',opts.prefix +"_"+ indexIncred.toString());
						indexIncred +=1;
					} else {
						o.attr('name',opts.prefix +"_"+ o.attr('name'));
					}

				}
				o.attr('name',o.attr('name').toLowerCase().replace(" ","_"));
	          //alert(o.attr('name'));
	      });


		});
};




	/*
	* bind data to Element (input ,select , textarea) with json format after back to page again
	*$('#FormTarger').jsonToElement({json:{}});
	*/
	$.fn.jsonToElement = function ( options ){
		var opts = $.extend({
			json : '{}'
		},options);


	  //var obj = $.parseJSON(opts.json);
	  var obj = opts.json;

	  return this.each(function(){
	  	$(this).find('input').each(function(){
	  		var o = $(this);
	  		var t = o.attr('type');
	  		var n = o.attr('name');
	  		var v = o.attr('value');

	  		$.each(obj,function(key,value){

	        if ( t == 'text' || t == 'hidden'){
	        	if(key == n  && n != 'selected'){
	        		o.val(value);
	        		delete obj[key];
	        	}
	        } else if( t=='checkbox' || t=='radio' ) {
	        	if ( key == n ){
	        		if ( v.toLowerCase() == value.toLowerCase() ) {
	        			o.click();
	        			delete obj[key];
	        		}
	        	}

	        }
	        //return false;
	    });
	  	});

	    // select
	    $(this).find('select').each(function(){
	    	var o = $(this);
	    	var n = o.attr('name');
	    	$.each(obj,function(key,value){
	    		if ( key == n ){
	    			o.find('option').removeAttr("selected");
	    			o.find('option').each(function(){
	    				if ( $(this).val() == value ){
	              //$(this).prop('selected', true).attr("selected","selected");
	              o.val(value);
	              o.change();
	              delete obj[key];
	          }
	      });
	    		}
	        //return false;
	    });
	    });

	    //textarea
	    $(this).find('textarea').each(function(){
	    	var o = $(this);
	    	var n = o.attr('name');
	    	$.each(obj,function(key,value){
	    		if ( key == n ){
	    			o.text(value);
	    			delete obj[key];
	    		}
	    	});
	    });


	});

};


$.fn.merch = function ( options ) {

	var opts = $.extend({
		targetClass:'merch-col-active',
	        methodType:"", // select and click
	        clearData:false, // true only click
	        afterClick:function(){},
	        afterSelected : function(){}
	    },options);

	return this.each(function(){
		var o = $(this);
		var oid = o.data('id');
		var price = o.data('price');
		var group = o.data('group');
		var quan = 0;
	      var addBorder = false; // เก็บดูว่า เป้าหมาย มีการเลือกค่าไปแล้วหรือไม่
	      /*o.hover(function(){
	      	if( o.hasClass(opts.targetClass) ){
	      		addBorder = true;
	      	} else {
	      		o.addClass(opts.targetClass);
	      	}
	      },function(){
	      	if (!addBorder) {
	      		o.removeClass(opts.targetClass);
	      	}
	      });*/

	      switch (opts.methodType) {
	      	case "select" :

	      	/* init function */
	      	if( $('select[name='+group+'-'+oid+'] option:selected').val() != "0" ){
	      		o.addClass(opts.targetClass);
	      		addBorder = true;
	      		//o.find('.product-price').text("included");
	      	}
	      	/* bind function change */
	      	o.find('select[name='+group+'-'+oid+']').change(function(){
	      		if ($(this).val()!="0"){

	      			quan = $(this).val();
	      			addBorder = true;
	      			o.addClass(opts.targetClass);
	      			var obj = {
	      				"id" :oid,
	      				"group" :group,
	      				"price" :price,
	      				"quan" :quan
	      			};
	      			$.ea.addToCart(obj);

		        } else {

		        	addBorder = false;
		        	o.removeClass(opts.targetClass);
		        	var obj = {
		        		"id" :oid,
		        		"group" :group
		        	};
		        	$.ea.removeFromCart(obj);
		        }

		        opts.afterSelected.call(this);
		    });

	      	break;

	      	case "click" :

	      	o.click(function(){

				if (opts.clearData){
					o.parent().find('.'+opts.targetClass+'-click').each(function(){
						if( $(this).hasClass(opts.targetClass+'-click') ){
							$(this).removeClass(opts.targetClass+'-click');
							var obj = {
								"id" :$(this).data('id'),
	      						"group" :$(this).data('group')
							};
							//console.log(obj);
							$.ea.removeFromCart(obj);
						}
					});
				}
	      			o.addClass(opts.targetClass+'-click');
		          	if (!addBorder){ // ถ้าเป็นการ Click เพิ่ม
			          	//addBorder = true;
			            //Add เข้า Object
			            var obj = {
			            	"id" :oid,
			            	"group" :group,
			            	"price" :price
		            	};
		            	//console.log(obj);
		            	$.ea.addToCart(obj);
		          	} else { //เป็นการคลิกเอาออก
		          		o.removeClass(opts.targetClass+'-click');
		          		addBorder = false;
		            	//Remove Object
		            	var obj = {
		        			"group" :group
			        	};
			        	$.ea.removeFromCart(obj);

		        }
		        opts.afterClick.call(this);
		    });
	      	break;
	      }
	  });
};

	/**
	 * For change input to div
	 */
	$.fn.formForPrint = function( options ) {
		var _default = $.extend({
			targetClass : "line-input"
		} , options );

		return this.each(function(){

			$(this).find(':input,select').each(function() {
		        switch(this.type) {
		            case 'password':
		            case 'text':
		            	var $this = $(this);
		                var value = $this.val();
		                var width = null;
		                if(!_.isUndefined($this.data("size"))){
		                	width = "style='width:"+$this.data("size")+"'"
		                }
		                var html  = '<div '+width+' class="'+_default.targetClass+'">' +
		               			   value +
		               			   '</div>'+
		               			   '';
		               	//if(value != ""){
			               	$(html).insertBefore($this);
			               	$this.remove();
		               	//}

		            break;
		            default :

		            	$(this).prop('disabled' , true);
		            break;
		        }
    		});
		});
	}

	/*
	*Plugin tabify
	*
	*/
	$.fn.extend({
		tabify: function( callback ) {

			function getHref(el){
				hash = $(el).find('a').attr('href');
				hash = hash.substring(0,hash.length-4);
				return hash;
			}

			function setActive(el){

				$(el).addClass('tab-active');
				$(getHref(el)).show();
				$(el).siblings('span').each(function(){
					$(this).removeClass('tab-active');
					$(getHref(this)).hide();
				});
			}

			return this.each(function() {

				var self = this;
				var	callbackArguments 	=	{'ul':$(self)};

				$(this).find('span a').each(function(){
					$(this).attr('href',$(this).attr('href') + '-tab');
				});

				function handleHash(){

					if(location.hash && $(self).find('a[href=' + location.hash + ']').length > 0){
						setActive($(self).find('a[href=' + location.hash + ']').parent());
					}
				}

				if(location.hash){
					handleHash();
				}

				setInterval(handleHash,100);

				$(this).find('span').each(function(){
					if($(this).hasClass('tab-active')){
						$(getHref(this)).show();
					} else {
						$(getHref(this)).hide();
					}
				});

				if(callback){
					callback(callbackArguments);
				}

			});
		}
	});

	/*
	$.fn.getJson = function(opts){

		var opts = $.extend({
			index : "" ,
	        callback:function(){}
	    },options);

		return this.each(function(){

			$.ajax({

			})


			opts.callback();
		})
	}
	*/

});