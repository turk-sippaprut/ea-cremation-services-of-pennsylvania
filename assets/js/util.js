/*
// don't use anymore.
function showPkgDetails(id){
	$('.pkg_detail').hide();
	$('#pkg_details_'+id).show();
}
function selectePkg(id){
	$("#pkg"+id).get(0).checked=true;
}
function backToCurrent(){
	var elem = document.getElementById('mainform').elements;
	for(var i = 0; i < elem.length; i++)
	{
		if(elem[i].type != "radio") continue;
		if(elem[i].checked){
			showPkgDetails(elem[i].value);
		}
	}
}
*/

function countPropertiesInObject(theObject){
	var propertyCount = 0;
	for (var p in theObject)
	{
		propertyCount++;
	}
	return propertyCount;
}

function inArray(needle, haystack){
	// Checks the given array (haystack) to see if it contains the given item of
	// interest (needle).

	for (var k = 0; k < haystack.length; k++)
	{
		//console.debug('inArray: "', needle, '" = "', haystack, '"?');
		if (needle == haystack[k]) return true;
	}

	return false;
}

function listFormElements(form){
	return true;
	// Sets the value of a hidden field to a comma-separated list of form
	// element names for use on the server-side to weed out removed data
	// from the session information.

	// var elements = form.elements;
	// var formElementNames = '';
	// for (var k = 0; k < elements.length; k++)
	// {
	// 	if ((elements[k].type == 'checkbox') ||
	// 		(elements[k].type == 'radio')    ||
	// 		(elements[k].type == 'text')     ||
	// 		(elements[k].type == 'textarea') ||
	// 		(elements[k].type == 'hidden'))
	// 	{
	// 	if (formElementNames.length > 0) formElementNames += ',';
	// 	formElementNames += elements[k].name;
	// 	}
	// }
	// if (formElementNames.length > 0) document.mainform.form_elements.value = formElementNames;

	// return true;
}

function formToParam(form){ // transform from form's element to ajax parameter
	var ret=new Array();
	for(var z = 0 ; z < form.elements.length ; z++){
		var obj = form.elements[z];
		if(!obj.name) continue;
		if(obj.type=="checkbox" && !obj.checked) continue;
		if(obj.type=="radio" && !obj.checked) continue;
		ret.push(encodeURIComponent(obj.name) + "=" + encodeURIComponent(obj.value));
	}
	return ret.join("&");
}

// function number_format (number, decimals, dec_point, thousands_sep) {
// 		// *     example 1: number_format(1234.56);
// 		// *     returns 1: '1,235'
// 		// *     example 2: number_format(1234.56, 2, ',', ' ');
// 		// *     returns 2: '1 234,56'
// 		// *     example 3: number_format(1234.5678, 2, '.', '');
// 		// *     returns 3: '1234.57'
// 		// *     example 4: number_format(67, 2, ',', '.');
// 		// *     returns 4: '67,00'
// 		// *     example 5: number_format(1000);
// 		// *     returns 5: '1,000'
// 		// *     example 6: number_format(67.311, 2);
// 		// *     returns 6: '67.31'
// 		// *     example 7: number_format(1000.55, 1);
// 		// *     returns 7: '1,000.6'
// 		// *     example 8: number_format(67000, 5, ',', '.');
// 		// *     returns 8: '67.000,00000'
// 		// *     example 9: number_format(0.9, 0);
// 		// *     returns 9: '1'
// 		// *    example 10: number_format('1.20', 2);
// 		// *    returns 10: '1.20'
// 		// *    example 11: number_format('1.20', 4);
// 		// *    returns 11: '1.2000'
// 		// *    example 12: number_format('1.2000', 3);
// 		// *    returns 12: '1.200'
// 		// *    example 13: number_format('1 000,50', 2, '.', ' ');
// 		// *    returns 13: '100 050.00'
// 		// Strip all characters but numerical ones.
// 		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
// 		var n = !isFinite(+number) ? 0 : +number,
// 				prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
// 				sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
// 				dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
// 				s = '',
// 				toFixedFix = function (n, prec) {
// 						var k = Math.pow(10, prec);
// 						return '' + Math.round(n * k) / k;
// 				};
// 		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
// 		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
// 		if (s[0].length > 3) {
// 				s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
// 		}
// 		if ((s[1] || '').length < prec) {
// 				s[1] = s[1] || '';
// 				s[1] += new Array(prec - s[1].length + 1).join('0');
// 		}
// 		return s.join(dec);
// }

function IsNumeric(num) {
     return (num >=0 || num < 0);
}
Array.prototype.remove = function(index){
    delete this[index];
    return this;
};
Array.prototype.clean = function(){
    var arr1 = this, arr2 = [];
    for(var a in arr1){
        if(arr1[a]&&arr1.hasOwnProperty(a)){
            arr2.push(arr1[a]);
        }
    }
    this.splice(0);
    for(var b in arr2){
        if(arr2.hasOwnProperty(b)){
            this.push(arr2[b]);
        }
    }
    return this;
};
function scrollTo(selector, diff){
	var diff = diff || 0;
	$('html, body').animate({
		scrollTop: $(selector).offset().top + diff
	}, 500).clearQueue();
}

 function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }


function clear_form_elements($ele) {

    $ele.find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });

}//func


function ObjectLength( object ) {
    var length = 0;
    for( var key in object ) {
        if( object.hasOwnProperty(key) ) {
            ++length;
        }
    }
    return length;
};


/*
 * Jquery custom instance
 */
// Scroll To
$.fn.scrollTo = function(diff) {
	var diff = diff || 0;
	$('html, body').animate({
		scrollTop: this.offset().top + diff
	}, 500).clearQueue();
	return this;
};
$.fn.goTo = function(diff) {
	var diff = diff || 0;
	$('body').get(0).scrollTop = this.offset().top + diff;
	$('html').get(0).scrollTop = this.offset().top + diff;
	return this;
};

$.fn.hasData = function(key) {
  return (typeof $(this).data(key) != 'undefined');
};



/*
 * For IE8
 */
Object.keys=Object.keys||function(o,k,r){r=[];for(k in o)r.hasOwnProperty.call(o,k)&&r.push(k);return r}


/**
 * Js Helper
 */
var util = {
    getUrl : function() {
        var url = location.href;  // entire url including querystring - also: window.location.href;
        var baseURL = url.substring(0, url.indexOf('/', 14));

        if (baseURL.indexOf('http://localhost') != -1) {
            // Base Url for localhost
            var url = location.href;  // window.location.href;
            var pathname = location.pathname;  // window.location.pathname;
            var index1 = url.indexOf(pathname);
            var index2 = url.indexOf("/", index1 + 1);
            var baseLocalUrl = url.substr(0, index2);

            return baseLocalUrl ;
        }
        else {
            // Root Url for domain name
            return baseURL;
        }
    } ,

    in_array : function (needle, haystack, argStrict) {
      var key = '',
        strict = !! argStrict;

      if (strict) {
        for (key in haystack) {
          if (haystack[key] === needle) {
            return true;
          }
        }
      } else {
        for (key in haystack) {
          if (haystack[key] == needle) {
            return true;
          }
        }
      }

      return false;
    } ,

    checkBrowser : function()
    {
        var browser;
        jQuery.uaMatch = function (ua) {
            ua = ua.toLowerCase();

            var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
                /(webkit)[ \/]([\w.]+)/.exec(ua) ||
                /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
                /(msie) ([\w.]+)/.exec(ua) || ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];

            return {
                browser: match[1] || "",
                version: match[2] || "0"
            };
        };
        // Don't clobber any existing jQuery.browser in case it's different
        if (!jQuery.browser) {
            matched = jQuery.uaMatch(navigator.userAgent);
            browser = {};

            if (matched.browser) {
                browser[matched.browser] = true;
                browser.version = matched.version;
            }

            // Chrome is Webkit, but Webkit is also Safari.
            if (browser.chrome) {
                browser.webkit = true;
            } else if (browser.webkit) {
                browser.safari = true;
            }

            jQuery.browser = browser;
        }
        return browser;
    } ,

    getParamUrl : function(){
        var parts = window.location.search.substr(1).split("&");
        return parts;
    } ,

    jumpTo : function($target){
        offset = $target.offset();
        $('body,html').animate({ scrollTop: offset.top-200 }, 300);
    } ,

    /**
     * [messageNotication description]
     * @param  {[type]} type    [success , error]
     * @param  {[type]} message [ข้อความแสดงผล]
     */
    showMessage : function(type , message){
        if(typeof type === "undefined"){
            type = "error";
        }

         var title = (type === "error") ? "Error" : "Success";

        new PNotify({
              type : type ,
              title: title,
              text: message
        });
    } ,


    defaultColorbox : function(){
        if(jQuery().colorbox) {
            $('a.OpenEa').colorbox({closeButton:false, scrolling:false});
            $(".Openview-details").colorbox({inline:true, closeButton:false, href:'#Openview-details', width: 700, heihgt:500, scrolling:false});

            $('.lightbox-cancel').click(function(){
                $.colorbox.close();
            });
        }
    } ,

    number_format : function(number, decimals, dec_point, thousands_sep) {
        // *     example 1: number_format(1234.56);
        // *     returns 1: '1,235'
        // *     example 2: number_format(1234.56, 2, ',', ' ');
        // *     returns 2: '1 234,56'
        // *     example 3: number_format(1234.5678, 2, '.', '');
        // *     returns 3: '1234.57'
        // *     example 4: number_format(67, 2, ',', '.');
        // *     returns 4: '67,00'
        // *     example 5: number_format(1000);
        // *     returns 5: '1,000'
        // *     example 6: number_format(67.311, 2);
        // *     returns 6: '67.31'
        // *     example 7: number_format(1000.55, 1);
        // *     returns 7: '1,000.6'
        // *     example 8: number_format(67000, 5, ',', '.');
        // *     returns 8: '67.000,00000'
        // *     example 9: number_format(0.9, 0);
        // *     returns 9: '1'
        // *    example 10: number_format('1.20', 2);
        // *    returns 10: '1.20'
        // *    example 11: number_format('1.20', 4);
        // *    returns 11: '1.2000'
        // *    example 12: number_format('1.2000', 3);
        // *    returns 12: '1.200'
        // *    example 13: number_format('1 000,50', 2, '.', ' ');
        // *    returns 13: '100 050.00'
        // Strip all characters but numerical ones.
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function (n, prec) {
                        var k = Math.pow(10, prec);
                        return '' + Math.round(n * k) / k;
                };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    } ,

    capitalizeFirstLetter : function(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    } ,
    viewsJson : function(){
          console.log($.ea.getSelected());
    } ,

    clearValue : function($ele){
        $ele.find(':input').each(function() {
            if(!$(this).prop('readonly')) {
                switch(this.type) {
                    case 'password':
                    case 'select-multiple':
                    case 'select-one':
                    case 'text':
                    case 'textarea':
                        $(this).val('');
                        break;
                    case 'checkbox':
                    case 'radio':
                        this.checked = false;
                }
            }
        });
    }
};
