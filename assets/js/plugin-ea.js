$(function(){
	util.defaultColorbox();

    $(document).on('click' , '.showMore' , function(e){
        var $this = $(this);
        $detail =  $($this.attr('href'));
        $detail.stop().slideToggle('slow' , function(){
            if($detail.css('display') != 'none') $this.html("Less Info");
            else $this.html("More Info");
        });
        e.preventDefault();
    });

    $(document).on("click" ,".popUpElement" , function(e){
        var $this = $(this);
        $.magnificPopup.open({
          items: {
            src: $this.attr("href"),
            type: 'inline'
          }
        });
        e.preventDefault();
    });

    var $imageLink = $(".imageLink");
    $imageLink.magnificPopup({type:'image'});

    if(jQuery().formatter) {
        $('.phoneFormat').formatter({
          'pattern': '({{999}}) {{999}}-{{9999}}',
          'persistent': false
        });

        $('.creditFormat').formatter({
          'pattern': '{{9999}}-{{9999}}-{{9999}}-{{9999}}',
          'persistent': false
        });
    }


    $(document).on("click" ,".printThisPage" , function(e){
        e.preventDefault();
        var $this = $(this) ,
            $printHtml = $( $this.attr("href") ).clone(),
            $divPrint = $("<div id='divPrint'/>");

        $divPrint.html($printHtml);
        // $divPrint.printThis({
        //     printContainer : true
        // });
        $divPrint.print();
    });


});


