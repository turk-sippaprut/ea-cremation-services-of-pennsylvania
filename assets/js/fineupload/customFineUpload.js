var customFineUpload = {
    init : function(){
        return {
            autoUpload: false,
            maxConnections: 1,
            request: {
                endpoint: ''
            } ,
            validation: {
                allowedExtensions: ['jpeg', 'jpg' , 'gif' , 'png']
            },
            retry: {
                enableAuto: false
            },
            chunking: {
                enabled: false
            },
            text: {
                uploadButton: '<div class="btn-click-hear-photo"><i class="glyphicon glyphicon-plus"></i></div>'
            },
            fileTemplate:
            '<li class="col-md-12">' +
                '<div class="qq-progress-bar"></div>' +
                '<span class="qq-upload-spinner"></span>' +
                '<span class="qq-upload-finished"></span>' +
                '<span class="hide-file"></span>' +
                '<div class="qq-form-action">'+
                    '<span class="hide-size"></span>' +
                    '<a class="btn btn-default btn-sm qq-upload-cancel" href="#">{cancelButtonText}</a>' +
                '</div>'+
                '<a class="qq-upload-retry" href="#">{retryButtonText}</a>' +
                '<span class="qq-upload-status-text">{statusText}</span>' +
            '</li>',

            classes: {
                file: 'hide-file',
                size: 'hide-size'
            },
            template: '\
                 <div class="progressbar ">\
                    <ul class="qq-upload-list row-fluid" >\
                    </ul>\
                    <div class="clearfix"></div>\
                </div>\
                <div class="qq-uploader ">\
                    <pre class="qq-upload-drop-area col-md-12"><span>{dragZoneText}</span></pre>\
                    <div class="qq-upload-button btn-click-hear-photo" >{uploadButtonText}</div>\
                    <span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>\
                </div>'
               ,
            multiple: true
        };
    }
}