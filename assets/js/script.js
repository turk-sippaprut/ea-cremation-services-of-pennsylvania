$(function(){
	$("#mainform").validationEngine();

	//$('input[type=checkbox]').prettyOption('option-checkbox');
    //$('input[type=radio]').prettyOption('optionRadio');


    //View Detail
    $(".viewDetail").click(function() {
      var moreBtn = this;
      $($(this).attr('rel')).slideToggle("slow");
    });

});
function getTimeNumber(){
	var dt = new Date();
	return (dt.getTime()/1000000);
}
function updateSummary(){

	var subtotal = 0;
	for(var i in summary['each']){
		if(typeof(summary['each'][i]) != 'function'){
			subtotal += parseInt(summary['each'][i]);
		}
	}
	summary['subtotal'] = parseInt(subtotal);
	summary['tax'] = 0;
	if(summary['each'] && summary['each']['merchandise']){
		summary['tax'] = (summary['each']['merchandise'] * summary['taxrate'] / 100);
	}
	summary['subtotal2'] = (summary['subtotal'] + summary['tax']);
	summary['total'] = summary['subtotal2'];

	if(summary['has_ins_policy']){
		summary['total'] += summary['payment_policy'];
	}
}

/*
 * Function for validateion function call
 */
function checkZip(field, rules, i, options){
	if(field.val()){
		if(!IsNumeric(field.val())){
			return "* Zip Code is invalid.";
		}else if(field.val().length != 5){
			return "* Zip Code is invalid.";
		}
	}
}