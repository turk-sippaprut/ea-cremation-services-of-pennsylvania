$(function(){

    var $form       = $("#mainform") ;
    var pkgtype         = $("#pkgtype").val() ;
    var selected        = $("#selected").val() ;
    var $submit         = $(".btnSubmit") ;

    var $creditForm     = $(".creditForm") ;
    var $bilingForm     = $(".bilingForm") ;
    var $tiltePayment   = $(".tiltePayment") ;

    var $bllingLabel    = $(".bllingName");

    var $cardNumber     = $("#card_number") ;
    var $creditList     = $(".creditcardIcon li") ;
    var $paybyCash      = $("input[name='paybycash']") ;
    var $cart           = $(".cartList");

    var payment = {
        init : function(){

            $form.validationEngine('attach', {promptPosition : "topLeft", scroll: false} );
            this.startForm();
            this.events();
            $('.radio').prettyOption('optionRadio');
            $cart.caculator('view');
        } ,

        startForm : function(){
            if(selected != "" & selected.length > 0 ){
                //Load session to Element
                $form.jsonToElement({
                    json : JSON.parse( selected )
                });
                validate.addElement($form.find(".validate"));
            }
           //if(pkgtype == 'atneed') $(".atneed-required").addClass('validate[required]');
        } ,

        events : function(){

            $cardNumber.validateCreditCard(
                function(e) {
                    if (e.card_type == null) {
                        $creditList.removeClass("off");
                        $cardNumber.removeClass("valid");
                        return
                    }
                    $creditList.addClass("off");
                    $(".creditcardIcon ." + e.card_type.name).removeClass("off");
                    return e.length_valid && e.luhn_valid ? $cardNumber.addClass("valid") : $cardNumber.removeClass("valid");
                } ,
                {
                    accept: ['visa', 'mastercard' , 'discover'  , 'amex']
                }
            );

            $paybyCash.click(function() {
                var $this = $(this);

                if( $this.val() == "credit card" ){
                    $creditForm.removeClass('hide');
                    validate.addElement($creditForm.find(".validate"));

                    $bilingForm.removeClass('hide');
                    validate.addElement($bilingForm.find(".validate"));
                } else {
                    //Now pay now
                    $creditForm.removeClass('hide').addClass('hide');
                    validate.clearElement($creditForm.find(".validate"));
                    util.clearValue($creditForm);

                    $bilingForm.removeClass('hide').addClass('hide');
                    validate.clearElement($bilingForm.find(".validate"));
                    util.clearValue($bilingForm);
                }

            });


            $submit.click(function( e ){
                var $this = $(this);
                var $paycashChecked = $("input[name='paybycash']:checked");
                if(pkgtype == "atneed"){
                    if($paycashChecked.val() == "credit card" ){
                        if(!$cardNumber.hasClass("valid")){
                            $cardNumber.validationEngine('showPrompt', 'Credit card type is not supported' , "error" , "topLeft" , true);
                            return false;
                        }
                    }
                }
                var valid = $form.validationEngine('validate');
                if(valid){
                    $this.prop("disabled" , true).html('PROCESS');
                    $form.submit();
                }
                e.preventDefault();
            });
        } ,

    };

    payment.init();

});