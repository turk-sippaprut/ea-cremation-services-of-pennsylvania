$(function(){

    var $form       = $("#mainform") ,
    pkgtype         = $("#pkgtype").val() ,
    selected        = $("#selected").val() ,
    $urnIncluded    = $(".urns_included") ,
    $containerIncluded = $(".container_included") ,
    $submit         = $(".btnSubmit") ,
    $selectOption   = $(".selectOption") ,
    $selectWidth    = $(".select2__width") ,
    $cart           = $(".cartList");

    var sliderConfig    = {
        moveSlideQty: 1,
        hideControlOnEnd: true ,
        auto:false,
        controls:true,
        pause: 10000,
        speed: 1000,
        pager: true
    } ,
    $slider         = $(".ea-merch-slide") ,
    sliderBx = [];

    var merchandise = {

        init : function(){
            $form.validationEngine();
            this.events();
            this.startForm();
            this.installSlider();
            $selectWidth.select2();
            $selectOption.prettyOption('option-yes-no');
            $cart.caculator();

        } ,

        startForm : function(){
            if( selected == "" || selected.length == 0 ){
                this.setDefaultProduct( $containerIncluded );
                this.setDefaultProduct( $urnIncluded );
            }
        } ,

        events : function(){

            /**
             * Tab Title product
             */
            $(document).on("click" , ".tabsSelect" , function(){
                var $this = $(this);
                var $content = $this.closest('.ea-box-content').find(".ea-box__data");
                var $icon = $this.closest('.ea-box-content').find('.iconCaret');
                if ($content.hasClass('hide')){
                    $content.removeClass('hide');
                    $icon.removeClass('fa-caret-right').addClass('fa-caret-down');
                } else {
                    $content.removeClass('hide').addClass('hide');
                    $icon.removeClass('fa-caret-down').addClass('fa-caret-right');
                }

                if ( typeof sliderBx[ $(this).data("key") ] !== "undefined") {
                    sliderBx[ $(this).data("key") ].reloadSlider();
                }
                
                $('.js-example-basic-single').select2();
            });

            /**
             * Add to Cart
             */
            $(document).on("click" , ".cartAdd" , function(e){
                var $this = $(this);

                if(!$this.hasClass("included")){
                    //Clear Included
                    if( $this.data('group') == "urns" ){
                       merchandise.removeProduct( $urnIncluded );
                    } else if( $this.data('group') == "container" ){
                        merchandise.removeProduct( $containerIncluded );
                        $this.closest(".ea-merch-product").find(".select-item").each(function(){
                            merchandise.removeProduct( $(this) );
                        });
                    }
                    if($this.data("hasquan") == 1)
                        $this.data("quan" , parseInt($this.closest('.productSection').find(".cartQuan").val()) );
                    else
                        $this.data("quan" , 1);
                    merchandise.selectedProduct( $this );
                } else{
                    $this.closest(".ea-merch-product").find(".select-item").each(function(){
                        merchandise.removeProduct( $(this) );
                    });
                    merchandise.setDefaultProduct( $this );
                }
                $cart.caculator();
            });

            /**
             * Remove Cart
             */
            $(document).on("click" , ".cartRemove" , function(e){
                var $this = $(this);
                var data;
                var method = {
                    'urns' : $urnIncluded ,
                    'container' : $containerIncluded
                };
                merchandise.removeProduct( $this );
                if($this.data("group") == "urns" ||  $this.data("group") == "container"){
                    data = $.ea.getSelected();
                    if(typeof data[$this.data("group")]  === 'undefined' || _.size(data[$this.data("group")]) == 0 ){
                        merchandise.setDefaultProduct( method[ $this.data("group") ] );
                    }
                }
                if($this.data("hasquan") == 1)
                    $this.closest('.productSection').find(".cartQuan").select2("val" , 1);

                $cart.caculator();
            });

            /**
             * Select Quan cart
             */
            $(document).on("change" , ".cartQuan" , function(){
                var $this = $(this);
                var $button = $this.closest(".productSection").find(".select-item");

                $button.removeClass("button-selected")
                 .removeClass("cartRemove")
                 .removeClass("cartAdd")
                 .addClass("cartAdd")
                 .html("SELECT ITEM");
            });

            //Select ADDITIONAL MERCHANDISE
            $selectOption.click(function(){
                var $this = $(this) ;
                if( $this.is(':checked') ){
                    if($this.data("id") == 13){
                        $this.data("keywidth" , $this.closest(".ea-select-style").find(".select2__width").val());
                        $this.data("name" ,  $this.data('title') + " " + $this.closest(".ea-select-style")
                             .find("#option-width").find(":selected").data('title'));
                    }
                    merchandise.selectedOption( $this );
                } else{
                    merchandise.removeOption( $this );
                }
                $cart.caculator();
            });

            $(document).on("change" , "#option-width" , function(){
                var $this = $(this);
                var data;
                var obj = {};
                var $option = $this.closest(".ea-select-style").find(".selectOption");
                if($option.is(":checked")){
                    data = $.ea.getSelected();
                    obj[ $option.data('id') ] = {
                        id : $option.data('id') ,
                        name : $option.data('title') + " " + $this.find(":selected").data("title") ,
                        group : $option.data('group') ,
                        quan : 1 ,
                        keywidth : $this.val()
                    };
                    data[ $option.data('group') ] = _.merge(data[ $option.data('group') ] , obj);
                    $.ea.setSelected(data);
                    $cart.caculator();
                }
            });

            $submit.click(function( e ){
                var $this  = $(this);
                var data   = $.ea.getSelected();
                $form.submit();
                e.preventDefault();
            });
        } ,
        installSlider : function(){
            $slider.each(function(){
                sliderBx[ $(this).data("key") ] =  $(this).bxSlider( sliderConfig );
            });
        } ,

        setDefaultProduct : function( $this ){
            $this.data("quan" , 1);
            this.selectedProduct( $this );
        } ,

        selectedProduct : function( $this ){
            $this.removeClass("button-selected").addClass("button-selected").addOption();
            if($this.hasClass("included")){
                $this.html("ITEM SELECTED").removeClass("cartAdd").removeClass("cartRemove");
            } else{
                $this.html("REMOVE ITEM").removeClass("cartAdd").addClass("cartRemove");
            }
        } ,

        removeProduct : function( $this ){
            $this.removeClass("button-selected")
             .removeClass("cartRemove")
             .addClass("cartAdd")
             .html("SELECT ITEM")
             .removeOption();
        } ,

        /**
         * This function for  Add/remove service
         */
        selectedOption : function( $this ){
            $this.parent().removeClass('option-yes').addClass('option-yes').end().attr("checked" , true).addOption();
        } ,

        removeOption : function( $this ){
            $this.parent().removeClass('option-yes').end().removeAttr('checked').removeOption();
        }



    };
    merchandise.init();
});