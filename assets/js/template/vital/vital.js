$(function(){

    var $form          = $("#mainform") ;
    var $formFamily        = $('#form-family') ;
    var pkgtype            = $("#pkgtype").val() ;
    var $selected          = $("#selected") ;
    var selected           = $selected.val() ;
    var $submit            = $(".btnSubmit") ;
    var $boxmask           = $("#boxmask") ;
    var $biographicalForms = $(".biographicalForms") ;
    var $sectionMilitary   = $(".military-service-detail") ;
    var $sectionObit       = $(".complete-obituary") ;
    var $photoUpload       = $(".fineUpload__photo") ;
    var $cart              = $(".cartList");

    var vital = {
        init : function(){

            $form.validationEngine('attach', {promptPosition : "topLeft", scroll: false} );
            $formFamily.validationEngine( 'attach', {promptPosition : "topLeft", scroll: false} );
            $('.choiceForm,.radio').prettyOption('optionRadio');
            this.startForm();
            this.events();
            $cart.caculator('view');

            //$("#dateSign").val( moment(new Date()).format('MMMM D, YYYY')  );
            //$("#timeSign").val( moment(new Date()).format('h:mm a') );

        } ,

        startForm : function(){
            if(selected != "" & selected.length > 0 ){
                //Load session to Element
                $form.jsonToElement({
                    json : JSON.parse( $selected.val() )
                });

                var session = $.ea.getSelected();
               
                if( session.military == "Yes" ) {
                    validate.addElement( $sectionMilitary.find(".military-required") );
                }

                if (session.pod) {
                    this.placeDeathAction(session.pod);
                }

                if (session.decedentVeteran) {
                   this.decedentVeteranAction(session.decedentVeteran);
                }

                if (session.hispanic_origin) {
                    this.hispanicOriginAction(session.hispanic_origin);
                }

                if (session.race) {
                    this.raceAction(session.race);
                }
            }
        } ,

        events : function(){

            $(document).on("change" , "#marital" , function(){
                var $this = $(this) ;
                var $spouseName = $("input[name='spouse_name']");

                var requireStatus = ['Married' , 'Separated'];

                var checkData = _.find( requireStatus , (v) =>{
                    return v === $this.val();
                });

                if ( typeof checkData === "undefined") {
                    validate.clearElement( $spouseName );
                }
                else {
                    validate.addElement( $spouseName );
                }
            });

            $(document).on("click" , 'input[name="pod"]' , function(){
                let $this = $(this);
                vital.placeDeathAction($this.val());
            });

            $(document).on("click" , "input[name='decedentVeteran']" , function(){
                let $this = $(this);
                vital.decedentVeteranAction($this.val());
            });

            $(document).on("click" , "input[name='hispanic_origin']" , function(){
                let $this = $(this);
                vital.hispanicOriginAction($this.val());
            });

            $(document).on("click" , "input[name='race']" , function(){
                let $this = $(this);
                vital.raceAction($this.val());
            });

            $(document).on("click" , ".btnSubmit" , function(e){
                e.preventDefault();
                var $this = $(this) ,
                $choice = $('input[name="choiceForm"]') 
                $form.submit();
            });
        } ,

        placeDeathAction : function(value) {
            let $otherPlace = $("input[name='other_pob']");
            if ( value === 'Other') {
                validate.addElement( $otherPlace );
            } else {
                validate.clearElement($otherPlace);
            }
        },

        decedentVeteranAction: function(value) {
            let $branch = $("input[name='decedent_veteran_branch']");
            if ( value === 'Yes') {
                validate.addElement( $branch );
            } else {
                validate.clearElement($branch);
            }
        },

        hispanicOriginAction: function(value) {
            let $hispanic_origin_other = $("input[name='hispanic_origin_other']");
            if ( value === 'other') {
                validate.addElement( $hispanic_origin_other );
            } else {
                validate.clearElement($hispanic_origin_other);
            }
        },

        raceAction: function(value) {
            let $race_other = $("input[name='race_other']");
            if ( value === 'other') {
                validate.addElement( $race_other );
            } else {
                validate.clearElement($race_other);
            }
        }

    }

    vital.init();

});