
$(function(){

    var $form       = $("#mainform") ,
    pkgtype         = $("#pkgtype").val() ,
    zoneCounties    = $("#zoneCounties").val() ,
    selected        = $("#selected").val() ,
    $errorPackage   = $(".errorPackage") ,
    $submit         = $(".btnSubmit") ,
    $selectOption   = $(".selectOption") ,
    $includedOption = $(".includedOption") ,
    $autoSelectedOption = $(".options-auto-selected"),
    $handleLancaster = $("#selectOption-4") ,

    $counties       = $("#counties") ,
    $tranPrice      = $(".transport1__price") ,
    $tranOption     = $("#tranOption") ,
    $deathCerti     = $("#deathCerti") ,
    $deathOccur     = $("#death_occur") ,
    $cart           = $(".cartList");

    var packages = {

        init : function(){
            $form.validationEngine( );
            this.events();
            this.startForm();

            $('.js-example-basic-single').select2();
            $selectOption.prettyOption('option-yes-no');
            $cart.caculator();

        } ,

        startForm : function(){

            // Add 1 DCC for both Preneed and Atneed
            $deathCerti.data('quan' , $deathCerti.val() ).addOption();

            if( selected == "" || selected.length == 0 ){
                this.setDefaultOption();
                if(pkgtype == "atneed"){
                    // $deathCerti.data('quan' , $deathCerti.val() ).addOption();
                    this.selectedCountry( $counties );
                }

                $autoSelectedOption.each(function(){
                     packages.selectedOption( $(this) );
                });

            }
            var data = $.ea.getSelected();
            var counties = _.keys(data.counties)[0];
            if( !this.checkDeliveryZone( counties ) ) $handleLancaster.attr("disabled" , true);
            this.changePriceDelivery();
        } ,

        events : function(){

            $(document).on('click' , '.show-package' , function(e){
                var $this = $(this) ,
                $content = $("#ea-recommended-plan");
                $content.slideToggle('slow');
            });

            $(document).on("click" , ".select-package" , function(){
                var $this = $(this) ,
                    data  = [] ,
                    $selectPackage = $(".select-package");
                if($this.hasClass('button-selected')){
                    packages.removePackage( $this );
                    // data = _.omit(data, $this.data('group') );
                    // $.ea.setSelected(data);
                }
                else{
                    packages.removePackage( $('.button-selected') );
                    packages.selectedPackage( $this );
                }
                $cart.caculator();
            });

            $(document).on('change' , '#counties' , function(){
                var $this = $(this) ;
                packages.selectedCountry( $this );
                $cart.caculator();
            });

            $(document).on("change" , "#deathCerti" , function(){
                var $this = $(this);
                var data;
                var obj = {};
                $this.removeOption();
                if( $this.val() > 0){
                    data = $.ea.getSelected();
                    obj[ $this.data('id') ] = {
                        id : $this.data('id') ,
                        name : $this.data('name') ,
                        group : $this.data('group') ,
                        quan : $this.val()
                    };
                    data[ $this.data('group') ] = _.merge(data[ $this.data('group') ] , obj);
                    $.ea.setSelected(data);
                }
                 $cart.caculator();
            });

            $(document).on("change" , "#death_occur" , function(){
                var $this = $(this);
                var data;
                var obj = {};
                $this.removeOption();
                if ($this.val() != "" ){
                    data = $.ea.getSelected();
                    obj[ $this.data('id') ] = {
                        id : $this.data('id') ,
                        name : $this.data('name') ,
                        group : $this.data('group') ,
                        choice : $this.val()
                    };
                    data[ $this.data('group') ] = _.merge(data[ $this.data('group') ] , obj);
                    $.ea.setSelected(data);
                }
                $cart.caculator();
            });

            //Select Handling or Plan addtional
            $selectOption.click(function(){
                var $this = $(this) ,
                method = {
                    handling : 'selectedHandling' ,
                    addition : 'selectedAddition'
                } ,
                fn = method[ $this.data( 'group' ) ];
                if( typeof packages[ fn ] === 'function' )
                    packages[ fn ]( $this );
                $cart.caculator();
            });

            $submit.click(function( e ){
                var $this  = $(this);
                var data   = $.ea.getSelected();
                if(typeof data['package'] === 'undefined' || _.size(data['package']) == 0 ){
                    $errorPackage.validationEngine('showPrompt', 'Please select a package');
                    $('body,html').animate({ scrollTop: $errorPackage.offset().top-200 }, 500);
                    return false;
                }
                if ( pkgtype == "atneed" ){
                    if ( $deathOccur.val() == "" ) {
                        $deathOccur.closest('.ea-options-list-area').find('.errorOption').validationEngine('showPrompt', 'Please select a Death occur');
                        $('body,html').animate({ scrollTop: $deathOccur.offset().top-400 }, 500);
                        return false;
                    }
                }
                $form.submit();
                e.preventDefault();
            });


        } ,


        /**
         * this function for add package
         */
        selectedPackage : function( $this ){
            $this.removeClass('button-selected').html('PLAN SELECTED').addClass('button-selected').addOption();
        } ,

        removePackage : function( $this ){
            $this.removeClass('button-selected').html('SELECT PLAN').removeOption();
        } ,

        /**
         * this function for selected Country
         */
        selectedCountry : function( $this ){
            var data = $.ea.getSelected();
            data = _.omit( data , $this.data('group') ); //delete old data
            var obj = {};
            obj[ $this.val() ] =  {  id : $this.val() , group : $this.data("group") };
            data[ $this.data("group") ] = obj;
            $.ea.setSelected(data);
            $tranPrice.html( "$" + util.number_format( $this.find(":selected").data('rate') , 2 ) );
            $tranOption.addOption();

            if( !this.checkDeliveryZone( $this.val() ) ) {
                if($this.val() == ""){
                    $tranPrice.html('$0');
                    $tranOption.removeOption();
                }
                $handleLancaster.attr("disabled" , true);
                this.removeOption( $handleLancaster );

                data = $.ea.getSelected();
                if(typeof data['handling'] === 'undefined' || _.size(data['handling']) == 0 ) this.setDefaultOption();
            }
            else {
                $handleLancaster.removeAttr("disabled");
            }

            this.changePriceDelivery();

        } ,

        changePriceDelivery() {
            if ($counties.find(":selected").data('deliverprice')) {
                var price = "$" + util.number_format($counties.find(":selected").data('deliverprice'), 2);
            } else {
                var price = "$" + util.number_format($handleLancaster.data('showprice'), 2);
            }
            $handleLancaster.closest('.ea-options-list-area').find('.price-text').html(price);
        },

        /**
         * Check Country for Hand Delivery of Cremated Remains option
         */
        checkDeliveryZone : function( country ){
            var zone = zoneCounties.split(',');
            if ( util.in_array( country , zone ) ) return true;
            else return false;
        } ,

        /**
         * Manage Handling
         */
        selectedHandling : function( $this ){
            if( $this.is(':checked') ){
                $this.closest('.optionSection').find('.selectOption:checked').each(function(){
                    packages.removeOption( $(this) );
                });
                this.selectedOption( $this );
            } else{
                this.removeOption( $this );
            }
            if( $("input[name='" + $this.data('group') + "_select']:checked").length == 0 )
                this.setDefaultOption();
        } ,

        /**
         * Manage Addition option
         */
        selectedAddition : function( $this ){
            if( $this.is(':checked') )
                this.selectedOption( $this );
            else
                this.removeOption( $this );
        } ,

        /**
         * This function for set service has class includeOption and set default
         */
        setDefaultOption : function(){
            $includedOption.each(function(){
                packages.selectedOption( $(this) );
            });
        } ,

        /**
         * This function for  Add/remove service
         */
        selectedOption : function( $this ){
            $this.parent().removeClass('option-yes').addClass('option-yes').end().attr("checked" , true).addOption();
        } ,

        removeOption : function( $this ){
            $this.parent().removeClass('option-yes').end().removeAttr('checked').removeOption();
        }
    };

    packages.init();

});
