$(function(){

    var $form  = $("#mainform") ,
    pkgtype            = $("#pkgtype").val() ,
    $selected          = $("#selected") ,
    selected           = $selected.val() ,
    $cart              = $(".cartList") ,
    $checkAnotherSign  = $("input[name='signature_other']") ,
    $submit            = $(".btnSubmit") ;


    var authorization = {

        init : function(){
            this.startForm();
            this.events();
            $cart.caculator('view');

            $form.validationEngine('attach', {promptPosition : "topLeft", scroll: false} );
            $('.radio').prettyOption('optionRadio');
            $('.checkbox').prettyOption('check-box-form');

            // $("#electironic_date").val( moment(new Date()).format('MMMM D, YYYY')  );
            // $("#electironic_time").val( moment(new Date()).format('h:mm a') );

            // var timeZone = jstz.determine();
            // var timeZoneName = timeZone.name();
            // if ( timeZoneName == 'Asia/Bangkok')
            // {
            //     timeZoneName = 'America/New_York';
            // }
            // console.log( moment().tz(timeZoneName).format('LT'));


        } ,

        startForm : function(){
            if(selected != "" & selected.length > 0 ){
                //Load session to Element
                $form.jsonToElement({
                    json : JSON.parse( $selected.val() )
                });
            }
        } ,

        events : function(){

            $(document).on("click" , ".view-info" , function(e){
                var $this = $(this);
                $.magnificPopup.open({
                  items: {
                    src: $this.attr("href") ,
                    type: 'ajax'
                  }
                });
                e.preventDefault();
            });

            $(document).on("blur" , ".initial" , function(){
                var $this = $(this);
                //var value = $this.val();
                var $init = $(".initial");
                var $containerInit = $(".containerInit");
                var emptyArr = [];
                if($this.val() != ""){
                    validate.clearElement( $containerInit.find('.radio') );
                    $init.each(function(){
                        if($.trim($(this).val()) != ''){
                            validate.addElement( $this.closest(".row").find('.radio') );
                        }
                    });
                }
                else validate.clearElement( $containerInit.find('.radio') );

            });

            $(document).on("blur" , "input[name='init_8_radio']" , function(){
                var $this = $(this);
                var $otherInput = $("input[name='init_8_inputYes']");
                if( $this.val() == "Yes") validate.addElement( $otherInput );
                else validate.clearElement( $otherInput );
            });

            $(document).on("blur" , "input[name='init_11_radio']" , function(){
                var $this = $(this);
                var $otherInput = $("input[name='init_11_inputYes']");
                if( $this.val() == "Yes") validate.addElement( $otherInput );
                else validate.clearElement( $otherInput );
            });

            /**
             * Make radio box if checked then it will show another signature
             */
            $checkAnotherSign.click(function(){
                var $this = $(this);
                var $signAnother = $("#signature-other-detail");

                $signAnother.removeClass("hide");
                if($this.is(':checked')){
                  validate.addElement( $signAnother.find(".validate") );
                }
                else{
                    $signAnother.addClass("hide");
                    validate.clearElement( $signAnother.find(".validate") );
                }
            });

            $(document).on("click" , ".btnSubmit" , function(e){
                e.preventDefault();
                var $this = $(this) ,
                $signature = $("#signature") ,
                $enterSignature = $("#enterSignature");

                if( $signature.val() != $enterSignature.val()  ){
                    $signature.validationEngine('showPrompt', 'Signature and Enter Name do not match' , "error" , "topLeft" , true);
                    $enterSignature.validationEngine('showPrompt', 'Signature and Enter Name do not match' , "error" , "topLeft" , true);
                    return false;
                }

                if( $checkAnotherSign.is(":checked") ){
                    var $signature_another = $("input[name='signature_another'");
                    var $entersignature_another = $("input[name='entersignature_another'");
                    if( $signature_another.val() != $entersignature_another.val()  ){
                        $signature_another
                            .validationEngine('showPrompt', 'Signature and Enter Name do not match' , "error" , "topLeft" , true);
                        $entersignature_another
                            .validationEngine('showPrompt', 'Signature and Enter Name do not match' , "error" , "topLeft" , true);
                        return false;
                    }
                }
                $form.submit();
            });

        }
    }

    authorization.init();

});