$(function(){

    var $form  = $("#mainform") ,
    $cart              = $(".cartList") ,
    $formFamily        = $("#form-family");

    var thankyou = {

        init : function(){
            this.events();
            $cart.caculator('view');
        } ,
        events : function(){
            $formFamily.submit(function(e){
                var $btn = $formFamily.find(".btn-submit-email");
                if( validate.sytaxEmail( $formFamily.find('#family_email') ) ){
                    $btn.prop("disabled" , true).val("Processing");
                    $.ajax({
                        url : root + "proccess/sendtoFamily" ,
                        data : $formFamily.serialize(),
                        type : "POST" ,
                        cache: false,
                        contentType:"application/x-www-form-urlencoded; charset=utf-8",
                        dataType:"json" ,
                        success : function(data){
                            $btn.prop("disabled" , false).val("Submit");
                            util.showMessage(data.pnotify['type'] , data.pnotify['text']);
                            if(data.result == "success"){
                                $("#family_email").val("");
                            }
                        }
                    });
                }
                e.preventDefault();
            });
        }
    }

    thankyou.init();

});