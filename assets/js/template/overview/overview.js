$(function(){

    var $form       = $("#mainform") ,
    pkgtype         = $("#pkgtype").val() ,
    selected        = $("#selected").val() ,
    $selectOption   = $(".selectOption") ,
    $submit         = $(".btnSubmit") ,
    $cart           = $(".cartList");

    var overview = {
        init : function(){
            $(".js-example-basic-single").select2();
            $selectOption.prettyOption('option-yes-no');

            $cart.caculator('view');
        } ,

        events : function(){
        }
    }

    overview.init();

});