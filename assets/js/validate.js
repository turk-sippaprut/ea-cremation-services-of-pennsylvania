
var validate = {
    clearElement : function($input){
        $input.each(function(){
            var $this = $(this);
            if(!$this.hasData('validate')){
                $this
                    .removeClass('validate[required]')
                    .validationEngine('hide')
                    .validationEngine('detach');
            } else{
                $this
                    .removeClass($this.data('validate'))
                    .validationEngine('hide')
                    .validationEngine('detach');
            }

        });
    } ,

    addElement : function($input){
        $input.each(function(){
            var $this = $(this);
            if(!$this.hasData('validate')){
                $this
                    .removeClass('validate[required]')
                    .addClass('validate[required]');
            } else{
                $this
                    .removeClass($this.data('validate'))
                    .addClass($this.data('validate'));
            }
        });
    } ,

    checkCreditCard : function($cardNumber){
        if($cardNumber.val() == ""){
            $cardNumber.validationEngine('showPrompt', 'This field is required' , "error" , "topRight" , true);
            return false;
        }
        if(!$cardNumber.hasClass("valid")){
            $cardNumber.validationEngine('showPrompt', 'Credit card type is not supported' , "error" , "topRight" , true);
            return false;
        }
        return true;
    } ,

    sytaxEmail : function( $ele ){
        var email = $ele.val() ,
        filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
        if(email.length==0){
            $ele.validationEngine('showPrompt', '*This field is required' , "error" , "topLeft" , true);
            return false;
        }
        if(!filter.test(email)){
            $ele.validationEngine('showPrompt', '*Invalid email address' , "error" , "topLeft" , true);
            return false;
        }
        return true;
    }


};