(function($,document,window){$.fn['ea']=$['ea']=function(options){$.ea.selected=new Object();for(var i in options){$.ea[i]=options[i]||false;}
$.ea.createMainForm=function(formAttrs){if($('#mainform').length){return this;}else{var form=document.createElement('form');$(form).attr('id','mainform').attr('method','post');if(typeof(formAttrs)!='undefined'){if(formAttrs['action']){$(form).attr('action',formAttrs['action']);}
if(formAttrs['method']){$(form).attr('method',formAttrs['method']);}
if(formAttrs['style']){$(form).attr('style',formAttrs['style']);}
if(formAttrs['class']){$(form).addClass(formAttrs['class']);}}
$(form).hide();var scriptaction=document.createElement('input');$(scriptaction).attr('type','hidden').attr('id','scriptaction').attr('name','scriptaction').attr('value','validate');$(form).append(scriptaction);var next_form=document.createElement('input');$(next_form).attr('type','hidden').attr('id','next_form').attr('name','next_form').attr('value',$.ea.next_form);$(form).append(next_form);var selected=document.createElement('input');$(selected).attr('type','hidden').attr('id','selected').attr('name','selected').attr('value','');$(form).append(selected);$('body').append(form);return this;}}
$.ea.getMainForm=function(){if(!$('#mainform').length){$.ea.createMainForm();}
return $('#mainform');}
$.ea.isJSON=function(jsoncode){try{JSON.parse(jsoncode);return true;}catch(e){return false;}}
$.ea.makeObject=function(variable){if(typeof(variable)=='undefined'){variable=new Object();}
return variable;}
$.ea.goNext=function(){var form=$.ea.getMainForm();$('#next_form',form).val($.ea.next_form);form.submit();}
$.ea.goBack=function(){var form=$.ea.getMainForm();form.validationEngine('detach');$('#next_form',form).val($.ea.prev_form);form.submit();}
$.ea.goTo=function(target){var form=$.ea.getMainForm();$('#next_form',form).val(target);form.submit();}
$.ea.getSelected=function(){var form=$.ea.getMainForm();var selected=$('input#selected',form).val();if($.ea.isJSON(selected)){selected=JSON.parse(selected);}else{selected=new Object();}
return selected;}
$.ea.setSelected=function(selected){var form=$.ea.getMainForm();$('input#selected',form).val(JSON.stringify(selected));}
$.ea.addOption=function(params){var item={group:false}
try{for(var i in params){item[i]=params[i];}
if(!item.id){ }
if(!item.group){  throw'Option need a GROUP of record.';}
if(typeof(params.onBefore)=='function'){params.onBefore(item);}
var selected=$.ea.getSelected();selected[item.group]=$.ea.makeObject(selected[item.group]);if(item.id){selected[item.group][item.id]=item;}else{selected[item.group]=item;}
$.ea.setSelected(selected);if(typeof(params.onAfter)=='function'){params.onAfter(item,selected);}
return this;}catch(err){return err;}}
$.ea.removeOption=function(params){try{if(typeof(params.id)=='function'||!params.id){}
if(typeof(params.group)=='function'||!params.group){throw'Option need a GROUP of record to remove.';}
if(typeof(params.onBefore)=='function'){params.onBefore(params);}
var selected=$.ea.getSelected();selected[params.group]=$.ea.makeObject(selected[params.group]);if(params.id){delete selected[params.group][params.id];}else{delete selected[params.group];}
$.ea.setSelected(selected);if(typeof(params.onAfter)=='function'){params.onAfter(params,selected);}
return this;}catch(err){return err;}}
$.ea.addToCart=function(params){var item={id:false,group:false,price:0,quan:1}
try{for(var i in params){item[i]=params[i];}
if(!item.id){throw'Cart need an ID of product.';}
if(!item.group){throw'Cart need a GROUP of product.';}
if(typeof(params.onBefore)=='function'){params.onBefore(item);}
var selected=$.ea.getSelected();selected[item.group]=$.ea.makeObject(selected[item.group]);selected[item.group][item.id]=item;$.ea.setSelected(selected);if(typeof(params.onAfter)=='function'){params.onAfter(item,selected);}
return this;}catch(err){return err;}}
$.ea.removeFromCart=function(params){try{if(typeof(params.id)=='function'||!params.id){if(typeof(params.group)=='function'||!params.group){throw'Cart need an ID or GROUP of product to remove.';}}
if(typeof(params.onBefore)=='function'){params.onBefore(params);}
var selected=$.ea.getSelected();if(params.id){if(params.group){for(var group in selected){if(params.group==group){for(var id in selected[group]){if(params.id==id){delete selected[group][id];}}}}}else{for(var group in selected){for(var id in selected[group]){if(params.id==id){delete selected[group][id];}}}}}else{delete selected[group];}
$.ea.setSelected(selected);if(typeof(params.onAfter)=='function'){params.onAfter(params,selected);}
return this;}catch(err){return err;}}
$.fn.addOption=function(){var item={}
try{for(var i in this.data()){if(typeof(this.data()[i])!='function'){item[i]=this.data()[i];}}
var res=$.ea.addOption(item);if(typeof(res)=='string'){throw res;}
return this;}catch(err){return err;}}
$.fn.removeOption=function(){var item={}
try{for(var i in this.data()){if(typeof(this.data()[i])!='function'){item[i]=this.data()[i];}}
var res=$.ea.removeOption(item);if(typeof(res)=='string'){throw res;}
return this;}catch(err){return err;}}
$.fn.addToCart=function(quantity){var item={}
try{for(var i in this.data()){if(typeof(this.data()[i])!='function'){item[i]=this.data()[i];}}
if(quantity){item['quan']=quantity;}
var res=$.ea.addToCart(item);if(typeof(res)=='string'){throw res;}
return this;}catch(err){return err;}}
$.fn.removeFromCart=function(){var item={}
try{if(this.data('id')){item['id']=this.data('id');}
var res=$.ea.removeFromCart(item);if(typeof(res)=='string'){throw res;}
return this;}catch(err){return err;}}}
$.ea();}(jQuery,document,window));