$(function() {

    var $sidebar     = $(".sidebarfix"),
        $window      = $(window),
        $content     = $("#content") ,
        // $innerbox = $("#innerbox-int-ea");
        offset       = $("#innerbox-int-ea").offset(),
        endPos    = $(".bottom-page").offset().top - $sidebar.innerHeight(true) - 100,
        sidbarWidth = $sidebar.innerHeight(),
        startTop     = offset.top,
        screenContent = $("#content").height(),
        screenHeight = startTop + screenContent,

        height       = $(".sidebarfix").innerHeight(true);

    $window.scroll(function() {

        if ($window.scrollTop() > offset.top) {
            $(".ea-callbox-step").hide();
            $sidebar.css({ top : "100px" });

            if($window.scrollTop() >= screenContent ){
                //$sidebar.css({ position : "absolute" , bottom: '0px' , top: $window.scrollTop() - height });
                $sidebar.css({ top : "30px" });
            } else{
                $sidebar.css({ position : "fixed" , bottom: '' , top:'90px' });
            }
        } else {
            $sidebar.css({ position : "fixed" , top : "145px" , bottom: ''});
             $(".ea-callbox-step").show();

        }
    });


});