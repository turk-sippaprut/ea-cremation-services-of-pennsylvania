<style type="text/css">
    .ea-content input[type="checkbox"] {
        display: none;
    }
/*    .optionCheck {
        width: 28px;
        height: 28px;
        background: #fff;
        border: 2px solid blue;
        position: relative;
        display: inline-block;
    }*/
    .optionCheck input[type=checkbox]:not(:checked) + label:before {
        content: "";
        border: 2px solid #3f819f;
        height: 22px;
        width: 21px;
        background: #fff;
        display: inline-block;
        margin: 0 10px 0px 0;
    }
    .optionCheck input[type=checkbox]:checked + label:before {
        content: "";
        border: 2px solid #3f819f;
        height: 22px;
        width: 20px;
        background: #003555;
        display: inline-block;
        margin: 0 10px 0px 0;
    }


</style>

<div class="col-sm-7 col-md-7">
    <form action="" id="mainform" name="mainform" method="POST">
        <input type="hidden" name="pkgtype"             id="pkgtype"        value="<?php echo $user_session['pkgtype'];?>" />
        <input type="hidden" name="scriptaction"        id="scriptaction"   value="validate" />
        <input type="hidden" name="notvalidate"         id="notvalidate"    value="" />
        <input type="hidden" name="next_form"           id="next_form"      value="<?php echo $next_form?>" />
        <input type="hidden" name="session_index"       id="session_index"  value="<?php echo $session_index?>" />
        <input type="hidden" name="selected"            id="selected"       value="<?php echo $$session_index;?>" />


    <div class="ea-content">
        <div id="biographical-information-form-area" class="biographicalForms">
            <div style="padding-top:10px;" class="formGroupTitle">
                Biographical Information
            </div>
            <div class="text-plan-merch">All fields below are required to file a death certificate. If you do not know the information and cannot obtain it, please put ‘NA’ in the required field to move on.</div>
            <div class="eaFormBox">
                <?php
                $titleFullName = "Full Name of Deceased";
                if($user_session['pkgtype'] == "preneed"){
                    $titleFullName = "Whom are the arrangements for (enter name)?";
                }
                ?>
                <?php echo $titleFullName; ?>

                <div class="eaFormField">
                    <input type="text" name="deceased_first" class="text validate[required]" value="">
                    <span class="text-bottom-input">(First)</span><br />

                    <input type="text" name="deceased_middle" class="text" value="">
                    <span class="text-bottom-input">(Middle)</span><br />

                    <input type="text" name="deceased_last" class="text validate[required]" value="">
                    <span class="text-bottom-input">(Last)</span>
                </div>
            </div>
            <div class="eaFormBox">
                Street Address:
                <div class="eaFormField">
                    <input type="text" name="deceased_address" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                City/Town:
                <div class="eaFormField">
                    <input type="text" name="deceased_city" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                Twp:
                <div class="eaFormField">
                    <input type="text" name="deceased_county" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                State:
                <div class="eaFormField">
                    <span class="payment-options">
                        <select name="address_state" class="input-dropdown validate[required]">
                            <option value="" >Select State</option>
                            <?php foreach($states as $state => $arr){ ?>
                                <option  value="<?php echo $state;?>"><?php echo $state;?></option>
                            <?php } ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="eaFormBox">
                Zip Code:
                <div class="eaFormField">
                    <input type="text" name="deceased_zip" class="text validate[required]">
                </div>
            </div>

            <div class="eaFormBox">
                Telephone:
                <div class="eaFormField">
                    <input type="text" name="deceased_phone" class="text validate[required]">
                </div>
            </div>

            <div class="eaFormBox">
                Social Security Number:
                <div class="eaFormField">
                    <input type="text" name="ssn" class="text ">
                </div>
            </div>

            <div class="eaFormBox">
                Date of Birth:
                <div class="eaFormField">
                        <span class="payment-options col-xs-5">
                        <select style="width: 98%;" id="mob" name="mob" class="validate[required]">
                            <option  value="">Month</option>
                            <?php foreach($cfg['month'] as $monthNumber => $month){?>
                                <option value="<?php echo $month;?>"><?php echo $month;?></option>
                            <?php } ?>
                        </select>
                        </span> <span class="payment-options col-xs-3">
                        <select style="width: 97%;" id="dob" name="dob" class="validate[required]">
                            <option value="">Day</option>
                            <?php for($day= 1; $day<= 31; $day++){
                                $day = substr($day, -2);
                            ?>
                            <option  value="<?php echo $day;?>"><?php echo $day;?></option>
                            <?php } ?>
                        </select>
                        </span> <span class="payment-options col-xs-4">
                        <select style="width: 98%;" id="yob" name="yob" class="validate[required]">
                            <option  value="">Year</option>
                            <?php
                                $currentYear = date("Y");
                                for($year = $currentYear; $year >= 1900; $year-- ){
                            ?>
                                    <option  value="<?php echo $year;?>"><?php echo $year;?></option>
                            <?php } ?>
                        </select>
                        </span>
                    </div>
            </div>

            <?php if($user_session['pkgtype'] == "atneed"){ ?>
            <div class="eaFormBox">
                Date of Death
                <div class="eaFormField">
                    <span class="payment-options col-xs-5" >
                        <select  id="month_of_death" name="mod" data-prompt-position="topLeft" readonly class="validate[required] col-md-12">
                            <option value="">Month</option>
                            <?php foreach($cfg['month'] as $monthNumber => $month) { ?>
                                    <option value="<?php echo $month;?>"><?php echo $month;?></option>
                            <?php } ?>
                        </select>
                    </span>
                    <span class="payment-options col-xs-3" >
                        <select  id="day_of_death" name="dod" data-prompt-position="topLeft" readonly class="validate[required] col-md-12">
                            <option selected="" value="">Day</option>
                            <?php for($day= 1; $day<= 31; $day++){
                                    $day = substr($day, -2);
                            ?>
                                <option  value="<?php echo $day;?>"><?php echo $day;?></option>
                            <?php } ?>
                        </select>
                    </span>
                    <span class="payment-options col-xs-4" >
                        <select  id="year_of_death" name="yod" data-prompt-position="topLeft" class="validate[required] col-md-12">
                            <?php for($year = date("Y"); $year >= 1900; $year--){
                                $selected = ($year == date("Y")) ? 'selected' : NULL;
                            ?>
                            <option <?php echo $selected;?> value="<?php echo $year;?>"><?php echo $year;?></option>
                            <?php }?>
                        </select>
                    </span>
                </div>
            </div>
            <?php } ?>

            <div class="eaFormBox">
                Sex:
                <div class="eaFormField">
                    <span class="planningRadio">
                        <span class="optionRadioInline">
                            <input type="radio" id="femaleRadio" name="sex" value="Female" class="validate[required] radio">
                            <label for="femaleRadio">Female</label>
                        </span>
                        <span class="optionRadioInline">
                            <input  type="radio" id="maleRadio" name="sex" value="Male" class="validate[required] radio">
                            <label for="maleRadio">Male</label>
                        </span>
                   </span>
                </div>
            </div>

            <div class="eaFormBox">
                Birth Place (City/Town, State or Foreign Country):
                <div class="eaFormField">
                    <input type="text" name="b_place" class="text validate[required]">
                </div>
            </div>

            <div class="eaFormBox">
                Marital Status
                <div class="eaFormField">
                    <span class="planningOptions">
                        <select name="marital" id="marital" class="validate[required]" >
                            <option value="Never Married">Never Married</option>
                            <option value="Married">Married</option>
                            <option value="Divorced">Divorced</option>
                            <option value="Widowed">Widowed</option>                        
                        </select>
                  </span>
                </div>
            </div>

            <div class="eaFormBox">
                Date of Marriage (If Married at Time of Death):
                <div class="eaFormField">
                        <span class="payment-options col-xs-5">
                        <select style="width: 98%;" id="mom" name="mom" >
                            <option selected="" value="">Month</option>
                            <?php foreach($cfg['month'] as $monthNumber => $month){?>
                                    <option  value="<?php echo $monthNumber;?>"><?php echo $month;?></option>
                            <?php } ?>
                        </select>
                        </span> <span class="payment-options col-xs-3">
                        <select style="width: 97%;" id="dom" name="dom" >
                            <option  value="">Day</option>
                            <?php
                                for($day= 1; $day<= 31; $day++){
                                    $day = substr($day, -2);
                                ?>
                                    <option value="<?php echo $day;?>"><?php echo $day;?></option>
                                <?php
                                }
                            ?>
                        </select>
                        </span> <span class="payment-options col-xs-4">
                        <select style="width: 98%;" id="yom" name="yom" >
                            <option selected="" value="">Year</option>
                            <?php
                            $currentYear = date("Y");
                            for($year = $currentYear; $year >= 1900; $year-- ){
                            ?>
                                <option value="<?php echo $year;?>"><?php echo $year;?></option>
                            <?php
                            }
                            ?>
                        </select>
                        </span>
                    </div>
            </div>

            <div class="eaFormBox">
                Surviving Spouse’s Name (Maiden Name):
                <div class="eaFormField">
                    <input type="text" name="spouse_name" class="text <?php echo $validateClass;?>">
                </div>
            </div>

            <div class="eaFormBox">
                Father’s Full Name:
                <div class="eaFormField">
                    <input type="text" name="father_name" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                Mother’s Full Maiden Name:
                <div class="eaFormField">
                    <input type="text" name="mother_name" class="text validate[required]">
                </div>
            </div>

            <div class="eaFormBox">
                Place of Death:
                <div class="eaFormField">
                    <span class="planningRadio">
                        <span class="optionRadioInline">
                            <input type="radio" id="hospitalInpatientRadio" name="pod" value="Hospital Inpatient" class="validate[required] radio">
                            <label for="hospitalInpatientRadio">Hospital Inpatient</label>
                        </span><br style="clear:both;" />
                        <span class="optionRadioInline">
                            <input type="radio" id="emergRmRadio" name="pod" value="Emerg. Rm." class="validate[required] radio">
                            <label for="emergRmRadio">Emerg. Rm.</label>
                        </span><br style="clear:both;" />
                        <span class="optionRadioInline">
                            <input type="radio" id="nursingHomeRadio" name="pod" value="Nursing Home" class="validate[required] radio">
                            <label for="nursingHomeRadio">Nursing Home</label>
                        </span><br style="clear:both;" />
                        <span class="optionRadioInline">
                            <input type="radio" id="hospiceRadio" name="pod" value="Hospice" class="validate[required] radio">
                            <label for="hospiceRadio">Hospice</label>
                        </span><br style="clear:both;" />
                        <span class="optionRadioInline">
                            <input type="radio" id="residenceRadio" name="pod" value="Residence" class="validate[required] radio">
                            <label for="residenceRadio">Residence</label>
                        </span><br style="clear:both;" />
                        <span class="optionRadioInline">
                            <input type="radio" id="otherRadio" name="pod" value="Other " class="validate[required] radio">
                            <label for="otherRadio">Other </label>
                        </span>
                   </span>
                </div>
                <div class="eaFormField">
                    <input type="text" name="other_pob" class="text validate[required]">
                </div>
            </div>

            <div class="eaFormBox">
                Was decedent a veteran? :
                <div class="eaFormField">
                    <span class="planningRadio">
                        <span class="optionRadioInline">
                            <input type="radio" id="decedentVeteranYes" name="decedentVeteran" value="Yes  " class="validate[required] radio">
                            <label for="decedentVeteranYes">Yes  </label>
                        </span>
                        <span class="optionRadioInline">
                            <input  type="radio" id="decedentVeteranNo" name="decedentVeteran" value="No" class="validate[required] radio">
                            <label for="decedentVeteranNo">No</label>
                        </span>
                   </span>
                </div>
            </div>

            <div class="eaFormBox">
               If yes, What branch:
                <div class="eaFormField">
                    <input type="text" name="decedent_veteran_branch" class="text">
                </div>
            </div>

            <div class="eaFormBox">
                What rank:
                <div class="eaFormField">
                    <input type="text" name="what_rank" class="text">
                </div>
            </div>

            <div class="eaFormBox">
                Education:
                <div class="eaFormField">
                    <span class="payment-options">
                    <select id="education" name="education" class="validate[required]">
                        <option value="8th Grade or Less">8th Grade or Less</option>
                        <option value="No diploma, 9-12 gr">No diploma, 9-12 gr</option>
                        <option value="High School Grad/GED">High School Grad/GED</option>
                        <option value="Some college credit, no degree">Some college credit, no degree</option>
                        <option value="Associates Degree">Associates Degree</option>
                        <option value="Bachelor’s Degree">Bachelor’s Degree</option>
                        <option value="Master’s Degree">Master’s Degree</option>
                        <option value="Doctorate or Professional Degree">Doctorate or Professional Degree</option>
                    </select>
                    </span>
                </div>
            </div>


            <div class="eaFormBox">
                Decedent of Hispanic Origin?:
                <div class="eaFormField">
                    <span class="planningRadio">
                        <input id="hispanic_origin1" class="validate[required] radio" type="radio" name="hispanic_origin" value="No, NOT Spanish/Hispanic/Latino" data-status="not-checked">
                        <label for="hispanic_origin1">No, NOT Spanish/Hispanic/Latino</label><br style="clear:both;" />
                        <input id="hispanic_origin2" class="validate[required] radio" type="radio" name="hispanic_origin" value="Yes, Mexican/Mexican American/Chicano  " data-status="not-checked">
                        <label for="hispanic_origin2">Yes, Mexican/Mexican American/Chicano </label><br style="clear:both;" />
                        <input id="hispanic_origin3" class="validate[required] radio" type="radio" name="hispanic_origin" value="Yes, Puerto Rican" data-status="not-checked">
                        <label for="hispanic_origin3">Yes, Puerto Rican</label><br style="clear:both;" />
                        <input id="hispanic_origin4" class="validate[required] radio" type="radio" name="hispanic_origin" value="Yes, Cuban" data-status="not-checked">
                        <label for="hispanic_origin4">Yes, Cuban</label><br style="clear:both;" />
                        <input id="hispanic_origin5" class="validate[required] radio" type="radio" name="hispanic_origin" value="Yes, Other (specify)" data-status="not-checked">
                        <label for="hispanic_origin5">Yes, Other (specify)</label><br style="clear:both;" />
                        <input type="text" name="hispanic_origin_other" class="text">
                   </span>
               </div>
            </div>

            <div class="eaFormBox">
                Decedent’s Race :
                <div class="eaFormField">
                    <span class="planningRadio">
                        <span class="optionCheck"><input id="race1" class="validate[required] radio" type="checkbox" name="race" value="White" data-status="not-checked">
                        <label for="race1">White</label></span><br style="clear:both;" />
                        <span class="optionCheck"><input id="race2" class="validate[required] radio" type="checkbox" name="race" value="Black or African American" data-status="not-checked">
                        <label for="race2">Black or African American</label></span><br style="clear:both;" />
                        <span class="optionCheck"><input id="race3" class="validate[required] radio" type="checkbox" name="race" value="American Indian or    Alaska Native" data-status="not-checked">
                        <label for="race3">American Indian or   Alaska Native</label></span><br style="clear:both;" />
                        <span class="optionCheck"><input id="race4" class="validate[required] radio" type="checkbox" name="race" value="Asian Indian" data-status="not-checked">
                        <label for="race4">Asian Indian</label></span><br style="clear:both;" />                  
                        <span class="optionCheck"><input id="race5" class="validate[required] radio" type="checkbox" name="race" value="Chinese" data-status="not-checked">
                        <label for="race5">Chinese</label></span><br style="clear:both;" />
                        <span class="optionCheck"><input id="race6" class="validate[required] radio" type="checkbox" name="race" value="Filipino" data-status="not-checked">
                        <label for="race6">Filipino</label></span><br style="clear:both;" />
                        <span class="optionCheck"><input id="race7" class="validate[required] radio" type="checkbox" name="race" value="Japanese" data-status="not-checked">
                        <label for="race7">Japanese</label></span><br style="clear:both;" />
                        <span class="optionCheck"><input id="race8" class="validate[required] radio" type="checkbox" name="race" value="Korean" data-status="not-checked">
                        <label for="race8">Korean</label></span><br style="clear:both;" />
                        <span class="optionCheck"><input id="race9" class="validate[required] radio" type="checkbox" name="race" value="Vietnamese" data-status="not-checked">
                        <label for="race9">Vietnamese</label></span><br style="clear:both;" />
                        <span class="optionCheck"><input id="race10" class="validate[required] radio" type="checkbox" name="race" value="Other Asian" data-status="not-checked">
                        <label for="race10">Other Asian</label></span><br style="clear:both;" />
                        <span class="optionCheck"><input id="race11" class="validate[required] radio" type="checkbox" name="race" value="Native Hawaiian" data-status="not-checked">
                        <label for="race11">Native Hawaiian</label></span><br style="clear:both;" />
                        <span class="optionCheck"><input id="race12" class="validate[required] radio" type="checkbox" name="race" value="Other (Specify)" data-status="not-checked">
                        <label for="race12">Other (Specify)</label></span>                    
                        <input type="text" name="race_other" class="text">
                   </span>
               </div>
            </div>

            <div class="eaFormBox">
               Decedent’s Single Race Self-Designation: Same as Above / Other (list) :
                <div class="eaFormField">
                    <input type="text" name="single_race" class="text">
                </div>
            </div>

            <div class="eaFormBox">
               Occupation:
                <div class="eaFormField">
                    <input type="text" name="occupation" class="text">
                </div>
            </div>

            <div class="eaFormBox">
               Type of Business:
                <div class="eaFormField">
                    <input type="text" name="type_business" class="text">
                </div>
            </div>

            <div class="eaFormBox">
               Informant’s Name:
                <div class="eaFormField">
                    <input type="text" name="informant_name" class="text">
                </div>
            </div>

            <div class="eaFormBox">
                Relationship to Decedent:
                <div class="eaFormField">
                    <input type="text" name="informant_relationship" class="text">
                </div>
            </div>


            <div class="eaFormBox">
               Street Address:
                <div class="eaFormField">
                    <input type="text" name="informant_address" class="text validate[required]" value="">
                </div>
            </div>
            <div class="eaFormBox">
                City:
                <div class="eaFormField">
                    <input type="text" name="informant_city" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
                State:
                <div class="eaFormField">
                    <span class="payment-options">
                        <select name="informant_state" class="input-dropdown validate[required]">
                            <option value="" >Select State</option>
                            <?php foreach($cfg['states'] as $state => $row){ ?>
                            <option value="<?php echo $state;?>"><?php echo $state;?></option>
                            <?php } ?>
                        </select>
                    </span>
                </div>
            </div>
            <div class="eaFormBox">
                Zip Code:
                <div class="eaFormField">
                    <input type="text" name="informant_zip" class="text validate[required]">
                </div>
            </div>
            <div class="eaFormBox">
               Phone:
                <div class="eaFormField">
                    <input type="text" name="informant_phone" class="text validate[required] phoneFormat">
                </div>
            </div>
            <div class="eaFormBox">
               Email:
                <div class="eaFormField">
                    <input type="text" name="informant_email" data-value="sip@frazerconsultants.com" class="text validate[required ,custom[email]]"
                    value=""  />
                </div>
            </div>

            <div class="eaFormBox">
               Mobile:
                <div class="eaFormField">
                    <input type="text" name="informant_mobile" class="text">
                </div>
            </div>


            <div class="eaFormBox">
               How many death certificates needed ($6/certified copy):
                <div class="eaFormField">
                    <input type="text" name="death_certificates_need" class="text">
                </div>
            </div>

            <div class="eaFormBox">
                Type of Services :
                <div class="eaFormField">
                    <span class="planningRadio">
                        <span class="optionRadioInline">
                            <input type="radio" id="service_type1" name="service_type" value="Simple Cremation w/ family pick up of urn" class="validate[required] radio">
                            <label for="service_type1">Simple Cremation w/ family pick up of urn</label>
                        </span>
                        <span class="optionRadioInline">
                            <input type="radio" id="service_type2" name="service_type" value="Simple cremation & mail urn to family" class="validate[required] radio">
                            <label for="service_type2">Simple cremation & mail urn to family</label>
                        </span>
                        <span class="optionRadioInline">
                            <input type="radio" id="service_type3" name="service_type" value="Veteran’s Cremation Package" class="validate[required] radio">
                            <label for="service_type3">Veteran’s Cremation Package</label>
                        </span>
                   </span>
                </div>
            </div>


            

            <p></p>

            <div class="ea-btt-prev-next-area">
                <div class="row">
                    <div class="col-sm-5 col-md-5">
                        <a class="ea-btt-prev-next" href="<?php echo $prev_form;?>">Previous Step</a>
                    </div>
                    <div class="col-xs-offset-0 col-sm-5 col-sm-offset-2 col-md-5 col-md-offset-2">
                        <a class="ea-btt-prev-next btnSubmit" href="/ea/step6">Next Step</a>
                    </div>
                </div>
            </div>
        </div><!--====  End of biographical-information-form-  ====-->
    </div>
    </form>
</div>



